
/* 
 * File:   newfile.hpp
 * Author: juan
 *
 * Created on November 13, 2017, 3:24 PM
 */

#ifndef ATOM_HPP
#define ATOM_HPP

#include <vector>

using namespace std;

typedef struct atom{
    
    int     n_atoms; //< # of atoms
    int     n_atoms_sc; //< # of atoms in the supercell
    int     n_types; //< # of type of atoms
    int     maxneigh; //< max # of neighbors
    int     diffusion; //< 0=no diffusion; 1=yes diffusion
    int     *numneigh; //< # of J neighbors for each I atom (we only need for the cell)
    int     *neigh; //< local indices of I atoms (we only need for the cell)
    int     *master; //< id of the master atom (size= n_atoms_sp)
    int     *trans_index; //< index that indicates the relation with its corresponding master atom
    double  r2_cutoff; //< radius cutoff raised to the power of two
    double  skin; //< skin distance
    //double  temperature; //<temperature
    double  b; //< term related to the temperature
    double* q; //< Atomic position (size= n_atoms_sp*3)
    double* x; //< atomic molar fraction (we only need for the cell)
    double* w; //< atomic frequency (we only need for the cell)
    
    double box_x_min;
    double box_y_min;
    double box_z_min;
    double box_x_max;
    double box_y_max;
    double box_z_max;
    //double lx;
    //double ly;
    //double lz;
    
    vector<int> fixed_q;
    vector<int> fixed_x;
    
}atom;

void init_atom(atom* cs);
void destroy_atom( atom* cs );

#endif /* ATOM_HPP */

