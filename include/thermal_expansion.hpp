
/* 
 * File:   thermal_expansion.hpp
 * Author: juan
 *
 * Created on March 5, 2018, 1:48 PM
 */

#ifndef THERMAL_EXPANSION_HPP
#define THERMAL_EXPANSION_HPP

double thermal_expansion(EAMPotential *eam[2][2], atom *atoms, double factor, double step);

#endif /* THERMAL_EXPANSION_HPP */

