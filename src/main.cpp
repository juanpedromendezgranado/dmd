#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <iomanip> // to print more decimals
#include "eam_potential.hpp"
#include "atom.hpp"
#include "input.hpp"
#include "output.hpp"
#include "variables.hpp"
#include "init_freq.hpp"
#include "boundary_conditions.hpp"
#include "supercell.hpp"
#include "neighbors.hpp"
#include "test.hpp" //eliminar
#include "solver_PETSc_pressure.hpp"
#include "solver_PETSc.hpp"
#include "mf_eam_free_energy.hpp"
#include "mass_transport.hpp"

using namespace std;
using namespace MPI;

static char help[] = "This is line is only for Petsc purpose.\n\n";

int main( int argc, char** argv ){
      
       
    // Definition of potential
    
    EAMPotential *eam[2][2];
    EAMPotential eamMg, eamAl, eamMgAl;
    init_EAM( &eamMg, Mg );
    init_EAM( &eamAl, Al );
    init_EAM( &eamMgAl, MgAl );
    eam[0][0]=&eamMg;   eam[1][1]=&eamAl;
    eam[0][1]=&eamMgAl; eam[1][0]=&eamMgAl;

    // MPI initializes
    //MPI_Init(&argc, &argv);
    PetscInitialize(&argc,&argv,(char*)0,help);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);    
    
    // Read atomic positions from a file 
	
    atom atoms;
    init_atom(&atoms);
    read_atoms(&atoms); 
    

    boundary_conditions(&atoms);
    supercell  (&atoms);
    neighbors  (&atoms);   
    
    //test(eam,&atoms);
    
    // Initial frequencies
    init_frequencies (eam, &atoms); 
    
    if(rank==0) Write_q_x_w (0, &atoms);

    // Initial Thermolization
    double final_temp = 1.0/k_B/atoms.b;
    if(final_temp>300){        
        for(double temp=300; temp<final_temp; temp += 100){
            
            cout<< endl <<"***** "<<temp<<" ******";
            atoms.b=1.0/temp/k_B;
            Minimize_petsc (0,eam, &atoms, "Fqw");
            double press[]={0,0,0,0,0,0};
            Minimize_npt_petsc (0, eam, &atoms, press);
        }
        atoms.b=1.0/final_temp/k_B;
        if(rank==0) Write_q_x_w (0, &atoms);
    }
          
    for(int it=1;it<100; it++ ){
        // Diffusive step
        if(atoms.diffusion==1) mass_transport (it, &atoms, eam);
        // Equilibrium step
        Minimize_petsc (it,eam, &atoms, "Fqw");
        double press[]={0,0,0,0,0,0};
        Minimize_npt_petsc (0, eam, &atoms, press);
        // Computation of energy
        double en = mf_EAM_Free_Energy(eam,&atoms); 
        if(rank==0) cout<<en<<endl;
        if(rank==0) Write_FreeEntropy(it, en);
        if(rank==0) Write_q_x_w (it, &atoms);
    }
        
    // convert binary files to text files
    
    if(rank==0)ConvertBinaryFiletoTextFile();
    if(rank==0)ConvertBinaryFiletoTextFile_Petsc();
    
    // free memory
    
    destroy_atom( &atoms );
    destroy_EAM( &eamMg );
    destroy_EAM( &eamAl );
    destroy_EAM( &eamMgAl );
    
    // MPI Finalizes
    PetscFinalize();
    //MPI_Finalize();

    return 0;
}

//int main( int argc, char** argv ){
//      
//       
//    // Definition of potential
//    
//    EAMPotential *eam[2][2];
//    EAMPotential eamMg, eamAl, eamMgAl;
//    init_EAM( &eamMg, Mg );
//    init_EAM( &eamAl, Al );
//    init_EAM( &eamMgAl, MgAl );
//    eam[0][0]=&eamMg;   eam[1][1]=&eamAl;
//    eam[0][1]=&eamMgAl; eam[1][0]=&eamMgAl;
//
//    // MPI initializes
//    //MPI_Init(&argc, &argv);
//    PetscInitialize(&argc,&argv,(char*)0,help);
//    MPI_Comm_size(MPI_COMM_WORLD, &size);
//    MPI_Comm_rank(MPI_COMM_WORLD, &rank);    
//    
//    // Read atomic positions from a file 
//	
//    atom atoms;
//    init_atom(&atoms);
//    read_atoms(&atoms); 
//    
//    
////    double val=4.8/3.0;
////    
////    for(int at=0; at<atoms.n_atoms; at++){//eliminar
////            atoms.q[at*3+0] *= val;//eliminar
////            atoms.q[at*3+1] *= val;//eliminar
////            atoms.q[at*3+2] *= val;//eliminar            
////    }//eliminar
////    
////    atoms.box_x_max = atoms.box_x_max*val;//eliminar
////    atoms.box_x_min = atoms.box_x_min*val;//eliminar
////    atoms.box_y_max = atoms.box_y_max*val;//eliminar
////    atoms.box_y_min = atoms.box_y_min*val;//eliminar
////    atoms.box_z_max = atoms.box_z_max*val;//eliminar
////    atoms.box_z_min = atoms.box_z_min*val;//eliminar
//    
//    supercell  (&atoms);
//    neighbors  (&atoms);
//    
//    //init_frequencies (eam, &atoms);
//    
//    int cont=1;
//    
//    for (double val=3.2; val<=5.3; val += 0.05){
//    
//        for(int at=0; at<atoms.n_atoms; at++){//eliminar
//            atoms.q[at*3+0] *= val/4.1;//eliminar
//            atoms.q[at*3+1] *= val/4.1;//eliminar
//            atoms.q[at*3+2] *= val/4.1;//eliminar            
//        }//eliminar
//    
//        atoms.box_x_max = atoms.box_x_max*val/4.1;//eliminar
//        atoms.box_x_min = atoms.box_x_min*val/4.1;//eliminar
//        atoms.box_y_max = atoms.box_y_max*val/4.1;//eliminar
//        atoms.box_y_min = atoms.box_y_min*val/4.1;//eliminar
//        atoms.box_z_max = atoms.box_z_max*val/4.1;//eliminar
//        atoms.box_z_min = atoms.box_z_min*val/4.1;//eliminar
//        
//               
//        supercell  (&atoms);//cout <<endl;
//        neighbors  (&atoms);
//        
//        if(cont==1){ init_frequencies (eam, &atoms); cont=0;}
//        //init_frequencies (eam, &atoms);
//        
//        
//        if(rank==0) Write_q_x_w (0, &atoms);
//        //if(rank==0) Write_q_x_w_nsc (0, &atoms);
//           
//       
//        for(int it=1;it<2; it++ ){
//            double en;
//            //mass_transport (it, &atoms, eam);
//            Minimize_petsc (it,eam, &atoms, "Fw");
//            en = mf_EAM_Free_Energy(eam,&atoms); 
//            //cout<<val<<" "<<en<<endl;
//            //Write_FreeEntropy(it, en);
//            //en = EAM_Potential(eam,&atoms); 
//            cout<<setprecision(10)<<val<<" "<<en<<endl;
//            //Write_FreeEntropy(it, en);
//            //Write_q_x_w (it, &atoms);
//        }        
//        
//        for(int at=0; at<atoms.n_atoms; at++){//eliminar
//            atoms.q[at*3+0] = atoms.q[at*3+0]/val*4.1;//eliminar
//            atoms.q[at*3+1] = atoms.q[at*3+1]/val*4.1;//eliminar
//            atoms.q[at*3+2] = atoms.q[at*3+2]/val*4.1;//eliminar            
//        }//eliminar
//    
//        atoms.box_x_max = atoms.box_x_max/val*4.1;//eliminar
//        atoms.box_x_min = atoms.box_x_min/val*4.1;//eliminar
//        atoms.box_y_max = atoms.box_y_max/val*4.1;//eliminar
//        atoms.box_y_min = atoms.box_y_min/val*4.1;//eliminar
//        atoms.box_z_max = atoms.box_z_max/val*4.1;//eliminar
//        atoms.box_z_min = atoms.box_z_min/val*4.1;//eliminar
//        
//    }
//    
//    exit(1);
//    
//    
//    //boundary_conditions(&atoms);
//    supercell  (&atoms);
//    neighbors  (&atoms);
//    
//    // Initial frequencies
//    init_frequencies (eam, &atoms); 
//    
//    Write_q_x_w (0, &atoms);
//      
//   test(eam, &atoms);   
//    
//    //atoms.fixed_q.push_back(0);
//    //Write_q_x_w (0, &atoms);
////    for(int it=1;it<100; it++ ){
////        //mass_transport (it, &atoms, eam);
////        Minimize_petsc (it,eam, &atoms, "Fw");
////        double en = mf_EAM_Free_Energy(eam,&atoms); cout<<en<<endl;
////        Write_FreeEntropy(it, en);
////        Write_q_x_w (it, &atoms);
////    }
//    
//    test(eam, &atoms);
//        
//    // convert binary files to text files
//    
//    if(rank==0)ConvertBinaryFiletoTextFile();
//    if(rank==0)ConvertBinaryFiletoTextFile_Petsc();
//    
//    // free memory
//    
//    destroy_atom( &atoms );
//    destroy_EAM( &eamMg );
//    destroy_EAM( &eamAl );
//    destroy_EAM( &eamMgAl );
//    
//    // MPI Finalizes
//    PetscFinalize();
//    //MPI_Finalize();
//
//    return 0;
//}
