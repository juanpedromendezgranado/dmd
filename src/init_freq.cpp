#include <mpi.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <iostream>
#include "eam_potential.hpp"
#include "atom.hpp"

extern double min_omega;
extern double min_init_freq;
extern int rank;
extern int size;

void init_frequencies ( EAMPotential *eam[2][2], atom *atoms){

    int i, j;
    
    double* x = atoms->x;
    int n_atoms = atoms->n_atoms;
    int n_types = atoms->n_types;
    
    double mass;
    double* kii = (double*) malloc( n_atoms * sizeof( double ) );
    double* all_kii = (double*) malloc( n_atoms * sizeof( double ) );
    
    // Initialization
    for(i=0; i<n_atoms; i++)kii[i]=0.0;
    
    tr_d2_EAM_Potential(eam, atoms, all_kii);

    for(i=rank; i<n_atoms; i+=size){        
        
        mass = 0;
        for(j=0; j<n_types; j++){ mass += x[i*n_types+j]/eam[j][j]->mass;}
        mass = 1.0/mass;
        
        kii[i] = sqrt(all_kii[i]/3.0/mass)*98.22694969;// the factor 98.22694969 is to change the units: 98.22694969 1/ps = 1 ((eV/A^2)/u.m.a.)^(1/2)                
        if(kii[i]!=kii[i] || kii[i]<min_omega) kii[i]=min_init_freq;
    }
    
    MPI_Allreduce(kii, all_kii, n_atoms, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    
    // update the atomic frequency
    for(i=0;i<n_atoms;i++) atoms->w[i] = all_kii[i];
    
    free(kii);
    free(all_kii);
    
    return;
}
