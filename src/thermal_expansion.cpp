
/* 
 * File:   thermal_expansion.cpp
 * Author: juan
 * 
 * Created on March 5, 2018, 1:48 PM
 */

#include <cstdlib>
#include <iostream> //std::cout//std::cin
#include <fstream>
#include <sstream> //std::istringstream
#include <cstdio>
#include <string> //std::string
#include <cstring>

#include "atom.hpp"
#include "eam_potential.hpp"
#include "mf_eam_free_energy.hpp"
#include "solver_PETSc.hpp"

extern int rank;
extern int size;
extern double k_B;

using namespace std;


double thermal_expansion(EAMPotential *eam[2][2], atom *atoms, double factor, double step){
    
/**
 * \brief This function finds the thermal expansion, increasing the computational cell and finding the optimal frequency and the atomic position, or one of them modifying the flag;
 * 
 * the starting configuration is the one giving in the input as atomicsites **sites_supercell and multiply by the initial factor a_initial
 * The search stops when the energy increases.
 * factor: it is the initial thermal expansion
 * We need to fix the amo 0. Be careful!!
 * 
 * @param sites
 * @param Ntotal_sc
 * @param Ntotal
 * @param factor
 * @param step
 * @return 
 */

      
    int n_atoms = atoms->n_atoms;
    int n_atoms_sc = atoms->n_atoms_sc;
    
    int i, atom_i, k, cont=0;
    double pos[3];
    double prev_en, en;
    ofstream fichero;
    
    if(rank==0){cout << endl << "Computing thermal expansion... ";} 
    
    fichero.open ("results/linearthermalexpansion", ios::app);
    
    //*** INITIAL ENERGY ***//
    
    // update position
    for(i=0; i<n_atoms_sc*3; i++) atoms->q[i] *= (1.0+factor);
    
    // update boundaries    
    atoms->box_x_max *= (factor+1.0);
    atoms->box_x_min *= (factor+1.0);
    atoms->box_y_max *= (factor+1.0);
    atoms->box_y_min *= (factor+1.0);
    atoms->box_z_max *= (factor+1.0);
    atoms->box_z_min *= (factor+1.0);
         
    if(rank==0)cout<<endl<<"Box_x_min: "<<atoms->box_x_min<<" Box_x_max: "<<atoms->box_x_max
                        <<" Box_y_min: "<<atoms->box_y_min<<" Box_y_max: "<<atoms->box_y_max
                        <<" Box_z_min: "<<atoms->box_z_min<<" Box_z_max: "<<atoms->box_z_max;    
    
    // Find optimal w
    Minimize_petsc (0, eam, atoms, "Fw");
    
    //Compute energy
    //en = mf_EAM_Free_Energy( eam, atoms);
    en = EAM_Potential( eam, atoms);   
    
    if(rank==0)cout<<endl<<factor<<"  "<<en;
    prev_en = en;    
    
    cont++;
    ////////////////////////////////////////////////////////////////////////////
    iteration:
    
    // update position
    for(i=0; i<n_atoms_sc*3; i++) atoms->q[i] *= (factor+step*(double)cont)/(factor+step*((double)cont-1.0));
       
    // update boundaries    
    atoms->box_x_max *= (factor+step*(double)cont)/(factor+step*((double)cont-1.0));
    atoms->box_x_min *= (factor+step*(double)cont)/(factor+step*((double)cont-1.0));
    atoms->box_y_max *= (factor+step*(double)cont)/(factor+step*((double)cont-1.0));
    atoms->box_y_min *= (factor+step*(double)cont)/(factor+step*((double)cont-1.0));
    atoms->box_z_max *= (factor+step*(double)cont)/(factor+step*((double)cont-1.0));
    atoms->box_z_min *= (factor+step*(double)cont)/(factor+step*((double)cont-1.0));
    
    if(rank==0)cout<<endl<<"Box_x_min: "<<atoms->box_x_min<<" Box_x_max: "<<atoms->box_x_max
                         <<" Box_y_min: "<<atoms->box_y_min<<" Box_y_max: "<<atoms->box_y_max
                         <<" Box_z_min: "<<atoms->box_z_min<<" Box_z_max: "<<atoms->box_z_max;   
       
    // Find optimal w
    Minimize_petsc (cont, eam, atoms, "Fw");
    
    //Compute energy
    //en = mf_EAM_Free_Energy( eam, atoms);
    en = EAM_Potential( eam, atoms);   
       
    if(en <= prev_en){ 
        if(rank==0)cout<< endl<<(factor+step*(double)cont) << "  "<< en;      
        prev_en = en;
        cont++;
        goto iteration;
    }else{
        
        if(rank==0){
            cout << endl << "en: "<<en<<" prev_en: "<<prev_en;
            cout << endl << (factor+step*(double)cont) << "  " << en;
        }       
    }
    
    if(rank==0) fichero << 1.0/(atoms->b*k_B) << "  " << factor+step*(double)(cont-1)<<endl;
    
    ////////////////////////////////////////////////////////////////////////////
    
    //restoring the position and boundary condition
    for(i=0; i<n_atoms_sc*3; i++) atoms->q[i] /= (factor+step*(double)cont);    
    
    atoms->box_x_max /= (factor+step*(double)cont);
    atoms->box_x_min /= (factor+step*(double)cont);
    atoms->box_y_max /= (factor+step*(double)cont);
    atoms->box_y_min /= (factor+step*(double)cont);
    atoms->box_z_max /= (factor+step*(double)cont);
    atoms->box_z_min /= (factor+step*(double)cont);
       
    
    fichero.close(); 
    
    if(rank==0) cout << endl << "Done computing thermal expansion... ";   
    
return factor+step*(double)(cont-1);

}

