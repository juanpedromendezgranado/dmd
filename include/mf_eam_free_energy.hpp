
#ifndef MF_EAM_FREE_ENERGY_HPP
#define MF_EAM_FREE_ENERGY_HPP

double mf_EAM_Free_Energy( EAMPotential *eam[2][2], atom* atoms);
void mf_d_EAM_Free_Energy( EAMPotential *eam[2][2], atom* atoms, double *f );
void mf_dx_EAM_Free_Energy( EAMPotential *eam[2][2], atom* atoms, double *f );
void mf_Virial_Stresses_EAM_Potential( EAMPotential *eam[2][2], atom* atoms, double *all_f );

#endif /* MF_EAM_FREE_ENERGY_HPP */

