
/* 
 * File:   mass_transport.hpp
 * Author: juan
 *
 * Created on March 6, 2018, 8:08 PM
 */

#ifndef MASS_TRANSPORT_HPP
#define MASS_TRANSPORT_HPP

void mass_transport (int iter, atom *atoms, EAMPotential *eam[2][2]);

#endif /* MASS_TRANSPORT_HPP */

