
/* 
 * File:   boundary_conditions.hpp
 * Author: juan
 *
 * Created on February 5, 2018, 1:51 PM
 */

#ifndef BOUNDARY_CONDITIONS_HPP
#define BOUNDARY_CONDITIONS_HPP

void boundary_conditions(atom *atoms);

#endif /* BOUNDARY_CONDITIONS_HPP */

