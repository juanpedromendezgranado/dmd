

/**
 * \author J.P. Mendez
 * \brief I/O Functions
 */


#include <mpi.h> // before <stdio.h>
#include <cstdlib>
#include <iostream> //std::cout//std::cin
#include <fstream>
#include <sstream> //std::istringstream
#include <cstdio>
#include <string> //std::string
#include <cstring>
#include <vector>

#include "atom.hpp"
#include "eam_potential.hpp"

using namespace std;
using namespace MPI;

extern int rank, size;


/**
 *
 *  Serial version
 * 
 **/

void Write_q_x_w (int it, atom *atoms) {
    
    /**
     * \brief function to write in a file the atomic position, the atomic molar fraction and the atomic frequency
     * @param it iteration
     * @param atoms 
     */
    
    int dim=3;
    int n_atoms = atoms->n_atoms;    
    int n_types = atoms->n_types;
    
    ofstream fichero;
    fichero.open ("results/q_w_x", ios::app);
    
    fichero << n_atoms<< endl;
    fichero << it << endl;
    
    for (int i=0; i<n_atoms; i++){
        
        fichero<<i+1<<"  "<<atoms->q[i*dim+0]<<"  "<<atoms->q[i*dim+1]<<"  "<<atoms->q[i*dim+2];
        for(int j=0; j<n_types; j++) fichero <<"  "<<atoms->x[i*n_types+j];
        fichero<<"  "<< atoms->w[i]<<endl;
    }
    
    fichero.close();
    
    return;
}

void Write_FreeEntropy(int it, double en) {
    
    ofstream fichero;
    fichero.open ("results/FreeEntropy", ios::app);
    
    if(it==0)fichero <<"Iteration     Free Entropy";
    fichero<<endl<<it<<"   "<<en;

    fichero.close();
    
    return;
}

/**
 *
 *  Parallel version
 * 
 **/

void Write_q_x_w_mpi (int it, atom *atoms){
    
    /**
     * \brief function to write in parallel the atomic position, atomic molar fraction and the atomic frequency. 
     * 
     * @param it iteration
     * @param sites info of atoms
     */
    
    // definition of variable
    int n_atoms = atoms->n_atoms;
    int n_types = atoms->n_types;
    int errcode;
    MPI_File file;
    char filename[]="results/q_w_x_binary";
    MPI_Offset ofst = (int)0;
    int num_elements = 0;    
    MPI_Datatype datatype, filetype;
    MPI_Aint data_disps[3], file_disps[3]; // q_i[3](double), x_ik[n_types](double), omega_i(double)
    int array_blocklens[3];
    MPI_Datatype array_types[3]={MPI_BYTE,MPI_BYTE,MPI_BYTE};
        
    // rank=0 will check if the file exits if not create the file    
    if(rank==0){
        errcode = MPI_File_open(MPI_COMM_SELF,filename, MPI_MODE_WRONLY | MPI_MODE_APPEND| MPI_MODE_CREATE, MPI_INFO_NULL, &file);
        if(errcode!=MPI_SUCCESS){
            if(rank==0)cout << endl << "We could not create the file q_w_x_binary!!. We proceed to exit.."<<endl;
            MPI_Abort(MPI_COMM_WORLD, errcode);
        }
        
        MPI_File_close (&file); 
    }
    
    // others ranks wait for rank=0
    MPI_Barrier(MPI_COMM_WORLD);
    
    // all ranks open file
    errcode = MPI_File_open(MPI_COMM_WORLD,filename, MPI_MODE_WRONLY | MPI_MODE_APPEND, MPI_INFO_NULL, &file);
    
    MPI_File_get_position(file, &ofst);

    MPI_Barrier(MPI_COMM_WORLD); // in order to get the correct position of the file for all ranks
    
    // to write heading
    if(rank==0){        
        MPI_File_write_at (file, ofst, &n_atoms, sizeof(int), MPI_BYTE, MPI_STATUS_IGNORE); // Number of atoms      
        MPI_File_write_at (file, ofst+sizeof(int), &it, sizeof(int), MPI_BYTE, MPI_STATUS_IGNORE); // iteration number          
    }
    
    ofst += sizeof(int)*2;
    
    num_elements = (int)n_atoms/size; 
    if(num_elements<1){// only when there is more processors than data to write
        if(rank<n_atoms){num_elements=1;}else{num_elements=0;}
    }
    
    //for data
    MPI_Get_address ( &atoms->q[3*rank*num_elements],       &data_disps[0]); //q[3]
    MPI_Get_address ( &atoms->x[n_types*rank*num_elements], &data_disps[1]); //x[n_types] 
    MPI_Get_address ( &atoms->w[rank*num_elements],         &data_disps[2]); //w
    //for file
    file_disps[0]=ofst+rank*num_elements*sizeof(double)*3; // q 
    file_disps[1]=ofst+(n_atoms*3+rank*num_elements*n_types)*sizeof(double); // x[n_types]
    file_disps[2]=ofst+(n_atoms*(3+n_types)+rank*num_elements)*sizeof(double); // w
    
    if(rank<size-1 || size==1){           
        if(num_elements>0){            
            array_blocklens[0]=sizeof(double)*3*num_elements; // q_i
            array_blocklens[1]=sizeof(double)*n_types*num_elements; // x_i[n_types]
            array_blocklens[2]=sizeof(double)*num_elements; //omega_i        
        }else{ for(int i=0; i<3; i++)array_blocklens[i]=1;}
    }else{// for the rank==size-1 when size>1, write the rest
        if(num_elements>0){            
            array_blocklens[0]=sizeof(double)*3*(n_atoms-num_elements*rank); // q_i
            array_blocklens[1]=sizeof(double)*n_types*(n_atoms-num_elements*rank); // x_i[n_types]
            array_blocklens[2]=sizeof(double)*(n_atoms-num_elements*rank); // omega_i                   
        }else{ for(int i=0; i<3; i++)array_blocklens[i]=1;}
    }        
    
    // define MPI_type for the data and file
    MPI_Type_create_struct(3,array_blocklens,data_disps,array_types, &datatype);
    MPI_Type_create_struct(3,array_blocklens,file_disps,array_types, &filetype);    
    // commit MPI_type
    MPI_Type_commit(&datatype);
    MPI_Type_commit(&filetype);    
    // write
    MPI_File_set_view(file,  0, MPI_BYTE,filetype,"native",MPI_INFO_NULL);
    MPI_File_write_all(file, 0, 1, datatype, MPI_STATUS_IGNORE);    
    // free MPI_type
    MPI_Type_free(&datatype);
    MPI_Type_free(&filetype);  
     
    // close file
    MPI_File_close(&file);
    
    return;
}

void Write_q_x_w_PETSc_mpi  (int it, atom *atoms, double f[]){
    
    /**
     * \brief function to write in parallel the atomic position, the atomic molar fraction, the atomic frquency and the forces when using PETSc solver. 
     * 
     * @param it iteration
     * @param atom info of atoms
     * @param f forces array (both)
     */
    
    // definition of variable
    int n_atoms = atoms->n_atoms;
    int n_types = atoms->n_types;
    int errcode;
    MPI_File file;
    char filename[]="results/q_w_x_Petsc_binary";
    MPI_Offset ofst = (int)0;
    int num_elements = 0;    
    MPI_Datatype datatype, filetype;
    MPI_Aint data_disps[4], file_disps[4]; // q_i[3](double), x_ik[n_types](double), omega_i(double), forces (both)
    int array_blocklens[4];
    MPI_Datatype array_types[4]={MPI_BYTE,MPI_BYTE,MPI_BYTE,MPI_BYTE}; 
    
    // rank=0 will check if the file exits if not create the file    
    if(rank==0){
        errcode = MPI_File_open(MPI_COMM_SELF,filename, MPI_MODE_WRONLY | MPI_MODE_APPEND| MPI_MODE_CREATE, MPI_INFO_NULL, &file);
        if(errcode!=MPI_SUCCESS){
            if(rank==0)cout << endl << "We could not create the file q_w_x_Petsc_binary!!. We proceed to exit.."<<endl;
            MPI_Abort(MPI_COMM_WORLD, errcode);
        }
        
        MPI_File_close (&file); 
    }
    
    // others ranks wait for rank=0
    MPI_Barrier(MPI_COMM_WORLD);
    
    // all ranks open file
    errcode = MPI_File_open(MPI_COMM_WORLD,filename, MPI_MODE_WRONLY | MPI_MODE_APPEND, MPI_INFO_NULL, &file);
    
    MPI_File_get_position(file, &ofst);

    MPI_Barrier(MPI_COMM_WORLD); // in order to get the correct position of the file for all ranks
    
    // to write heading
    if(rank==0){        
        MPI_File_write_at (file, ofst, &n_atoms, sizeof(int), MPI_BYTE, MPI_STATUS_IGNORE); // Number of atoms      
        MPI_File_write_at (file, ofst+sizeof(int), &it, sizeof(int), MPI_BYTE, MPI_STATUS_IGNORE); // iteration or time           
    }
    ofst += sizeof(int)*2;
    
    num_elements = (int)n_atoms/size; 
    if(num_elements<1){// only when there is more processors than data to write
        if(rank<n_atoms){num_elements=1;}else{num_elements=0;}
    }  
    
    //for data
    MPI_Get_address ( &atoms->q[rank*num_elements*3],       &data_disps[0]); //q[3]
    MPI_Get_address ( &atoms->x[rank*num_elements*n_types], &data_disps[1]); //x[n_types]
    MPI_Get_address ( &atoms->w[rank*num_elements],         &data_disps[2]); //w
    MPI_Get_address ( &f[rank*num_elements*4],              &data_disps[3]); //f[3+1]
    //for file
    file_disps[0]=ofst+rank*num_elements*sizeof(double)*3; //q[3]
    file_disps[1]=ofst+(n_atoms*3+rank*num_elements*n_types)*sizeof(double); //x[n_types]
    file_disps[2]=ofst+(n_atoms*(3+n_types)+rank*num_elements)*sizeof(double); //w
    file_disps[3]=ofst+(n_atoms*(4+n_types)+rank*num_elements*4)*sizeof(double); //f[3+1]
    
    if(rank<size-1 || size==1){           
        if(num_elements>0){            
            array_blocklens[0]=sizeof(double)*3*num_elements; // q_i[3]
            array_blocklens[1]=sizeof(double)*n_types*num_elements; // x_i[n_types]
            array_blocklens[2]=sizeof(double)*num_elements; // omega_i
            array_blocklens[3]= 4*sizeof(double)*num_elements; // forces (both)
        }else{for(int i=0; i<4; i++)array_blocklens[i]=1;}       
    }else{// for the rank==size-1 when size>1, write the rest
        if(num_elements>0){            
            array_blocklens[0]=sizeof(double)*3*(n_atoms-num_elements*rank); // q_i[3]
            array_blocklens[1]=sizeof(double)*n_types*(n_atoms-num_elements*rank); // x_i[n_types]
            array_blocklens[2]=sizeof(double)*(n_atoms-num_elements*rank); // omega_i
            array_blocklens[3]= 4*sizeof(double)*(n_atoms-num_elements*rank); // forces (both)
        }else{for(int i=0; i<4; i++)array_blocklens[i]=1;} 
    }
    
    // define MPI_type for the data and the file
    MPI_Type_create_struct(4,array_blocklens,data_disps,array_types, &datatype);
    MPI_Type_create_struct(4,array_blocklens,file_disps,array_types, &filetype);    
    // commit MPI_type
    MPI_Type_commit(&datatype);
    MPI_Type_commit(&filetype);
    // write
    MPI_File_set_view(file,  0, MPI_BYTE,filetype,"native",MPI_INFO_NULL);
    MPI_File_write_all(file, 0, 1, datatype, MPI_STATUS_IGNORE);        
    // free MPI_type
    MPI_Type_free(&datatype);
    MPI_Type_free(&filetype);
    
    // close file
    MPI_File_close(&file);
    
    return;
}

void Write_restart_mpi (atom *atoms){
    
    /**
     * \brief Write the file restart in parallel, using MPI.
     * 
     * @param atoms info of atoms
     */
    
    
    // definition of variable
    
    int n_atoms = atoms->n_atoms;
    int n_types = atoms->n_types;
    int errcode;
    MPI_File file;
    char filename[]="results/restart";
    MPI_Offset ofst = (int)0;
    int num_elements;    
    MPI_Datatype datatype, filetype;
    int heading1[4];
    double heading2[9];
    int num_fixed_q=(atoms->fixed_q).size();
    int num_fixed_x=(atoms->fixed_x).size();
    MPI_Aint data_disps[3], file_disps[3]; // q_i[3](double), x_ik[n_types](double), omega_i(double)
    int array_blocklens[3];
    MPI_Datatype array_types[3]={MPI_BYTE,MPI_BYTE,MPI_BYTE};
          
    //Opening the file only rank=0
    
    if(rank==0){   
        errcode = MPI_File_open(MPI_COMM_SELF,filename, MPI_MODE_WRONLY | MPI_MODE_CREATE, MPI_INFO_NULL, &file);
        if(errcode!=MPI_SUCCESS){
            if(rank==0) cout << endl << "We could not create the file restart!!. We proceed to exit.."<<endl;
            MPI_Abort(MPI_COMM_WORLD, errcode);           
        }
        
        MPI_File_close (&file);
    }
    
    MPI_Barrier(MPI_COMM_WORLD);
    
    MPI_File_open(MPI_COMM_WORLD,filename, MPI_MODE_WRONLY, MPI_INFO_NULL, &file);
    
    //heading
    if(rank==0){
        
        heading1[0] = n_atoms;
        heading1[1] = n_types;
        heading1[2] = atoms->diffusion;
        heading1[3] = atoms->maxneigh;
        
        heading2[0] = atoms->r2_cutoff;
        heading2[1] = atoms->skin;
        heading2[2] = atoms->b;
        heading2[3] = atoms->box_x_min;
        heading2[4] = atoms->box_x_max;
        heading2[5] = atoms->box_y_min;
        heading2[6] = atoms->box_y_max;
        heading2[7] = atoms->box_z_min;
        heading2[8] = atoms->box_z_max;
   
        MPI_File_write_at (file, (int)0, heading1, sizeof(int)*4, MPI_BYTE, MPI_STATUS_IGNORE); // n_atoms, n_types, diffusion, maxneigh
        MPI_File_write_at (file, sizeof(int)*4, heading2, sizeof(double)*9, MPI_BYTE, MPI_STATUS_IGNORE); // r_cutoff, skin, b, box_x_min, box_x_max, box_y_min, box_y_max, box_z_min, box_z_max  
        MPI_File_write_at (file, sizeof(int)*4+sizeof(double)*9, &num_fixed_q, sizeof(int), MPI_BYTE, MPI_STATUS_IGNORE);// Number of sites that the position are fixed
        MPI_File_write_at (file, sizeof(int)*5+sizeof(double)*9, &(atoms->fixed_q[0]), num_fixed_q*sizeof(int), MPI_BYTE, MPI_STATUS_IGNORE);// List of sites that the position are fixed
        MPI_File_write_at (file, sizeof(int)*(5+num_fixed_q)+sizeof(double)*9, &num_fixed_x, sizeof(int), MPI_BYTE, MPI_STATUS_IGNORE);// Number of sites that have fixed atomic molar concentration
        MPI_File_write_at (file, sizeof(int)*(6+num_fixed_q)+sizeof(double)*9, &(atoms->fixed_x[0]), num_fixed_x*sizeof(int), MPI_BYTE, MPI_STATUS_IGNORE);// List of sites that have fixed atomic molar concentration
    }  

    num_elements = (int)n_atoms/size; 
    if(num_elements<1){// only when there is more processors than data to write
        if(rank<n_atoms){num_elements=1;}else{num_elements=0;}
    } 
    
    ofst = sizeof(double)*9 + sizeof(int)*(6+num_fixed_q+num_fixed_x);
    
    //for data
    MPI_Get_address ( &atoms->q[rank*num_elements*3],       &data_disps[0]); //q[3]
    MPI_Get_address ( &atoms->x[rank*num_elements*n_types], &data_disps[1]); //x[n_types]
    MPI_Get_address ( &atoms->w[rank*num_elements],         &data_disps[2]); //w
    //for file
    file_disps[0]=ofst+rank*num_elements*sizeof(double)*3; //q[3]
    file_disps[1]=ofst+(n_atoms*3+rank*num_elements*n_types)*sizeof(double); //x_i[n_types]
    file_disps[2]=ofst+(n_atoms*(3+n_types)+rank*num_elements)*sizeof(double); //w
    
    if(rank<size-1 || size==1){           
        if(num_elements>0){
            // define MPI_type for the data
            array_blocklens[0]=sizeof(double)*3*num_elements; // q_i
            array_blocklens[1]=sizeof(double)*n_types*num_elements; // x_i[n_types]
            array_blocklens[2]=sizeof(double)*num_elements; // omega_i     
        }else{for(int i=0; i<3; i++)array_blocklens[i]=1;}
    }else{// for the rank==size-1 when size>1, write the rest
        if(num_elements>0){ 
            array_blocklens[0]=sizeof(double)*3*(n_atoms-num_elements*rank); // q_i
            array_blocklens[1]=sizeof(double)*n_types*(n_atoms-num_elements*rank); // x_i[n_types]
            array_blocklens[2]=sizeof(double)*(n_atoms-num_elements*rank); // omega_i 
        }else{for(int i=0; i<3; i++)array_blocklens[i]=1;}
    }
    
    // define MPI_type for the data anf the file
    MPI_Type_create_struct(3,array_blocklens,data_disps,array_types, &datatype);
    MPI_Type_create_struct(3,array_blocklens,file_disps,array_types, &filetype);      
    // commit MPI_type
    MPI_Type_commit(&datatype);
    MPI_Type_commit(&filetype);    
    // write
    MPI_File_set_view(file,  0,MPI_BYTE,filetype,"native",MPI_INFO_NULL);
    MPI_File_write_all(file, 0, 1, datatype, MPI_STATUS_IGNORE);    
    // free MPI_type
    MPI_Type_free(&datatype);
    MPI_Type_free(&filetype); 
    
    // close file
    MPI_File_close(&file);
    
    return;    
}

/**
 *
 *  Convert files 
 * 
 **/

void ConvertBinaryFiletoTextFile(){
    
    /**
     *  \brief Convert the binary file (q_w_x_binary) of output to a text file.
     * 
     *  No parallel version.
     */
    
    // definition of variable
    MPI_Status status;
    int errcode;
    MPI_File file;
    char filename[]="results/q_w_x_binary";
    MPI_Offset ofst = (int)0, maxofst; 
    int n_types=2;
    int Nat, it;
    double q[3], x[n_types], omega;
    ofstream fichero;
    
    errcode = MPI_File_open(MPI_COMM_SELF,filename, MPI_MODE_RDONLY, MPI_INFO_NULL, &file);
    if(errcode!=MPI_SUCCESS){
        cout << endl << "We could not open the binary file: " << filename << endl;
        return;         
    }

   fichero.open ("results/q_w_x_txt", ios::app);

    MPI_File_get_size(file, &maxofst);
    
    while( ofst < maxofst ){
        
        MPI_File_read_at (file, ofst, &Nat, sizeof(int), MPI_BYTE, &status); // Number of atoms
        ofst += sizeof(int);
        fichero << Nat << endl;
        
        MPI_File_read_at (file, ofst, &it, sizeof(int), MPI_BYTE, &status); // time step or iteration number
        ofst += sizeof(int);
        fichero << it << endl;
        
        for (int i=0; i<Nat;i++){
                           
            MPI_File_read_at (file, ofst+i*3*sizeof(double),                 q, sizeof(double)*3, MPI_BYTE, &status); // q_i[3]            
            MPI_File_read_at (file, ofst+(3*Nat+i*n_types)*sizeof(double),   &x, sizeof(double)*n_types, MPI_BYTE, &status); // x_ik[n_types]            
            MPI_File_read_at (file, ofst+((3+n_types)*Nat+i)*sizeof(double), &omega, sizeof(double), MPI_BYTE, &status); // omega_i 
        
            // id << qx << qy << qz << xi[n_types] << frequency
            fichero<<i+1<<"  "<<q[0]<<"  "<<q[1]<<"  "<<q[2];
            for(int j=0; j<n_types; j++)fichero<<"  "<<x[j];
            fichero<<"  "<<omega<<endl; 
        }
        
        ofst += Nat*sizeof(double)*(4+n_types) ;
    }
    
    cout << endl << "Done converting binary file to text file.";
    
    fichero.close();
    MPI_File_close(&file);

    return;
}

void ConvertBinaryFiletoTextFile_Petsc(){
    
    /**
     *  \brief Convert the binary file of output (q_w_x_Petsc_binary) to a text file.
     * 
     *  No parallel version.
     */
    
    // definition of variable
    int n_types =2;
    MPI_Status status;
    int errcode;
    MPI_File file;
    char filename[]="results/q_w_x_Petsc_binary";
    MPI_Offset ofst = (int)0, maxofst; 
    int Nat, it;
    double q[3], x[n_types], omega, fq[3], fw;
    ofstream fichero;
    
    errcode = MPI_File_open(MPI_COMM_SELF,filename, MPI_MODE_RDONLY, MPI_INFO_NULL, &file);
    if(errcode!=MPI_SUCCESS){
        cout << endl << "We could not open the binary file: " << filename << endl;
        return;         
    }

   fichero.open ("results/q_w_x_Petsc_txt", ios::app);

    MPI_File_get_size(file, &maxofst);
    
    while( ofst < maxofst ){
        
        MPI_File_read_at (file, ofst, &Nat, sizeof(int), MPI_BYTE, &status); // Number of atoms
        ofst += sizeof(int);
        fichero << Nat << endl;
        
        MPI_File_read_at (file, ofst, &it, sizeof(int), MPI_BYTE, &status); // time step or iteration number
        ofst += sizeof(int);
        fichero << it << endl;
        
        for (int i=0; i<Nat;i++){
                                      
            MPI_File_read_at (file, ofst+i*3*sizeof(double), q, sizeof(double)*3, MPI_BYTE, &status); // q_i[3]             
            MPI_File_read_at (file, ofst+(3*Nat+i*n_types)*sizeof(double), &x, sizeof(double)*n_types, MPI_BYTE, &status); // x_ik[n_types]            
            MPI_File_read_at (file, ofst+((3+n_types)*Nat+i)*sizeof(double), &omega, sizeof(double), MPI_BYTE, &status); // omega_i
            MPI_File_read_at (file, ofst+Nat*sizeof(double)*(4+n_types)+3*sizeof(double)*i, fq, sizeof(double)*3, MPI_BYTE, &status); // atomic forces            
            MPI_File_read_at (file, ofst+Nat*sizeof(double)*(7+n_types)+sizeof(double)*i, &fw, sizeof(double), MPI_BYTE, &status); // frequency force 
        
            // id << qx << qy << qz << xi[n_types] << frequency << atomic force << frequency force
            fichero<<i+1<<"  "<<q[0]<<"  "<<q[1]<<"  "<<q[2];
            for(int j=0; j<n_types; j++)fichero<<"  "<<x[j];
            fichero<<"  "<<omega<<"  "<<fq[0]<<"  "<<fq[1]<<"  "<<fq[2]<<"  "<<fw<<endl; 
        }
        
        ofst += Nat*sizeof(double)*(8+n_types);
    }
    
    cout << endl << "Done converting binary file to text file.";
    
    fichero.close();
    MPI_File_close(&file);

    return;
}

/**
 *
 *
 */

void Write_Diffusion_Coeff(int iter, double coeff){
    
    ofstream fichero;
    fichero.open("results/DiffusionCoeff", ios::app);
    
    if(iter == 1)fichero<<"Iteration"<<"    "<<" Diffusion coeff.:";
    fichero<<endl<<iter<<"   "<<coeff;

    fichero.close();
    
    return;
}

void Write_Diffusive_Step(int iter, double coeff){
    
    ofstream fichero;
    fichero.open("results/DiffusiveStep", ios::app);
    
    if(iter == 1)fichero<<"Iteration"<<"    "<<"Diffusive step";
    fichero<<endl<<iter<<"   "<<coeff;

    fichero.close();
    
    return;
}