#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <iostream>

#include "atom.hpp"

using namespace std;

extern int size;
extern int rank;
extern int sc_coeff;
extern double matrix_translation[27][3];

static int n_supercell( atom *atoms){
    
/**
 * \brief function to compute the total # of sites in the supercell
 * @param atoms info
 * @return total number of atoms in the supercell
 */
    
    int n_atoms = atoms->n_atoms;
    int cont = n_atoms;
    
    double box_x_min = atoms->box_x_min;
    double box_y_min = atoms->box_y_min;
    double box_z_min = atoms->box_z_min;
    double box_x_max = atoms->box_x_max;
    double box_y_max = atoms->box_y_max;
    double box_z_max = atoms->box_z_max;
    
    //double r_cutoff = sqrt(atoms->r_cutoff) + atoms->skin;
    double r_cutoff = sqrt(atoms->r2_cutoff)*(double)sc_coeff;    
    
    double vector[3], desp[3], lx = fabs(box_x_max-box_x_min), ly = fabs(box_y_max-box_y_min), lz = fabs(box_z_max-box_z_min);
    double Box_x_min_star = box_x_min - r_cutoff;
    double Box_x_max_star = box_x_max + r_cutoff;
    double Box_y_min_star = box_y_min - r_cutoff;
    double Box_y_max_star = box_y_max + r_cutoff;
    double Box_z_min_star = box_z_min - r_cutoff;
    double Box_z_max_star = box_z_max + r_cutoff;

    for (int n=1; n<27; n++){
    
        desp[0] = matrix_translation[n][0]*lx;
        desp[1] = matrix_translation[n][1]*ly;
        desp[2] = matrix_translation[n][2]*lz;    
    
        for (int l=0; l<n_atoms; l++ ){
    
            vector[0] = desp[0] + atoms->q[l*3+0];
            vector[1] = desp[1] + atoms->q[l*3+1];
            vector[2] = desp[2] + atoms->q[l*3+2];
        
            if ( Box_x_min_star <= vector[0] && Box_x_max_star >= vector[0] && 
                 Box_y_min_star <= vector[1] && Box_y_max_star >= vector[1] && 
                 Box_z_min_star <= vector[2] && Box_z_max_star >= vector[2]) {cont++;}
    
        }
    }

    return cont;
}

void supercell ( atom *atoms){

/**
 * \brief  function to compute supercell (parallel version)
 * @param Sites
 * @param Ntotal_sc
 * @param Ntotal
 * @param rank
 * 
 */

    int *master, *trans_index, n_atoms_sc, n_atoms;
    double *q_i; 

    n_atoms_sc = n_supercell(atoms);
    atoms->n_atoms_sc=n_atoms_sc;
    n_atoms = atoms->n_atoms;
    
    double box_x_min = atoms->box_x_min;
    double box_y_min = atoms->box_y_min;
    double box_z_min = atoms->box_z_min;
    double box_x_max = atoms->box_x_max;
    double box_y_max = atoms->box_y_max;
    double box_z_max = atoms->box_z_max;
    
    //double r_cutoff = sqrt(atoms->r_cutoff) + atoms->skin;
    double r_cutoff = sqrt(atoms->r2_cutoff)*(double)sc_coeff;

    // allocate memory
    
    q_i = (double*) malloc(3*n_atoms_sc*sizeof(double));
    master = (int*) malloc(n_atoms_sc*sizeof(int));
    trans_index = (int*) malloc( (n_atoms_sc-n_atoms)*sizeof(int) );

    double vector[3], desp[3], lx = fabs(box_x_max-box_x_min), ly = fabs(box_y_max-box_y_min), lz = fabs(box_z_max-box_z_min);
    double Box_x_min_star = box_x_min-r_cutoff;
    double Box_x_max_star = box_x_max+r_cutoff;
    double Box_y_min_star = box_y_min-r_cutoff;
    double Box_y_max_star = box_y_max+r_cutoff;
    double Box_z_min_star = box_z_min-r_cutoff;
    double Box_z_max_star = box_z_max+r_cutoff;
    int cont=0;

    for (int n=0; n<27; n++){
    
        desp[0] = matrix_translation[n][0]*lx;
        desp[1] = matrix_translation[n][1]*ly;
        desp[2] = matrix_translation[n][2]*lz;
    
        for (int l=0; l<n_atoms; l++ ){    
        
            vector[0] = desp[0] + atoms->q[l*3+0];
            vector[1] = desp[1] + atoms->q[l*3+1];
            vector[2] = desp[2] + atoms->q[l*3+2];
        
            if ( Box_x_min_star <= vector[0] && Box_x_max_star >= vector[0] && 
                 Box_y_min_star <= vector[1] && Box_y_max_star >= vector[1] && 
                 Box_z_min_star <= vector[2] && Box_z_max_star >= vector[2]){
                            
                    master[cont] = l;
                    if(cont>=n_atoms) trans_index[cont-n_atoms]=n;
                   
                    q_i[cont*3+0]=vector[0];
                    q_i[cont*3+1]=vector[1];
                    q_i[cont*3+2]=vector[2];
                    cont++;
            }
    
        }
    }

    // delete
    
    free(atoms->q);
    free(atoms->master);
    free(atoms->trans_index);

    // update
    
    atoms->q = q_i;
    atoms->master = master;
    atoms->trans_index = trans_index;

    if(rank==0) cout << endl << "Number of atoms in the supercell: " << n_atoms_sc;

    return;
}
