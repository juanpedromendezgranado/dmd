#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <iostream>

#include "atom.hpp"

using namespace std;

void boundary_conditions(atom *atoms){
    
    /**
     * \brief This function is to apply boundary conditions.
     * 
     * The box is a rectangular parallelepiped, then we have to specify the following data:
     * x_min, x_max
     * y_min, y_max
     * z_min, z_max
     * 
     * @param sites atoms info (sites could be either the cell or the supercell).
     * @param Ntotal_sites total number of atoms (no supercell).
     * 
     */
        
    double position[3];    
    int i;
    int n_atoms = atoms->n_atoms;
    
    double box_x_min = atoms->box_x_min;
    double box_y_min = atoms->box_y_min;
    double box_z_min = atoms->box_z_min;
    double box_x_max = atoms->box_x_max;
    double box_y_max = atoms->box_y_max;
    double box_z_max = atoms->box_z_max;
    
    for(i=0; i<n_atoms; i++){        
        
        position[0]=atoms->q[i*3+0];
        position[1]=atoms->q[i*3+1];
        position[2]=atoms->q[i*3+2];
        
        // Condition 1 x > x_min
        if(atoms->q[i*3+0] <= box_x_min) position[0] += fabs(box_x_max-box_x_min);   
        // Condition 2 x < x_max
        if(atoms->q[i*3+0] > box_x_max)  position[0] -= fabs(box_x_max-box_x_min);    
        // Condition 3 y > y_min
        if(atoms->q[i*3+1] <= box_y_min) position[1] += fabs(box_y_max-box_y_min);    
        // Condition 4 y < y_max
        if(atoms->q[i*3+1] > box_y_max)  position[1] -= fabs(box_y_max-box_y_min);
        // Condition 5 z > z_min
        if(atoms->q[i*3+2] <= box_z_min) position[2] += fabs(box_z_max-box_z_min);
        // Condition 6 z < z_max
        if(atoms->q[i*3+2] > box_z_max)  position[2] -= fabs(box_z_max-box_z_min);               
        
        atoms->q[i*3+0] = position[0];
        atoms->q[i*3+1] = position[1];
        atoms->q[i*3+2] = position[2];
        
        // Check error. This is important, because it could a error later
        if( position[0] < box_x_min || position[0] > box_x_max ||  position[1] < box_y_min || position[1] > box_y_max ||  position[2] < box_z_min || position[2] > box_z_max){
            cout << endl << " Fatal error: In Function Apply_BoundaryConditions(), atom "<< i+1 <<" which its current position is "<< position[0] << ", "<<position[1]<<", "<<position[2]<<" has been lost.";
            exit(1);
        } 
    
    }// end of loop over i
    
    return;    

}
