#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>  //std::cout//std::cin
#include <math.h>
#include "eam_potential.hpp"
#include "atom.hpp"

using namespace std;
using namespace MPI;
extern int size;
extern int rank;

void init_EAM(EAMPotential *eam, EAMMaterials eam_material){
	
    int i, alloy=0, error;  
    FILE* f_eam;
    
    int n_rho; double dr_rho;
    int n_embed; double drho_embed;
    int n_pair; double dr_pair;

    switch ( eam_material ){
        case Mg:
            f_eam = fopen( "input/eam_Mg.dat", "r" );            
            break;
        case Al:
            f_eam = fopen( "input/eam_Al.dat", "r" );
            break;
        case MgAl:
            f_eam = fopen( "input/eam_MgAl.dat", "r" );
            alloy=1;
            break;
	default:
            printf("init_EAM: Unknown material\n");
            exit(0);  
    }   
    
    if(!alloy){
        
        error=fscanf( f_eam, "%lf %lf %lf", &eam->mass, &eam->factor, &eam->r_cutoff ); // the r_cutoff is useless here, we read it again when reading the position of atoms
    
        error=fscanf( f_eam, "%d %lf", &n_rho, &dr_rho );
        init_spline( &eam->rho, n_rho, dr_rho );

        for( i = 0; i < n_rho; i++ ){
            error=fscanf( f_eam, "%lf %lf %lf %lf %lf",
                    &eam->rho.x[i], &eam->rho.d[i], &eam->rho.c[i], &eam->rho.b[i], &eam->rho.a[i] );
        }

        error=fscanf( f_eam, "%d %lf", &n_embed, &drho_embed );
        init_spline( &eam->embed, n_embed, drho_embed );

        for( i = 0; i < n_embed; i++ ){
            error=fscanf( f_eam, "%lf %lf %lf %lf %lf",
                    &eam->embed.x[i], &eam->embed.d[i], &eam->embed.c[i], &eam->embed.b[i], &eam->embed.a[i] );
        }
    }else{
        error=fscanf( f_eam, "%lf", &eam->r_cutoff ); // the r_cutoff is useless here, we read it again when reading the position of atoms
        init_spline( &eam->rho, 1 , 0);
        init_spline( &eam->embed, 1, 0);
    }

    error=fscanf( f_eam, "%d %lf", &n_pair, &dr_pair );
    init_spline( &eam->pair, n_pair, dr_pair );

    for( i = 0; i < n_pair; i++ ){
	error=fscanf( f_eam, "%lf %lf %lf %lf %lf",
		&eam->pair.x[i], &eam->pair.d[i], &eam->pair.c[i], &eam->pair.b[i], &eam->pair.a[i] );
    }

    fclose(f_eam); 
}

void destroy_EAM( EAMPotential* eam ){
    destroy_spline( &eam->embed );
    destroy_spline( &eam->rho );
    destroy_spline( &eam->pair );
}

double EAM_Potential( EAMPotential *eam[2][2], atom* atoms){

    int i, j, j_master, k;
    int dim = 3;

    double en=0.0, all_en=0.0, r2ij=0.0, rij =0.0;
    
    double* q = atoms->q;
    double* x = atoms->x;
    int*    numneigh = atoms->numneigh;
    int*    neigh = atoms->neigh;
    int*    master = atoms->master;
    int     maxneigh = atoms->maxneigh;
    int     n_atoms = atoms->n_atoms;
    int     n_types = atoms->n_types;

    double* qi = (double*) malloc( dim * sizeof( double ) );
    double* qj = (double*) malloc( dim * sizeof( double ) );
    double* temp_ed = (double*) malloc( n_atoms * sizeof( double ) );
    double* ed = (double*) malloc( n_atoms * sizeof( double ) );
    
    double  pair_mat[n_types][n_types];
    
    //
    // Compute the electron densities for all sites
    //
    
    for(i=0; i<n_atoms; i++) temp_ed[i]=0.0;
    
    for(i=rank; i<n_atoms; i+=size){
        
        qi[0] = q[i*dim + 0];
        qi[1] = q[i*dim + 1];
        qi[2] = q[i*dim + 2];
        
        for(k=0; k<numneigh[i]; k++){
            
            j = neigh[i*maxneigh+k]; // if(i>=j)continue;
            j_master = master[j];
            
            qj[0] = q[j*dim + 0];
            qj[1] = q[j*dim + 1];
            qj[2] = q[j*dim + 2];

            r2ij = (qi[0]-qj[0])*(qi[0]-qj[0]) + (qi[1]-qj[1])*(qi[1]-qj[1]) + (qi[2]-qj[2])*(qi[2]-qj[2]);

            if( r2ij < atoms->r2_cutoff ){
                
                rij = sqrt(r2ij);
                
                for(int type=0; type<n_types; type++){
                    double rhoj = cubic_spline( &(eam[type][type]->rho), rij );
                    temp_ed[i] += rhoj*x[j_master*n_types+type];
                    if(j< n_atoms) temp_ed[j] += rhoj*x[i*n_types+type];
                }
            }
        }
    }
    
    MPI_Allreduce(temp_ed, ed, n_atoms, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    
    //
    // Compute the energy = embedding energy + pair energy 
    //
    
    for(i=rank; i<n_atoms; i+=size){       
    
        qi[0] = q[i*dim + 0];
        qi[1] = q[i*dim + 1];
        qi[2] = q[i*dim + 2];
        
        // Compute the embedding energy
        
        for(int i_type=0; i_type<n_types; i_type++) en += cubic_spline( &(eam[i_type][i_type]->embed), ed[i]/eam[i_type][i_type]->factor )*x[i*n_types+i_type];
        
        // Compute the pair energy i-j
        
        for(k=0; k<numneigh[i]; k++){
            
            j = neigh[i*maxneigh+k]; // if(i>=j)continue;
            j_master = master[j];
        
            qj[0] = q[j*dim + 0];
            qj[1] = q[j*dim + 1];
            qj[2] = q[j*dim + 2];
        
            r2ij = (qi[0]-qj[0])*(qi[0]-qj[0]) + (qi[1]-qj[1])*(qi[1]-qj[1]) + (qi[2]-qj[2])*(qi[2]-qj[2]);

            if( r2ij < atoms->r2_cutoff ){
                
                rij = sqrt(r2ij);
                
                for(int i_type=0; i_type<n_types; i_type++){
                    for(int j_type=i_type; j_type<n_types; j_type++){                        
                        double val = cubic_spline( &(eam[i_type][j_type]->pair), rij );
                        pair_mat[i_type][j_type] = val;
                        pair_mat[j_type][i_type] = val;
                    }
                }                
                
                if(j<n_atoms){    
                    for(int i_type=0; i_type<n_types; i_type++){
                        for(int j_type=0; j_type<n_types; j_type++){
                            en += pair_mat[i_type][j_type]*x[i*n_types+i_type]*x[j_master*n_types+j_type];
                        }
                    }
                }else{
                    for(int i_type=0; i_type<n_types; i_type++){
                        for(int j_type=0; j_type<n_types; j_type++){
                            en += 0.5*pair_mat[i_type][j_type]*x[i*n_types+i_type]*x[j_master*n_types+j_type];
                        }
                    }
                }
                
            }
        }
    }
    
    MPI_Allreduce(&en, &all_en, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

    free( qi );
    free( qj );
    free (ed);
    free (temp_ed);

    return all_en;
}

void d_EAM_Potential( EAMPotential *eam[2][2], atom* atoms, double *all_f ){

    int i, j, k, j_master;
    int dim = 3;

    double fij = 0.0;
    double rij = 0.0, r2ij=0.0;
    
    double* q = atoms->q;
    double* x = atoms->x;
    int*    numneigh = atoms->numneigh;
    int*    neigh = atoms->neigh;
    int*    master = atoms->master;
    int     maxneigh = atoms->maxneigh;
    int n_atoms = atoms->n_atoms;
    int n_types = atoms->n_types;

    double* qi = (double*) malloc( dim * sizeof( double ) );
    double* qj = (double*) malloc( dim * sizeof( double ) );
    double* ed = (double*) malloc( n_atoms * sizeof( double ) );
    double* temp_ed = (double*) malloc( n_atoms * sizeof( double ) );
    double* dF = (double*) malloc( n_types * n_atoms * sizeof( double ) );
    double* temp_dF = (double*) malloc( n_types * n_atoms * sizeof( double ) );
    double* f = (double*) malloc( dim * n_atoms * sizeof( double ) );
    
    double d_pair_mat[n_types][n_types];
    double d_rho_mat[n_types];
    
    // Initialization
    for(i=0; i<n_atoms; i++) temp_ed[i]=0.0;
    for(i=0; i<n_atoms*n_types; i++) temp_dF[i]=0.0;    
    for(i=0; i<n_atoms*dim; i++)f[i]=0.0;
    
    //
    // Compute the electron densities for all sites
    //    
    
    for(i=rank; i<n_atoms; i+=size){
        
        qi[0] = q[i*dim + 0];
        qi[1] = q[i*dim + 1];
        qi[2] = q[i*dim + 2];
        
        for(k=0; k<numneigh[i]; k++){
            
            j = neigh[i*maxneigh+k]; // if(i>=j)continue;
            j_master = master[j];
            
            qj[0] = q[j*dim + 0];
            qj[1] = q[j*dim + 1];
            qj[2] = q[j*dim + 2];

            r2ij = (qi[0]-qj[0])*(qi[0]-qj[0]) + (qi[1]-qj[1])*(qi[1]-qj[1]) + (qi[2]-qj[2])*(qi[2]-qj[2]);

            if( r2ij < atoms->r2_cutoff ){
                
                rij = sqrt(r2ij);
                
                for(int type=0; type<n_types; type++){
                    double rhoj = cubic_spline( &(eam[type][type]->rho), rij );
                    temp_ed[i] += rhoj*x[j_master*n_types+type];
                    if(j< n_atoms) temp_ed[j_master] += rhoj*x[i*n_types+type];
                }
            }
        }
    }
    
    MPI_Allreduce(temp_ed, ed, n_atoms, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    
    //
    // Compute the derivatives of embedding function with respect to the electron density
    //
    
    for(i=rank; i<n_atoms; i+=size){        
        for(int type=0; type<n_types; type++){ 
            temp_dF[i*n_types+type] = d_cubic_spline( &(eam[type][type]->embed), ed[i]/eam[type][type]->factor )/eam[type][type]->factor;
        }
    }
    
    MPI_Allreduce(temp_dF, dF, n_types*n_atoms, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    
    //
    // Compute the pair forces
    //

    for(i=rank; i<n_atoms; i+=size){
        
        qi[0] = q[i*dim + 0];
        qi[1] = q[i*dim + 1];
        qi[2] = q[i*dim + 2];
        
        for(k=0; k<numneigh[i]; k++){
            
            j = neigh[i*maxneigh+k]; // if(i>=j)continue;
            j_master = master[j];
            
            fij =0;
            
            qj[0] = q[j*dim + 0];
            qj[1] = q[j*dim + 1];
            qj[2] = q[j*dim + 2];

            r2ij = (qi[0]-qj[0])*(qi[0]-qj[0])+(qi[1]-qj[1])*(qi[1]-qj[1])+(qi[2]-qj[2])*(qi[2]-qj[2]);

            if( r2ij < atoms->r2_cutoff ){
                
                rij = sqrt(r2ij);
                
                for(int i_type=0; i_type<n_types; i_type++){
                    d_rho_mat[i_type]= d_cubic_spline( &(eam[i_type][i_type]->rho), rij );
                    for(int j_type=i_type; j_type<n_types; j_type++){
                        double val = d_cubic_spline( &(eam[i_type][j_type]->pair), rij );
                        d_pair_mat[i_type][j_type]=val;
                        d_pair_mat[j_type][i_type]=val;                         
                    }                   
                }
              	
                for(int i_type=0; i_type<n_types; i_type++){                    
                    for(int j_type=0; j_type<n_types; j_type++){
                    
                        fij += (                          
                            dF[i*n_types+i_type]*d_rho_mat[j_type] + 
                            dF[j_master*n_types+j_type]*d_rho_mat[i_type] +
                            d_pair_mat[i_type][j_type]                       
                         )*x[i*n_types+i_type]*x[j_master*n_types+j_type];
                    }
                }
                
                fij /= rij;
            
                f[i*dim+0] += fij*(qj[0]-qi[0]);
                f[i*dim+1] += fij*(qj[1]-qi[1]);
                f[i*dim+2] += fij*(qj[2]-qi[2]);
                
                if(j< n_atoms){
                    f[j*dim+0] -= fij*(qj[0]-qi[0]);
                    f[j*dim+1] -= fij*(qj[1]-qi[1]);
                    f[j*dim+2] -= fij*(qj[2]-qi[2]);
                }
            
            }            
        }    
    }
    
    MPI_Allreduce(f, all_f, dim*n_atoms, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

    free(qi);
    free(qj);
    free(ed);
    free(temp_ed);
    free(dF);
    free(temp_dF);
    free(f);

    return;
}

void tr_d2_EAM_Potential( EAMPotential *eam[2][2], atom* atoms, double *all_f ){

    int i, j, j_master, k;
    int dim = 3;

    double fij = 0.0, dfij=0.0;
    double rij = 0.0, irij=0.0, ir2ij=0.0, ir3ij=0.0, r2ij=0.0, r2ij_1 = 0.0, r2ij_2 = 0.0, r2ij_3= 0.0, phi_1 = 0.0, phi_2 = 0.0, phi_3 = 0.0;
    
    double* q = atoms->q;
    double* x = atoms->x;
    int*    numneigh = atoms->numneigh;
    int*    neigh = atoms->neigh;
    int*    master = atoms->master;
    int     maxneigh = atoms->maxneigh;
    int n_atoms = atoms->n_atoms;
    int n_types = atoms->n_types;

    double* qi = (double*) malloc( dim * sizeof( double ) );
    double* qj = (double*) malloc( dim * sizeof( double ) );
    double* ed = (double*) malloc( n_atoms * sizeof( double ) );
    double* temp_ed =  (double*) malloc( n_atoms * sizeof( double ) );
//////    double* dF =       (double*) malloc( n_types * n_atoms * sizeof( double ) );
//////    double* temp_dF =  (double*) malloc( n_types * n_atoms * sizeof( double ) );
//////    double* d2F =      (double*) malloc( n_types * n_atoms * sizeof( double ) );
//////    double* temp_d2F = (double*) malloc( n_types * n_atoms * sizeof( double ) );
    double* dF       =  new double[n_types * n_atoms];
    double* temp_dF  =  new double[n_types * n_atoms];
    double* d2F      =  new double[n_types * n_atoms];
    double* temp_d2F =  new double[n_types * n_atoms];
//////    double temp_d2F[ n_types * n_atoms];
//////    double d2F[ n_types * n_atoms];
//////    double dF[ n_types * n_atoms];
//////    double temp_dF[ n_types * n_atoms];
    double* f = (double*) malloc( n_atoms * sizeof( double ) );
    
    double d_pair_mat[n_types][n_types], d2_pair_mat[n_types][n_types];
    double d_rho_mat[n_types], d2_rho_mat[n_types];
    
    // Initialization
    for(i=0; i<n_atoms; i++) temp_ed[i]=0.0;
    for(i=0; i<n_atoms*n_types; i++) {temp_dF[i]=0.0; temp_d2F[i]=0.0;}
    for(i=0; i<n_atoms; i++) f[i]=0.0;
    
    //
    // Compute the electron densities for all sites
    //
    
    for(i=rank; i<n_atoms; i+=size){
        
        qi[0] = q[i*dim + 0];
        qi[1] = q[i*dim + 1];
        qi[2] = q[i*dim + 2];
        
        for(k=0; k<numneigh[i]; k++){
            
            j = neigh[i*maxneigh+k]; // if(i>=j)continue;
            j_master = master[j];
            
            qj[0] = q[j*dim + 0];
            qj[1] = q[j*dim + 1];
            qj[2] = q[j*dim + 2];

            r2ij = (qi[0]-qj[0])*(qi[0]-qj[0]) + (qi[1]-qj[1])*(qi[1]-qj[1]) + (qi[2]-qj[2])*(qi[2]-qj[2]);

            if( r2ij < atoms->r2_cutoff ){
                
                rij = sqrt(r2ij);
                
                for(int type=0; type<n_types; type++){
                    double rhoj = cubic_spline( &(eam[type][type]->rho), rij );
                    temp_ed[i] += rhoj*x[j_master*n_types+type];
                    if(j< n_atoms)temp_ed[j_master] += rhoj*x[i*n_types+type];
                }
            }
        }
    }
    
    MPI_Allreduce(temp_ed, ed, n_atoms, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    
    //
    // Compute the derivatives of embedding function with respect to the electron density
    //
    
    for(i=rank; i<n_atoms; i+=size){        
        for(int type=0; type<n_types; type++){ 
            temp_dF[i*n_types+type]  = d_cubic_spline( &(eam[type][type]->embed),  ed[i]/eam[type][type]->factor )/eam[type][type]->factor;
            temp_d2F[i*n_types+type] = d2_cubic_spline( &(eam[type][type]->embed), ed[i]/eam[type][type]->factor )/( eam[type][type]->factor * eam[type][type]->factor );
        }
    }
    
    MPI_Allreduce(temp_dF, dF, n_types*n_atoms, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    MPI_Allreduce(temp_d2F, d2F, n_types*n_atoms, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    
    //
    // Compute the pair forces
    //

    for(i=rank; i<n_atoms; i+=size){
        
        qi[0] = q[i*dim + 0];
        qi[1] = q[i*dim + 1];
        qi[2] = q[i*dim + 2];
    
        for(k=0; k<numneigh[i]; k++){
            
            j = neigh[i*maxneigh+k]; // if(i>=j)continue;
            j_master = master[j];
            
            fij =0;
            dfij =0;
            
            qj[0] = q[j*dim + 0];
            qj[1] = q[j*dim + 1];
            qj[2] = q[j*dim + 2];

            r2ij = (qi[0]-qj[0])*(qi[0]-qj[0])+(qi[1]-qj[1])*(qi[1]-qj[1])+(qi[2]-qj[2])*(qi[2]-qj[2]);

            if( r2ij < atoms->r2_cutoff ){
                
                rij = sqrt(r2ij);
                
                for(int i_type=0; i_type<n_types; i_type++){
                    d_rho_mat[i_type]= d_cubic_spline( &(eam[i_type][i_type]->rho), rij );
                    d2_rho_mat[i_type]= d2_cubic_spline( &(eam[i_type][i_type]->rho), rij );
                    for(int j_type=i_type; j_type<n_types; j_type++){
                        double val = d_cubic_spline( &(eam[i_type][j_type]->pair), rij );
                        double dval = d2_cubic_spline( &(eam[i_type][j_type]->pair), rij );
                        d_pair_mat[i_type][j_type]=val;
                        d_pair_mat[j_type][i_type]=val;
                        d2_pair_mat[i_type][j_type]=dval;
                        d2_pair_mat[j_type][i_type]=dval; 
                    }                   
                }
              	
                for(int i_type=0; i_type<n_types; i_type++){                    
                    for(int j_type=0; j_type<n_types; j_type++){
                    
                        fij += (                          
                            dF[i*n_types+i_type]*d_rho_mat[j_type] + 
                            dF[j_master*n_types+j_type]*d_rho_mat[i_type] +
                            d_pair_mat[i_type][j_type]                       
                         )*x[i*n_types+i_type]*x[j_master*n_types+j_type];
                        
                        dfij += (                          
                            d2F[i*n_types+i_type]*d_rho_mat[j_type]*d_rho_mat[j_type] + dF[i*n_types+i_type]*d2_rho_mat[j_type] +
                            d2F[j_master*n_types+j_type]*d_rho_mat[i_type]*d_rho_mat[i_type] + dF[j_master*n_types+j_type]*d2_rho_mat[i_type] +
                            d2_pair_mat[i_type][j_type]                       
                         )*x[i*n_types+i_type]*x[j_master*n_types+j_type];
                    }
                }
                 
                r2ij_1=(qj[0]-qi[0])*(qj[0]-qi[0]);
                r2ij_2=(qj[1]-qi[1])*(qj[1]-qi[1]);
                r2ij_3=(qj[2]-qi[2])*(qj[2]-qi[2]);
                irij =1.0/rij;
                ir2ij=irij/rij;
                ir3ij=ir2ij/rij;
                
                phi_1 = dfij*r2ij_1*ir2ij + fij*( irij - r2ij_1*ir3ij );
                phi_2 = dfij*r2ij_2*ir2ij + fij*( irij - r2ij_2*ir3ij );
                phi_3 = dfij*r2ij_3*ir2ij + fij*( irij - r2ij_3*ir3ij );
            
                f[i] += phi_1+phi_2+phi_3;                
                if(j<n_atoms) f[j_master] += phi_1+phi_2+phi_3;
            
            }            
        }    
    }
    
    MPI_Allreduce(f, all_f, n_atoms, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    
    free(qi);
    free(qj);
    free(ed);
    free(temp_ed);
//////    free(dF);
//////    free(temp_dF);
//////    free(d2F);
//////    free(temp_d2F);
    delete[] dF;
    delete[] temp_dF;
    delete[] d2F;
    delete[] temp_d2F;
    free(f);

    return;
}

void dx_EAM_Potential( EAMPotential *eam[2][2], atom* atoms, double *all_f ){

    int i, j, j_master, k;
    int dim = 3;
    double rij = 0.0, r2ij=0.0;
    
    double* q = atoms->q;
    double* x = atoms->x;
    int*    numneigh = atoms->numneigh;
    int*    neigh = atoms->neigh;
    int*    master = atoms->master;
    int     maxneigh = atoms->maxneigh;
    int n_atoms = atoms->n_atoms;
    int n_types = atoms->n_types;

    double* qi = (double*) malloc( dim * sizeof( double ) );
    double* qj = (double*) malloc( dim * sizeof( double ) );
    double* ed = (double*) malloc( n_atoms * sizeof( double ) );
    double* temp_ed = (double*) malloc( n_atoms * sizeof( double ) );
    double* dF = (double*) malloc( n_types * n_atoms * sizeof( double ) );
    double* temp_dF = (double*) malloc( n_types * n_atoms * sizeof( double ) );
    double* f = (double*) malloc( n_types * n_atoms * sizeof( double ) );
    
    double d_pair_mat[n_types][n_types];
    
    // Initialization
    for(i=0; i<n_atoms; i++) temp_ed[i]=0.0;
    for(i=0; i<n_atoms*n_types; i++) temp_dF[i]=0.0;
    for(i=0; i<n_atoms*n_types; i++) f[i]=0.0;
    
    //
    // Compute the electron densities for all sites
    //  
    
    for(i=rank; i<n_atoms; i+=size){
        
        qi[0] = q[i*dim + 0];
        qi[1] = q[i*dim + 1];
        qi[2] = q[i*dim + 2];
        
        for(k=0; k<numneigh[i]; k++){
            
            j = neigh[i*maxneigh+k]; // if(i>=j)continue;
            j_master = master[j];
            
            qj[0] = q[j*dim + 0];
            qj[1] = q[j*dim + 1];
            qj[2] = q[j*dim + 2];

            r2ij = (qi[0]-qj[0])*(qi[0]-qj[0]) + (qi[1]-qj[1])*(qi[1]-qj[1]) + (qi[2]-qj[2])*(qi[2]-qj[2]);

            if( r2ij < atoms->r2_cutoff ){
                
                rij = sqrt(r2ij);
                
                for(int type=0; type<n_types; type++){
                    double rhoj = cubic_spline( &(eam[type][type]->rho), rij );
                    temp_ed[i] += rhoj*x[j_master*n_types+type];
                    if(j< n_atoms) temp_ed[j_master] += rhoj*x[i*n_types+type];
                }
            }
        }
    }
    
    MPI_Allreduce(temp_ed, ed, n_atoms, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    
    //
    // Compute the derivatives of embedding function with respect to the electron density
    //
    
    for(i=rank; i<n_atoms; i+=size){        
        for(int type=0; type<n_types; type++){ 
            temp_dF[i*n_types+type] = d_cubic_spline( &(eam[type][type]->embed), ed[i]/eam[type][type]->factor )/eam[type][type]->factor;
        }
    }
    
    MPI_Allreduce(temp_dF, dF, n_types*n_atoms, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD); 
        
    //
    // Compute the pair forces
    //

    for(i=rank; i<n_atoms; i+=size){
        
        qi[0] = q[i*dim + 0];
        qi[1] = q[i*dim + 1];
        qi[2] = q[i*dim + 2];
        
        for(int type=0; type<n_types; type++) f[i*n_types+type] += cubic_spline( &(eam[type][type]->embed), ed[i]/eam[type][type]->factor );
        
        for(k=0; k<numneigh[i]; k++){
                       
            double val_i=0.0, val_j=0.0;
            
            j = neigh[i*maxneigh+k]; // if(i>=j)continue;
            j_master = master[j];
            
            qj[0] = q[j*dim + 0];
            qj[1] = q[j*dim + 1];
            qj[2] = q[j*dim + 2];

            r2ij = (qi[0]-qj[0])*(qi[0]-qj[0])+(qi[1]-qj[1])*(qi[1]-qj[1])+(qi[2]-qj[2])*(qi[2]-qj[2]);

            if( r2ij < atoms->r2_cutoff ){
                
                rij = sqrt(r2ij);
                
                for(int l=0; l<n_types; l++){
                    val_j += x[j_master*n_types+l]*dF[j_master*n_types+l];
                    val_i += x[i*n_types+l]*dF[i*n_types+l];
                    for(int k=l; k<n_types; k++){
                        double val = cubic_spline( &(eam[l][k]->pair), rij );
                        d_pair_mat[l][k]=val;
                        d_pair_mat[k][l]=val;                         
                    }                   
                }
              	
                for(int l=0; l<n_types; l++){
                    double rho = cubic_spline( &(eam[l][l]->rho), rij );
                    f[i*n_types+l]+=val_j*rho;
                    if(j< n_atoms) f[j_master*n_types+l]+=val_i*rho;
                    
                    for(int k=0; k<n_types; k++){
                        f[i*n_types+l]+=x[j_master*n_types+k]*d_pair_mat[l][k];
                        if(j< n_atoms) f[j_master*n_types+l]+=x[i*n_types+k]*d_pair_mat[l][k]; 
                    }
                }            
            
            }            
        }    
    }
    
    MPI_Allreduce(f, all_f, n_types*n_atoms, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

    free(qi);
    free(qj);
    free(ed);
    free(temp_ed);
    free(dF);
    free(temp_dF);
    free(f);

    return;
}

void Virial_Stresses_EAM_Potential( EAMPotential *eam[2][2], atom* atoms, double *all_f ){

    int i, j, k, j_master;
    int dim = 3;

    double fij = 0.0;
    double rij = 0.0, r2ij=0.0;
    
    double* q = atoms->q;
    double* x = atoms->x;
    int*    numneigh = atoms->numneigh;
    int*    neigh = atoms->neigh;
    int*    master = atoms->master;
    int     maxneigh = atoms->maxneigh;
    int n_atoms = atoms->n_atoms;
    int n_types = atoms->n_types;

    double* qi = (double*) malloc( dim * sizeof( double ) );
    double* qj = (double*) malloc( dim * sizeof( double ) );
    double* ed = (double*) malloc( n_atoms * sizeof( double ) );
    double* temp_ed = (double*) malloc( n_atoms * sizeof( double ) );
    double* dF = (double*) malloc( n_types * n_atoms * sizeof( double ) );
    double* temp_dF = (double*) malloc( n_types * n_atoms * sizeof( double ) );
    double* f = (double*) malloc( 6*n_atoms*sizeof( double ) );
    
    double d_pair_mat[n_types][n_types];
    double d_rho_mat[n_types];
    
    // Initialization
    for(i=0; i<n_atoms; i++) temp_ed[i]=0.0;
    for(i=0; i<n_atoms*n_types; i++)temp_dF[i]=0.0;    
    for(i=0; i<n_atoms*6; i++)f[i]=0.0;
    
    //
    // Compute the electron densities for all sites
    //    
    
    for(i=rank; i<n_atoms; i+=size){
        
        qi[0] = q[i*dim + 0];
        qi[1] = q[i*dim + 1];
        qi[2] = q[i*dim + 2];
        
        for(k=0; k<numneigh[i]; k++){
            
            j = neigh[i*maxneigh+k]; // if(i>=j)continue;
            j_master = master[j];
            
            qj[0] = q[j*dim + 0];
            qj[1] = q[j*dim + 1];
            qj[2] = q[j*dim + 2];

            r2ij = (qi[0]-qj[0])*(qi[0]-qj[0]) + (qi[1]-qj[1])*(qi[1]-qj[1]) + (qi[2]-qj[2])*(qi[2]-qj[2]);

            if( r2ij < atoms->r2_cutoff ){
                
                rij = sqrt(r2ij);
                
                for(int type=0; type<n_types; type++){
                    double rhoj = cubic_spline( &(eam[type][type]->rho), rij );
                    temp_ed[i] += rhoj*x[j_master*n_types+type];
                    if(j< n_atoms) temp_ed[j_master] += rhoj*x[i*n_types+type];
                }
            }
        }
    }
    
    MPI_Allreduce(temp_ed, ed, n_atoms, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    
    //
    // Compute the derivatives of embedding function with respect to the electron density
    //
    
    for(i=rank; i<n_atoms; i+=size){        
        for(int type=0; type<n_types; type++){ 
            temp_dF[i*n_types+type] = d_cubic_spline( &(eam[type][type]->embed), ed[i]/eam[type][type]->factor )/eam[type][type]->factor;
        }
    }
    
    MPI_Allreduce(temp_dF, dF, n_types*n_atoms, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    
    //
    // Compute the pair forces
    //

    for(i=rank; i<n_atoms; i+=size){
        
        qi[0] = q[i*dim + 0];
        qi[1] = q[i*dim + 1];
        qi[2] = q[i*dim + 2];
        
        for(k=0; k<numneigh[i]; k++){
            
            j = neigh[i*maxneigh+k]; // if(i>=j)continue;
            j_master = master[j];
            
            fij =0;
            
            qj[0] = q[j*dim + 0];
            qj[1] = q[j*dim + 1];
            qj[2] = q[j*dim + 2];

            r2ij = (qi[0]-qj[0])*(qi[0]-qj[0])+(qi[1]-qj[1])*(qi[1]-qj[1])+(qi[2]-qj[2])*(qi[2]-qj[2]);

            if( r2ij < atoms->r2_cutoff ){
                
                rij = sqrt(r2ij);
                
                for(int i_type=0; i_type<n_types; i_type++){
                    d_rho_mat[i_type]= d_cubic_spline( &(eam[i_type][i_type]->rho), rij );
                    for(int j_type=i_type; j_type<n_types; j_type++){
                        double val = d_cubic_spline( &(eam[i_type][j_type]->pair), rij );
                        d_pair_mat[i_type][j_type]=val;
                        d_pair_mat[j_type][i_type]=val;                         
                    }                   
                }
              	
                for(int i_type=0; i_type<n_types; i_type++){                    
                    for(int j_type=0; j_type<n_types; j_type++){
                    
                        fij += (                          
                            dF[i*n_types+i_type]*d_rho_mat[j_type] + 
                            dF[j_master*n_types+j_type]*d_rho_mat[i_type] +
                            d_pair_mat[i_type][j_type]                       
                         )*x[i*n_types+i_type]*x[j_master*n_types+j_type];
                    }
                }
                
                fij = 0.5*fij/rij;
            
                // sij = (1/vol) \sum_k ( -m^k*(u_i^k-\bar{u}_i)*(u_j^k-\bar{u}_j)  + 0.5 \sum_l ( q_i^l - q_i^k )*f_j^{lk})
                
                f[i*6+0] += (qj[0]-qi[0])*fij*(qj[0]-qi[0]);//xx
                f[i*6+1] += (qj[1]-qi[1])*fij*(qj[1]-qi[1]);//yy
                f[i*6+2] += (qj[2]-qi[2])*fij*(qj[2]-qi[2]);//zz
                f[i*6+3] += (qj[0]-qi[0])*fij*(qj[1]-qi[1]);//xy 
                f[i*6+4] += (qj[0]-qi[0])*fij*(qj[2]-qi[2]);//xz 
                f[i*6+5] += (qj[1]-qi[1])*fij*(qj[2]-qi[2]);//yz 
                
                if(j< n_atoms){
                    f[j*6+0] += (qj[0]-qi[0])*fij*(qj[0]-qi[0]);//xx
                    f[j*6+1] += (qj[1]-qi[1])*fij*(qj[1]-qi[1]);//yy
                    f[j*6+2] += (qj[2]-qi[2])*fij*(qj[2]-qi[2]);//zz
                    f[j*6+3] += (qj[0]-qi[0])*fij*(qj[1]-qi[1]);//xy 
                    f[j*6+4] += (qj[0]-qi[0])*fij*(qj[2]-qi[2]);//xz 
                    f[j*6+5] += (qj[1]-qi[1])*fij*(qj[2]-qi[2]);//yz  
                }
            
            }            
        }
        
        // K_B*T -> effect of the temperature.
        
        double KbT= 1.0/atoms->b;
        
        f[i*6+0] -= KbT;
        f[i*6+1] -= KbT;
        f[i*6+2] -= KbT;
    }
    
    MPI_Allreduce(f, all_f, 6*n_atoms, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

    free(qi);
    free(qj);
    free(ed);
    free(temp_ed);
    free(dF);
    free(temp_dF);
    free(f);

    return;
}
