/** 
 * \file 
 * \brief Solver PETSc for the minimization of the functional
 * \author j.p. mendez
 */

#include <mpi.h> // before <stdio.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h> /*exit(1)*/
#include <math.h> /*sqrt*/ /* exp */ /* pow */ /*fabs*/
#include <time.h>
#include <string>
#include <petscsnes.h>
#include "petscdm.h"          
#include "petscdmlabel.h" 
#include "petscdmda.h" 
#include "atom.hpp"
#include "eam_potential.hpp"
#include "mf_eam_free_energy.hpp"
#include "neighbors.hpp"
#include "output.hpp"

using namespace MPI;
using namespace std;

extern int rank, size;

extern double min_omega;

extern double petsc_pressure_abstol;
extern double petsc_pressure_rtol;
extern double petsc_pressure_stol;
extern double petsc_pressure_maxit;
extern double petsc_pressure_maxf;

extern char linesearch_damping;
extern double frequency_force_factor;
extern int Fw;

extern double matrix_translation[27][3];

typedef struct {
    Vec r;
    atom *atoms; /* Info of atoms */ 
    EAMPotential *eam[2][2]; /* Definition of the potential */
    int last_its; /* last iteration in which we updated the list of neighbors */
    double *press; /* The pressure tensor */    
    double qcenter[3];
    double box_x_max;
    double box_x_min;
    double box_y_max;
    double box_y_min;
    double box_z_max;
    double box_z_min;
    double alpha_prev[6];
} Ctx;

static PetscErrorCode FormFunction (SNES snes, Vec x, Vec f, void *ctx)
{
    
/**
 * \brief Function to compute the function for the petsc solver.
 * 
 * @param snes the SNES context
 * @param x input vector
 * @param f function vector (output)
 * @param ctx optional user-defined context. Info of atoms.
 * @return f
 */
    
    int dim = 3;
    atom *atoms =        ((Ctx*)ctx)->atoms;
    double *qcenter =    ((Ctx*)ctx)->qcenter;
    double *alpha_prev = ((Ctx*)ctx)->alpha_prev;
    double vol;
    double factor = 160.21766208/0.000101325*1.01325; //units: eV/A^3 = 1602176.6208 bar
    int n_atoms =   atoms->n_atoms;
    int n_atoms_sc= atoms->n_atoms_sc;    
    int iw0 = dim*n_atoms, iq0, imq0;
    PetscErrorCode    ierr;
    const PetscScalar *xx;
    PetscScalar *ff;
    int its;
    
    double df[n_atoms*6];        
    int i, i_tr;
    
    ierr = VecGetArrayRead(x,&xx);CHKERRQ(ierr);
    ierr = VecGetArray(f,&ff);CHKERRQ(ierr);
    
    //Initialization    
    for(i=0;i<n_atoms+6;i++)ff[i]=0.0;
    
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Update positions and frequency
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    
    //update box
    double desp_box_x = fabs(atoms->box_x_max-atoms->box_x_min)*( xx[n_atoms]   - 1.0 )/2.0;
    double desp_box_y = fabs(atoms->box_y_max-atoms->box_y_min)*( xx[n_atoms+1] - 1.0 )/2.0;
    double desp_box_z = fabs(atoms->box_z_max-atoms->box_z_min)*( xx[n_atoms+2] - 1.0 )/2.0;
    ((Ctx*)ctx)->box_x_max = atoms->box_x_max + desp_box_x;
    ((Ctx*)ctx)->box_x_min = atoms->box_x_min - desp_box_x;
    ((Ctx*)ctx)->box_y_max = atoms->box_y_max + desp_box_y;
    ((Ctx*)ctx)->box_y_min = atoms->box_y_min - desp_box_y;
    ((Ctx*)ctx)->box_z_max = atoms->box_z_max + desp_box_z;
    ((Ctx*)ctx)->box_z_min = atoms->box_z_min - desp_box_z;
    
    
    // map the position
    for (i=0; i<n_atoms; i++){             
        iq0 = dim*i;
        /* Position */      
        atoms->q[iq0]   = xx[n_atoms]  *( atoms->q[iq0]   - qcenter[0] )/alpha_prev[0] + qcenter[0];
        atoms->q[iq0+1] = xx[n_atoms+1]*( atoms->q[iq0+1] - qcenter[1] )/alpha_prev[1] + qcenter[1]; 
        atoms->q[iq0+2] = xx[n_atoms+2]*( atoms->q[iq0+2] - qcenter[2] )/alpha_prev[2] + qcenter[2];         
        /* Frequency */
        atoms->w[i] = xx[i];
          
    }//end of loop over i
    for (i=n_atoms; i<n_atoms_sc; i++){        
        iq0  = dim*i;
        imq0 = dim*atoms->master[i];
        i_tr = atoms->trans_index[i-n_atoms];      
        /* Position */      
        atoms->q[iq0]   = atoms->q[imq0]   + matrix_translation[i_tr][0]*fabs( ((Ctx*)ctx)->box_x_max - ((Ctx*)ctx)->box_x_min );
        atoms->q[iq0+1] = atoms->q[imq0+1] + matrix_translation[i_tr][1]*fabs( ((Ctx*)ctx)->box_y_max - ((Ctx*)ctx)->box_y_min );
        atoms->q[iq0+2] = atoms->q[imq0+2] + matrix_translation[i_tr][2]*fabs( ((Ctx*)ctx)->box_z_max - ((Ctx*)ctx)->box_z_min );        
    }
    
    for(i=0; i<6; i++) ((Ctx*)ctx)->alpha_prev[i] = xx[n_atoms+i];
    
    vol = fabs( ((Ctx*)ctx)->box_x_max - ((Ctx*)ctx)->box_x_min )*
          fabs( ((Ctx*)ctx)->box_y_max - ((Ctx*)ctx)->box_y_min )*
          fabs( ((Ctx*)ctx)->box_z_max - ((Ctx*)ctx)->box_z_min );
        
    /* Compute neighbors */
    ierr = SNESGetIterationNumber(snes,&its);CHKERRQ(ierr);
    if( its%20 == 0 && ((Ctx*)ctx)->last_its != its ){
        neighbors(atoms);
        ((Ctx*)ctx)->last_its = its;
    }
    

    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    Compute function
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    if(Fw){ 
        mf_d_EAM_Free_Energy( ((Ctx*)ctx)->eam, atoms, df);                  
        for(i=0; i< n_atoms; i++) ff[i] = frequency_force_factor*df[iw0+i];
    }
    
    mf_Virial_Stresses_EAM_Potential( ((Ctx*)ctx)->eam, atoms, df );
    
    for(i=0; i<n_atoms; i++){
        ff[n_atoms]   += df[i*6]  /vol * factor; //units: eV/A^3 = 1602176.6208 bar
        ff[n_atoms+1] += df[i*6+1]/vol * factor;
        ff[n_atoms+2] += df[i*6+2]/vol * factor;
        ff[n_atoms+3] += 0.0;//df[i*dim+3];
        ff[n_atoms+4] += 0.0;//df[i*dim+4];
        ff[n_atoms+5] += 0.0;//df[i*dim+5];
    }
    
    
    ff[n_atoms]   += ((Ctx*)ctx)->press[0]; // Apply the prescribed pression. Unit bars
    ff[n_atoms+1] += ((Ctx*)ctx)->press[1];
    ff[n_atoms+2] += ((Ctx*)ctx)->press[2];
    ff[n_atoms+3] -= 0.0;
    ff[n_atoms+4] -= 0.0;
    ff[n_atoms+5] -= 0.0;      
    
    
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Restore vectors
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
   ierr = VecRestoreArrayRead(x,&xx);CHKERRQ(ierr);
   ierr = VecRestoreArray(f,&ff);CHKERRQ(ierr);

  
   return 0;
    
}

static PetscErrorCode SNESLinePreCheck(SNESLineSearch linesearch, Vec x, Vec y, PetscBool *changed, void *ctx)
{
    /**
     * \brief This function constrains the maximum step in each iteration.
     * 
     * @param linesearch
     * @param x solution vector
     * @param y search direction vector
     * @param changed flag to indicate the precheck changed x or y
     * @param ctx user context
     * @return 
     */

    PetscScalar *yy;
    PetscErrorCode ierr;
    atom *atoms = ((Ctx*)ctx)->atoms;
    //int n_atoms = atoms->n_atoms;
    
    ierr = VecGetArray(y,&yy);CHKERRQ(ierr);
    
    // ** do something **//
    
    ierr = VecRestoreArray(y,&yy);CHKERRQ(ierr);
    
    *changed = PETSC_FALSE;
    
    return 0;
}

static PetscErrorCode SNESLinePostCheck(SNESLineSearch linesearch, Vec x, Vec y, Vec w, PetscBool *changed_y, PetscBool *changed_w, void *ctx)
{
    /**
     * \brief This function constrains the maximum step in each iteration.
     * 
     * @param linesearch
     * @param x old solution vector
     * @param y search direction vector
     * @param w new solution vector
     * @param changed_y indicates that the line search changed y
     * @param changed_w indicates that the line search changed w
     * @param ctx user context
     * @return 
     */
    

    const PetscScalar *xx;
    PetscScalar *ww;
    PetscErrorCode ierr;    
    atom *atoms = ((Ctx*)ctx)->atoms;
    int n_atoms = atoms->n_atoms;                 
    int i;
    
    ierr = VecGetArrayRead(x,&xx);CHKERRQ(ierr);
    ierr = VecGetArray(w,&ww)    ;CHKERRQ(ierr);  
    
    //checks frequency
    
    if( Fw ) for(i=0; i<n_atoms; i++){
        if(ww[i]<=min_omega)ww[i]=min_omega;    
    }//end of loop over i
        
    ierr = VecRestoreArrayRead(x,&xx);CHKERRQ(ierr);
    ierr = VecRestoreArray(w,&ww);CHKERRQ(ierr);
    
    *changed_y = PETSC_FALSE;
    *changed_w = PETSC_TRUE;
    
    return 0;
}

PetscErrorCode Minimize_npt_petsc (int time_step, EAMPotential *eam[2][2], atom *atoms, double *press)
{
    
    /**
     * \brief 
     * 
     * @param time_step time step
     * @param eam
     * @param atoms
     * @param press the prescribed pressure. Positive pression: ))<- ; negative pression: ))->
     * @return 
     */
    
    SNES           snes;       /* nonlinear solver context */
    Vec            x;          /* solution vectors */
    const PetscScalar *rr, *xx;
    PetscErrorCode ierr;
    PetscInt       its;
    SNESLineSearch linesearch;
    double dwmax=0.0, dw=0.0;
    int i, iq0, imq0, dim=3, i_tr;
    int n_atoms    = atoms->n_atoms;
    int n_atoms_sc = atoms->n_atoms_sc;
    
    PetscReal abstol = petsc_pressure_abstol; //absolute convergence tolerance
    PetscReal rtol   = petsc_pressure_rtol;     //relative convergence tolerance
    PetscReal stol   = petsc_pressure_stol;     //convergence tolerance in terms of the norm of the change in the solution between steps, || delta x || < stol*|| x ||
    PetscInt  maxit  = petsc_pressure_maxit;   //maximum number of iterations
    PetscInt  maxf   = petsc_pressure_maxf;    //maximum number of function evaluations
    SNESConvergedReason	reason;      //Set a reason for convergence/divergence of solver (SNES)
    
  
    /* Set some options */

    
    /* SNESNEWTONLS */
    
    PetscOptionsSetValue(NULL,"-snes_type","nrichardson");
    PetscOptionsSetValue(NULL,"-snes_linesearch_type","basic");
    PetscOptionsSetValue(NULL,"-snes_linesearch_damping",(const char*)&linesearch_damping);
   
        
    Ctx ctx;
    ctx.atoms = atoms;
    ctx.press = press;
    ctx.last_its=-1;
    
    ctx.eam[0][0] = eam[0][0];
    ctx.eam[0][1] = eam[0][1];
    ctx.eam[1][1] = eam[1][1];
    ctx.eam[1][0] = eam[1][0];
    
    ctx.qcenter[0]=(atoms->box_x_max + atoms->box_x_min)*0.5;
    ctx.qcenter[1]=(atoms->box_y_max + atoms->box_y_min)*0.5;
    ctx.qcenter[2]=(atoms->box_z_max + atoms->box_z_min)*0.5;
    
    ctx.box_x_max = atoms->box_x_max;
    ctx.box_x_min = atoms->box_x_min;
    ctx.box_y_max = atoms->box_y_max;
    ctx.box_y_min = atoms->box_y_min;
    ctx.box_z_max = atoms->box_z_max;
    ctx.box_z_min = atoms->box_z_min;
    
    ctx.alpha_prev[0] = 1.0;
    ctx.alpha_prev[1] = 1.0;
    ctx.alpha_prev[2] = 1.0;
    ctx.alpha_prev[3] = 0.0;
    ctx.alpha_prev[4] = 0.0;
    ctx.alpha_prev[5] = 0.0;

  
   /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Create nonlinear solver context
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    ierr = SNESCreate(PETSC_COMM_SELF ,&snes);CHKERRQ(ierr);
    
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Create matrix and vector data structures; set corresponding routines
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  /* Create vectors for solution and nonlinear function */
    ierr = VecCreateSeq(PETSC_COMM_SELF,(PetscInt)(n_atoms+6),&x);CHKERRQ(ierr);
    ierr = VecSetFromOptions(x);CHKERRQ(ierr);
    ierr = VecDuplicate(x,&(ctx.r));CHKERRQ(ierr);

  /* Set function evaluation routine and vector. */    
    ierr = SNESSetFunction(snes,ctx.r,FormFunction,&ctx);CHKERRQ(ierr);
      
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Customize nonlinear solver; set runtime options
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
        
    ierr = SNESGetLineSearch(snes, &linesearch);CHKERRQ(ierr);CHKERRQ(ierr);
    ierr = SNESLineSearchSetPreCheck(linesearch, SNESLinePreCheck, &ctx);CHKERRQ(ierr);
    ierr = SNESLineSearchSetPostCheck(linesearch,SNESLinePostCheck, &ctx);CHKERRQ(ierr);   
    
    ierr = SNESSetFromOptions(snes);CHKERRQ(ierr);
    ierr = SNESSetTolerances(snes,abstol,PETSC_DEFAULT,PETSC_DEFAULT,maxit,maxf);CHKERRQ(ierr);  

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Evaluate initial guess; then solve nonlinear system; save final solution
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    /* Set the initial values */
    
    for(i=0; i<n_atoms; i++) VecSetValue(x,i,atoms->w[i],INSERT_VALUES);
    for(i=0; i<6; i++)       VecSetValue(x,n_atoms+i,ctx.alpha_prev[i],INSERT_VALUES);    
        
    /* Solve */
    ierr = SNESSolve(snes,NULL,x);CHKERRQ(ierr);
    ierr = SNESGetIterationNumber(snes,&its);CHKERRQ(ierr); 
       
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Store the final solution
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    ierr = VecGetArrayRead(x,&xx);CHKERRQ(ierr);
    for (i=0; i<n_atoms; i++){             
        iq0 = dim*i;
        /* Position */      
        atoms->q[iq0]   = xx[n_atoms]  *( atoms->q[iq0]   - ctx.qcenter[0] )/ctx.alpha_prev[0] + ctx.qcenter[0];
        atoms->q[iq0+1] = xx[n_atoms+1]*( atoms->q[iq0+1] - ctx.qcenter[1] )/ctx.alpha_prev[1] + ctx.qcenter[1]; 
        atoms->q[iq0+2] = xx[n_atoms+2]*( atoms->q[iq0+2] - ctx.qcenter[2] )/ctx.alpha_prev[2] + ctx.qcenter[2];         
        /* Frequency */
        atoms->w[i] = xx[i];
          
    }//end of loop over i
    for (i=n_atoms; i<n_atoms_sc; i++){        
        iq0  = dim*i;
        imq0 = dim*atoms->master[i];
        i_tr = atoms->trans_index[i-n_atoms];      
        /* Position */      
        atoms->q[iq0]   = atoms->q[imq0]   + matrix_translation[i_tr][0]*fabs(ctx.box_x_max - ctx.box_x_min);
        atoms->q[iq0+1] = atoms->q[imq0+1] + matrix_translation[i_tr][1]*fabs(ctx.box_y_max - ctx.box_y_min);
        atoms->q[iq0+2] = atoms->q[imq0+2] + matrix_translation[i_tr][2]*fabs(ctx.box_z_max - ctx.box_z_min);
        
    }
    
    
    //update box
    atoms->box_x_max = ctx.box_x_max;
    atoms->box_x_min = ctx.box_x_min;
    atoms->box_y_max = ctx.box_y_max;
    atoms->box_y_min = ctx.box_y_min;
    atoms->box_z_max = ctx.box_z_max;
    atoms->box_z_min = ctx.box_z_min;
    
    ctx.alpha_prev[0] = xx[n_atoms];
    ctx.alpha_prev[1] = xx[n_atoms+1];
    ctx.alpha_prev[2] = xx[n_atoms+2];
    ctx.alpha_prev[3] = xx[n_atoms+3];
    ctx.alpha_prev[4] = xx[n_atoms+4];
    ctx.alpha_prev[5] = xx[n_atoms+5];
    
    ierr = VecRestoreArrayRead(x,&xx);CHKERRQ(ierr);
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     converged or diverged reason
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    ierr = VecGetArrayRead(ctx.r,&rr);CHKERRQ(ierr);
    
    if(rank==0){
    
        ierr = SNESGetConvergedReason(snes,&reason); CHKERRQ(ierr);
    
        ofstream fichero;
        fichero.open ("results/ConvergenceCondition_alpha", ios::app);
    
        fichero << "Iteration: "<< time_step << endl;
        fichero << "Number of SNES iterations = "<< its << endl;
        switch (reason) {
            /*---------- DIVERGED-----------*/
            case  SNES_DIVERGED_FUNCTION_DOMAIN:{fichero << "SNES_DIVERGED_FUNCTION_DOMAIN\n";}break; /* the new x location passed the function is not in the domain of F */
            case  SNES_DIVERGED_FUNCTION_COUNT:{fichero <<"SNES_DIVERGED_FUNCTION_COUNT\n";} break;
            case  SNES_DIVERGED_LINEAR_SOLVE:{fichero <<"SNES_DIVERGED_LINEAR_SOLVE\n";} break; /* the linear solve failed */
            case  SNES_DIVERGED_FNORM_NAN:{fichero <<"SNES_DIVERGED_FNORM_NAN\n";} break;
            case  SNES_DIVERGED_MAX_IT:{fichero <<"SNES_DIVERGED_MAX_IT\n";} break;
            case  SNES_DIVERGED_LINE_SEARCH:{fichero <<"SNES_DIVERGED_LINE_SEARCH\n";} break; /* the line search failed */
            case  SNES_DIVERGED_INNER:{fichero <<"SNES_DIVERGED_INNER\n";} break; /* inner solve failed */
            case  SNES_DIVERGED_LOCAL_MIN:{fichero <<"SNES_DIVERGED_LOCAL_MIN\n";} break; /* || J^T b || is small, implies converged to local minimum of F() */
            case  SNES_CONVERGED_ITERATING:{fichero <<"SNES_CONVERGED_ITERATING\n";} break;
            /*---------- CONVERGED-----------*/
            case  SNES_CONVERGED_FNORM_ABS:{fichero <<"SNES_CONVERGED_FNORM_ABS\n";} break; /* ||F|| < atol */
            case  SNES_CONVERGED_FNORM_RELATIVE:{fichero <<"SNES_CONVERGED_FNORM_RELATIVE\n";} break; /* ||F|| < rtol*||F_initial|| */
            case  SNES_CONVERGED_SNORM_RELATIVE:{fichero <<"SNES_CONVERGED_SNORM_RELATIVE\n";} break; /* Newton computed step size small; || delta x || < stol || x ||*/
            case  SNES_CONVERGED_ITS:{fichero <<"SNES_CONVERGED_ITS\n";} break; /* maximum iterations reached */
            case  SNES_CONVERGED_TR_DELTA:{fichero <<"SNES_CONVERGED_TR_DELTA\n";} break;
        }        
    
        for(i=0; i<n_atoms; i++){
            dw = fabs(rr[i]);       
            if(dw>dwmax) dwmax = dw; 
        }
     
                
        fichero<<"max_dw "<<dwmax<<endl;
        fichero<<"pressure tensor "<<(-1.0/3.0)*(rr[n_atoms]+rr[n_atoms+1]+rr[n_atoms+2]-press[0]-press[1]-press[2])<<endl;
        fichero<<"alpha tensor "<<ctx.alpha_prev[0]<<" "<<ctx.alpha_prev[1]<<" "<<ctx.alpha_prev[2]<<" "<<ctx.alpha_prev[3]<<" "<<ctx.alpha_prev[4]<<" "<<ctx.alpha_prev[5]<<endl;
        fichero<<"box "<<atoms->box_x_max<<" "<<atoms->box_x_min<<" "<<atoms->box_y_max<<" "<<atoms->box_y_min<<" "<<atoms->box_z_max<<" "<<atoms->box_z_min<<endl;
        
        fichero.close();
    
    }
    
    ierr = VecRestoreArrayRead(ctx.r,&rr);CHKERRQ(ierr); 
    
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Free work space.
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

    ierr = VecDestroy(&x);CHKERRQ(ierr);
    ierr = SNESDestroy(&snes);CHKERRQ(ierr);
    ierr = VecDestroy(&(ctx.r));CHKERRQ(ierr);  
        
    return 0;
}
