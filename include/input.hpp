/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   input.hpp
 * Author: juan
 *
 * Created on February 1, 2018, 3:44 PM
 */

#ifndef INPUT_HPP
#define INPUT_HPP

void read_atoms(atom *atoms);
void read_atoms_mpi (atom *atoms);

#endif /* INPUT_HPP */

