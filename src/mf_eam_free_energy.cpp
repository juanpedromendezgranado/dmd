#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "eam_potential.hpp"
#include "atom.hpp"

#include <iostream> //eliminar

using namespace MPI;
extern int size;
extern int rank;
extern double k_B;
extern double h_planck;

double mf_EAM_Free_Energy( EAMPotential *eam[2][2], atom* atoms){

       
    int i, j, j_master, k;
    int dim = 3;

    double en=0.0, free_en=0.0, all_free_en=0.0, r2ij=0.0, rij=0.0;
    
    double* q = atoms->q;
    double* x = atoms->x;
    double* w = atoms->w;
    int*    numneigh = atoms->numneigh;
    int*    neigh = atoms->neigh;
    int*    master = atoms->master;
    int     maxneigh = atoms->maxneigh;
    int n_atoms = atoms->n_atoms;
    int n_types = atoms->n_types;

    double* qi = (double*) malloc( dim * sizeof( double ) );
    double* qj = (double*) malloc( dim * sizeof( double ) );
    double* ed = (double*) malloc( n_atoms * sizeof( double ) );
    double* mf_ed = (double*) malloc( n_atoms * sizeof( double ) );
    double* temp_ed = (double*) malloc( n_atoms * sizeof( double ) );
    double* temp_mf_ed = (double*) malloc( n_atoms * sizeof( double ) );
    
    double pair_mat[n_types][n_types];
    
    double mass_i, mass_j;
    
    //
    // Compute the electron densities for all sites
    //
    
    for(i=0; i<n_atoms; i++) {temp_ed[i]=0.0; temp_mf_ed[i]=0.0;}
    
    for(i=rank; i<n_atoms; i+=size){
        
        qi[0] = q[i*dim + 0];
        qi[1] = q[i*dim + 1];
        qi[2] = q[i*dim + 2];
        
        mass_i =0;
        for(int type=0; type<n_types; type++){ mass_i += x[i*n_types+type]/eam[type][type]->mass;}
        mass_i = 1.0/mass_i;
        
        for(k=0; k<numneigh[i]; k++){
            
            j = neigh[i*maxneigh+k]; // if(i>=j)continue;
            j_master = master[j];
            
            qj[0] = q[j*dim + 0];
            qj[1] = q[j*dim + 1];
            qj[2] = q[j*dim + 2];
            
            mass_j =0;
            for(int type=0; type<n_types; type++){ mass_j += x[j_master*n_types+type]/eam[type][type]->mass;}
            mass_j = 1.0/mass_j;

            r2ij = (qi[0]-qj[0])*(qi[0]-qj[0]) + (qi[1]-qj[1])*(qi[1]-qj[1]) + (qi[2]-qj[2])*(qi[2]-qj[2]);

            if( r2ij < atoms->r2_cutoff ){
                
                rij = sqrt(r2ij);
                
                for(int type=0; type<n_types; type++){
                    double rhoj = cubic_spline( &(eam[type][type]->rho), rij );
                    double mf_rhoj = mf_cubic_spline(&(eam[type][type]->rho), qi, qj, mass_i, mass_j, w[i], w[j_master], atoms->b);
                    temp_ed[i]   += rhoj*x[j_master*n_types+type];
                    temp_mf_ed[i]+= mf_rhoj*x[j_master*n_types+type];
                    if(j<n_atoms){
                        temp_ed[j_master]   += rhoj*x[i*n_types+type];                    
                        temp_mf_ed[j_master]+= mf_rhoj*x[i*n_types+type];
                    }
                }
            }
        }
    }
    
    MPI_Allreduce(temp_ed, ed, n_atoms, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    MPI_Allreduce(temp_mf_ed, mf_ed, n_atoms, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        
    //
    // Compute the energy
    //
    
    for(i=rank; i<n_atoms; i+=size){
        
        en=0.0;
        
        qi[0] = q[i*dim + 0];
        qi[1] = q[i*dim + 1];
        qi[2] = q[i*dim + 2];
        
        mass_i =0;
        for(int type=0; type<n_types; type++){ mass_i += x[i*n_types+type]/eam[type][type]->mass;}
        mass_i = 1.0/mass_i;
        
        // Compute the embedding energy
        
        for(int type=0; type<n_types; type++){
            en += ( cubic_spline( &(eam[type][type]->embed), ed[i]/eam[type][type]->factor ) +
                        (mf_ed[i]-ed[i])*d_cubic_spline( &(eam[type][type]->embed), ed[i]/eam[type][type]->factor )/eam[type][type]->factor 
                      )*x[i*n_types+type];
        }
        
        // Compute the pair energy
               
        for(k=0; k<numneigh[i]; k++){
            
            j = neigh[i*maxneigh+k]; // if(i>=j)continue;
            j_master = master[j];
        
            qj[0] = q[j*dim + 0];
            qj[1] = q[j*dim + 1];
            qj[2] = q[j*dim + 2];
            
            mass_j =0;
            for(int type=0; type<n_types; type++){ mass_j += x[j_master*n_types+type]/eam[type][type]->mass;}
            mass_j = 1.0/mass_j;
        
            r2ij = (qi[0]-qj[0])*(qi[0]-qj[0]) + (qi[1]-qj[1])*(qi[1]-qj[1]) + (qi[2]-qj[2])*(qi[2]-qj[2]);

            if( r2ij < atoms->r2_cutoff ){ 
                
                for(int i_type=0; i_type<n_types; i_type++){
                    for(int j_type=i_type; j_type<n_types; j_type++){
                        double val = mf_cubic_spline( &(eam[i_type][j_type]->pair), qi, qj, mass_i, mass_j, w[i], w[j_master], atoms->b);
                        pair_mat[i_type][j_type] = val;
                        pair_mat[j_type][i_type] = val;
                    }
                }
                
                if(j<n_atoms){
                    for(int i_type=0; i_type<n_types; i_type++){
                        for(int j_type=0; j_type<n_types; j_type++){
                            en += pair_mat[i_type][j_type]*x[i*n_types+i_type]*x[j_master*n_types+j_type];
                        }
                    }
                }else{
                    for(int i_type=0; i_type<n_types; i_type++){
                        for(int j_type=0; j_type<n_types; j_type++){
                            en += 0.5*pair_mat[i_type][j_type]*x[i*n_types+i_type]*x[j_master*n_types+j_type];
                        }
                    }
                } 
                
                
            }            
        }
        
        // entropy part and kinetic energy
        
        double s = 0.0;
        for(int type=0; type<n_types; type++){
            s += atoms->x[i*n_types+type]*log( atoms->x[i*n_types+type] + 1.0e-4 ); // we add 1.0e-4 to avoid nan number
        }
        free_en += k_B*( atoms->b*en - 3.0/2.0 + 3.0*log( h_planck*atoms->b*atoms->w[i] ) + s );
        
//        free_en += en;

    }
    
    MPI_Allreduce(&free_en, &all_free_en, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

    free( qi );
    free( qj );
    free (ed);
    free (mf_ed);
    free (temp_ed);
    free (temp_mf_ed);

    return all_free_en;
}

void mf_d_EAM_Free_Energy( EAMPotential *eam[2][2], atom* atoms, double *all_f ){

    int i, j, j_master, k;
    int dim = 3;

    double fij[3] = {0.0,0.0,0.0}, fijwi = 0.0, fijwj = 0.0;
    double r2ij = 0.0, rij=0;
    
    double* q = atoms->q;
    double* x = atoms->x;
    double* w = atoms->w;
    int*    numneigh = atoms->numneigh;
    int*    neigh = atoms->neigh;
    int*    master = atoms->master;
    int     maxneigh = atoms->maxneigh;
    int n_atoms = atoms->n_atoms;
    int n_types = atoms->n_types;

    double* qi = (double*) malloc( dim * sizeof( double ) );
    double* qj = (double*) malloc( dim * sizeof( double ) );
    double* ed = (double*) malloc( n_atoms * sizeof( double ) );
    double* temp_ed = (double*) malloc( n_atoms * sizeof( double ) );
    double* dF = (double*) malloc( n_types * n_atoms * sizeof( double ) );
    double* temp_dF = (double*) malloc( n_types * n_atoms * sizeof( double ) );
    double* f = (double*) malloc( (dim+1) * n_atoms * sizeof( double ) ); // we will store fq and fw
    
    double d_pair_mat[n_types][n_types][dim], d_pair_mat_wi[n_types][n_types], d_pair_mat_wj[n_types][n_types];
    double d_rho_mat[n_types][dim], d_rho_mat_wi[n_types], d_rho_mat_wj[n_types];
    
    double mass_i, mass_j;
    
    double k1 = k_B*atoms->b;
    
    // Initialization
    for(i=0; i<n_atoms;         i++) temp_ed[i]=0.0;  
    for(i=0; i<n_atoms*n_types; i++) temp_dF[i]=0.0;
    for(i=0; i<n_atoms*(dim+1); i++) f[i]=0.0;
    
    //
    // Compute the electron densities for all sites
    // 
        
    for(i=rank; i<n_atoms; i+=size){
        
        qi[0] = q[i*dim + 0];
        qi[1] = q[i*dim + 1];
        qi[2] = q[i*dim + 2];
        
        mass_i =0;
        for(int type=0; type<n_types; type++){ mass_i += x[i*n_types+type]/eam[type][type]->mass;}
        mass_i = 1.0/mass_i;
        
        for(k=0; k<numneigh[i]; k++){
            
            j = neigh[i*maxneigh+k]; // if(i>=j)continue;
            j_master = master[j];
            
            qj[0] = q[j*dim + 0];
            qj[1] = q[j*dim + 1];
            qj[2] = q[j*dim + 2];
            
            mass_j =0;
            for(int type=0; type<n_types; type++){ mass_j += x[j_master*n_types+type]/eam[type][type]->mass;}
            mass_j = 1.0/mass_j;

            r2ij = (qi[0]-qj[0])*(qi[0]-qj[0]) + (qi[1]-qj[1])*(qi[1]-qj[1]) + (qi[2]-qj[2])*(qi[2]-qj[2]);

            if( r2ij < atoms->r2_cutoff ){
                
                rij = sqrt(r2ij);
                
                for(int type=0; type<n_types; type++){
                    double rhoj = cubic_spline( &(eam[type][type]->rho), rij );
                    temp_ed[i]   += rhoj*x[j_master*n_types+type];
                    if(j<n_atoms) temp_ed[j_master]+= rhoj*x[i*n_types+type];                    
                }
            }
        }
    }
    
    MPI_Allreduce(temp_ed, ed, n_atoms, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    
    //
    // Compute the derivatives of embedding function with respect to the electron density
    //
    
    for(i=rank; i<n_atoms; i+=size){       
        for(int type=0; type<n_types; type++){ 
            temp_dF[i*n_types+type] = d_cubic_spline( &(eam[type][type]->embed), ed[i]/eam[type][type]->factor )/eam[type][type]->factor;
        }
    }
    
    MPI_Allreduce(temp_dF, dF, n_types*n_atoms, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);    
    
    //
    // Compute the pair forces
    //

    for(i=rank; i<n_atoms; i+=size){
        
        qi[0] = q[i*dim + 0];
        qi[1] = q[i*dim + 1];
        qi[2] = q[i*dim + 2];
        
        mass_i =0;
        for(int type=0; type<n_types; type++){ mass_i += x[i*n_types+type]/eam[type][type]->mass;}
        mass_i = 1.0/mass_i;
    
        for(k=0; k<numneigh[i]; k++){
            
            j = neigh[i*maxneigh+k]; // if(i>=j)continue;
            j_master = master[j];
            
            fij[0] =0; fij[1] =0; fij[2] =0; fijwi=0.0; fijwj=0.0;
            
            qj[0] = q[j*dim + 0];
            qj[1] = q[j*dim + 1];
            qj[2] = q[j*dim + 2];
            
            mass_j =0;
            for(int type=0; type<n_types; type++){ mass_j += x[j_master*n_types+type]/eam[type][type]->mass;}
            mass_j = 1.0/mass_j;

            r2ij = (qi[0]-qj[0])*(qi[0]-qj[0])+(qi[1]-qj[1])*(qi[1]-qj[1])+(qi[2]-qj[2])*(qi[2]-qj[2]);

            if( r2ij < atoms->r2_cutoff ){
                
                for(int i_type=0; i_type<n_types; i_type++){
                    mf_d_cubic_spline(&d_rho_mat[i_type][0], &d_rho_mat_wi[i_type], &d_rho_mat_wj[i_type], &(eam[i_type][i_type]->rho), qi, qj, mass_i, mass_j, w[i], w[j_master], atoms->b);
                    for(int j_type=i_type; j_type<n_types; j_type++){
                        double val1[3], val2, val3;
                        mf_d_cubic_spline(val1, &val2, &val3, &(eam[i_type][j_type]->pair), qi, qj, mass_i, mass_j, w[i], w[j_master], atoms->b);
                        d_pair_mat[i_type][j_type][0]=val1[0];
                        d_pair_mat[i_type][j_type][1]=val1[1];
                        d_pair_mat[i_type][j_type][2]=val1[2];
                        d_pair_mat[j_type][i_type][0]=val1[0];
                        d_pair_mat[j_type][i_type][1]=val1[1];
                        d_pair_mat[j_type][i_type][2]=val1[2];
                        
                        d_pair_mat_wi[i_type][j_type]=val2;
                        d_pair_mat_wi[j_type][i_type]=val2;
                        
                        d_pair_mat_wj[i_type][j_type]=val3;
                        d_pair_mat_wj[j_type][i_type]=val3; 
                    }                   
                }
              	
                for(int i_type=0; i_type<n_types; i_type++){                    
                    for(int j_type=0; j_type<n_types; j_type++){
                    
                        for(int l=0; l<dim; l++){
                            fij[l]+=(dF[i*n_types+i_type]*d_rho_mat[j_type][l]+dF[j_master*n_types+j_type]*d_rho_mat[i_type][l]+d_pair_mat[i_type][j_type][l])*x[i*n_types+i_type]*x[j_master*n_types+j_type];
                        }
                        
                        fijwi+=(dF[i*n_types+i_type]*d_rho_mat_wi[j_type]+dF[j_master*n_types+j_type]*d_rho_mat_wi[i_type]+d_pair_mat_wi[i_type][j_type])*x[i*n_types+i_type]*x[j_master*n_types+j_type];
                        fijwj+=(dF[i*n_types+i_type]*d_rho_mat_wj[j_type]+dF[j_master*n_types+j_type]*d_rho_mat_wj[i_type]+d_pair_mat_wj[i_type][j_type])*x[i*n_types+i_type]*x[j_master*n_types+j_type];
                    }
                }
                                
                fijwi *= sqrt( 2.0/ (atoms->b * mass_i) )/(w[i]*w[i])*98.227002603; // the number sqrt(2) is incluided before                       
                fijwj *= sqrt( 2.0/ (atoms->b * mass_j) )/(w[j_master]*w[j_master])*98.227002603;
            
                // update pair force
                
                f[i*dim+0] += fij[0]*k1; 
                f[i*dim+1] += fij[1]*k1;
                f[i*dim+2] += fij[2]*k1;
                
                f[n_atoms*dim+i] += fijwi*k1;
                
                if(j<n_atoms){
                    f[j_master*dim+0] -= fij[0]*k1;
                    f[j_master*dim+1] -= fij[1]*k1;
                    f[j_master*dim+2] -= fij[2]*k1;
                    
                    f[n_atoms*dim+j_master] += fijwj*k1;
                }
            
            }            
        }
        
        // entropy contribution
        
        f[n_atoms*dim+i] += 3.0*k_B/atoms->w[i];
        
    }
    
    MPI_Allreduce(f, all_f, (1+dim)*n_atoms, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

    free(qi);
    free(qj);
    free(ed);
    free(temp_ed);
    free(dF);
    free(temp_dF);
    free(f);

    return;
}

void mf_dx_EAM_Free_Energy( EAMPotential *eam[2][2], atom* atoms, double *all_f ){

    int i, j, j_master, k;
    int dim = 3;
    double r2ij = 0.0, rij=0.0;
    
    double* q = atoms->q;
    double* x = atoms->x;
    double* w = atoms->w;
    int*    numneigh = atoms->numneigh;
    int*    neigh = atoms->neigh;
    int*    master = atoms->master;
    int     maxneigh = atoms->maxneigh;
    int n_atoms = atoms->n_atoms;
    int n_types = atoms->n_types;

    double* qi = (double*) malloc( dim * sizeof( double ) );
    double* qj = (double*) malloc( dim * sizeof( double ) );
    double* ed = (double*) malloc( n_atoms * sizeof( double ) );
    double* temp_ed = (double*) malloc( n_atoms * sizeof( double ) );
    double* mf_ed = (double*) malloc( n_atoms * sizeof( double ) );
    double* temp_mf_ed = (double*) malloc( n_atoms * sizeof( double ) );
    double* dF = (double*) malloc( n_types * n_atoms * sizeof( double ) );
    double* temp_dF = (double*) malloc( n_types * n_atoms * sizeof( double ) );
    double* f = (double*) malloc( n_types * n_atoms * sizeof( double ) );
    
    double pair_mat[n_types][n_types], d_pair_mat_xi[n_types][n_types], d_pair_mat_xj[n_types][n_types];
    double d_rho_mat[n_types][dim], d_rho_mat_xi[n_types], d_rho_mat_xj[n_types];
    
    double mass_i, mass_j;
    
    double k1 = k_B*atoms->b;
    
    // Initialization
    for(i=0; i<n_atoms; i++){temp_ed[i]=0.0;  temp_mf_ed[i]=0.0;}
    for(i=0; i<n_atoms*n_types; i++) temp_dF[i]=0.0;
    for(i=0; i<n_atoms*n_types; i++) f[i]=0.0;
    
    //
    // Compute the electron densities for all sites
    //
    
    for(i=rank; i<n_atoms; i+=size){
        
        qi[0] = q[i*dim + 0];
        qi[1] = q[i*dim + 1];
        qi[2] = q[i*dim + 2];
        
        mass_i =0;
        for(int type=0; type<n_types; type++){ mass_i += x[i*n_types+type]/eam[type][type]->mass;}
        mass_i = 1.0/mass_i;
        
        for(k=0; k<numneigh[i]; k++){
            
            j = neigh[i*maxneigh+k]; // if(i>=j)continue;
            j_master = master[j];
            
            qj[0] = q[j*dim + 0];
            qj[1] = q[j*dim + 1];
            qj[2] = q[j*dim + 2];
            
            mass_j =0;
            for(int type=0; type<n_types; type++){ mass_j += x[j_master*n_types+type]/eam[type][type]->mass;}
            mass_j = 1.0/mass_j;

            r2ij = (qi[0]-qj[0])*(qi[0]-qj[0]) + (qi[1]-qj[1])*(qi[1]-qj[1]) + (qi[2]-qj[2])*(qi[2]-qj[2]);

            if( r2ij < atoms->r2_cutoff ){
                
                rij = sqrt(r2ij);
                
                for(int type=0; type<n_types; type++){
                    double rhoj = cubic_spline( &(eam[type][type]->rho), rij );
                    double mf_rhoj = mf_cubic_spline(&(eam[type][type]->rho), qi, qj, mass_i, mass_j, w[i], w[j_master], atoms->b);
                    temp_ed[i]   += rhoj*x[j_master*n_types+type];
                    temp_mf_ed[i]+= mf_rhoj*x[j_master*n_types+type];
                    if(j<n_atoms){
                        temp_ed[j_master]   += rhoj*x[i*n_types+type];                    
                        temp_mf_ed[j_master]+= mf_rhoj*x[i*n_types+type];
                    }
                }
            }
        }
    }
    
    MPI_Allreduce(temp_ed, ed, n_atoms, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    MPI_Allreduce(temp_mf_ed, mf_ed, n_atoms, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        
    //
    // Compute the derivatives of embedding function with respect to the electron density
    //
    
    for(i=rank; i<n_atoms; i+=size){        
        for(int type=0; type<n_types; type++){ 
            temp_dF[i*n_types+type] = d_cubic_spline( &(eam[type][type]->embed), ed[i]/eam[type][type]->factor )/eam[type][type]->factor;
        }
    }
    
    MPI_Allreduce(temp_dF, dF, n_types*n_atoms, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD); 
        
    //
    // Compute the pair forces
    //

    for(i=rank; i<n_atoms; i+=size){
        
        qi[0] = q[i*dim + 0];
        qi[1] = q[i*dim + 1];
        qi[2] = q[i*dim + 2];
        
        mass_i =0;
        for(int type=0; type<n_types; type++){ mass_i += x[i*n_types+type]/eam[type][type]->mass;}
        mass_i = 1.0/mass_i;
        
        // Compute <Fi>_0
        
        for(int type=0; type<n_types; type++){
            f[i*n_types+type] += k1*( cubic_spline( &(eam[type][type]->embed), ed[i]/eam[type][type]->factor )+(mf_ed[i]-ed[i])*dF[i*n_types+type] );
        }
        
        // i-j pair
        
        for(k=0; k<numneigh[i]; k++){
            
            double val_i=0.0, val_j=0.0;
            
            j = neigh[i*maxneigh+k]; // if(i>=j)continue;
            j_master = master[j];
            
            qj[0] = q[j*dim + 0];
            qj[1] = q[j*dim + 1];
            qj[2] = q[j*dim + 2];
            
            mass_j =0;
            for(int type=0; type<n_types; type++){ mass_j += x[j_master*n_types+type]/eam[type][type]->mass;}
            mass_j = 1.0/mass_j;

            r2ij = (qi[0]-qj[0])*(qi[0]-qj[0])+(qi[1]-qj[1])*(qi[1]-qj[1])+(qi[2]-qj[2])*(qi[2]-qj[2]);

            if( r2ij < atoms->r2_cutoff ){
                
                // compute d_rho, d_rho_xi, d_rho_xj, phi, d_phi, d_phi_xi, d_phi_xj
                
                for(int l=0; l<n_types; l++){
                    mf_d_cubic_spline(&d_rho_mat[l][0], &d_rho_mat_xi[l], &d_rho_mat_xj[l], &(eam[l][l]->rho), qi, qj, mass_i, mass_j, w[i], w[j_master], atoms->b);// we will not use d_rho_mat 
                    
                    for(int k=l; k<n_types; k++){
                        
                        double val, val1[3], val2, val3;
                        
                        val = mf_cubic_spline( &(eam[l][k]->pair), qi, qj, mass_i, mass_j, w[i], w[j_master], atoms->b);
                        mf_d_cubic_spline(val1, &val2, &val3, &(eam[l][k]->pair), qi, qj, mass_i, mass_j, w[i], w[j_master], atoms->b);//we will not use the array val1[3]                        
                        
                        pair_mat[l][k]=val;
                        pair_mat[k][l]=val;
                        
                        //d_pair_mat[l][k][0]=val1[0]; // we do not need d_pair_mat
                        //d_pair_mat[l][k][1]=val1[1];
                        //d_pair_mat[l][k][2]=val1[2];
                        //d_pair_mat[k][l][0]=val1[0];
                        //d_pair_mat[k][l][1]=val1[1];
                        //d_pair_mat[k][l][2]=val1[2];
                        
                        d_pair_mat_xi[l][k]=val2;
                        d_pair_mat_xi[k][l]=val2;
                        
                        d_pair_mat_xj[l][k]=val3;
                        d_pair_mat_xj[k][l]=val3; 
                    }                   
                }                   
                
                //first term
                
                val_i=0.0; val_j =0.0;
                
                for(int l=0; l<n_types; l++){
                    val_i += x[j_master*n_types+l]*dF[j_master*n_types+l];
                    val_j += x[i*n_types+l]*dF[i*n_types+l];                   
                }
              	
                for(int l=0; l<n_types; l++){                    
                    
                    double rho = mf_cubic_spline( &(eam[l][l]->rho), qi, qj, mass_i, mass_j, w[i], w[j_master], atoms->b);
                    
                    //update pair force
                    f[i*n_types+l] += k1*val_i*rho;
                    if(j<n_atoms) f[j_master*n_types+l] += k1*val_j*rho; 
                    
                    for(int k=0; k<n_types; k++){
                        f[i*n_types+l] += k1*x[j_master*n_types+k]*pair_mat[l][k];
                        if(j<n_atoms) f[j_master*n_types+l] += k1*x[i*n_types+k]*pair_mat[l][k]; 
                    }
                }
                
                //second term
                
                val_i=0.0; val_j =0.0;                
                
                for(int l1=0; l1<n_types; l1++){                    
                    for(int l2=0; l2<n_types; l2++){                                           
                        val_i+=(dF[i*n_types+l1]*d_rho_mat_xi[l2]+dF[j_master*n_types+l2]*d_rho_mat_xi[l1]+d_pair_mat_xi[l1][l2])*x[i*n_types+l1]*x[j_master*n_types+l2];                        
                        val_j+=(dF[i*n_types+l1]*d_rho_mat_xj[l2]+dF[j_master*n_types+l2]*d_rho_mat_xj[l1]+d_pair_mat_xj[l1][l2])*x[i*n_types+l1]*x[j_master*n_types+l2];
                    }
                }              
                
                for(int l=0; l<n_types; l++){
                    f[i*n_types+l]                      -= k1*val_i*( sqrt( mass_i/(2.0*atoms->b) )/(w[i]*eam[l][l]->mass) )*98.227002603;
                    if(j<n_atoms) f[j_master*n_types+l] -= k1*val_j*( sqrt( mass_j/(2.0*atoms->b) )/(w[j_master]*eam[l][l]->mass) )*98.227002603;
                } 
            
            }            
        }    
    
        // entropy contribution
        
        for(int type=0; type<n_types; type++) f[i*n_types+type] += k_B*( 1.0 + log( atoms->x[i*n_types+type] + 1.0e-4 ) );
    
    }
    
    MPI_Allreduce(f, all_f, n_types*n_atoms, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

    free(qi);
    free(qj);
    free(ed);
    free(temp_ed);
    free(mf_ed);
    free(temp_mf_ed);
    free(dF);
    free(temp_dF);
    free(f);

    return;
}

void mf_Virial_Stresses_EAM_Potential( EAMPotential *eam[2][2], atom* atoms, double *all_f ){

    int i, j, k, j_master;
    int dim = 3;

    double fij[6] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
    double r2ij = 0.0, rij=0.0;
    
    double* q = atoms->q;
    double* x = atoms->x;
    double* w = atoms->w;
    int*    numneigh = atoms->numneigh;
    int*    neigh = atoms->neigh;
    int*    master = atoms->master;
    int     maxneigh = atoms->maxneigh;
    int n_atoms = atoms->n_atoms;
    int n_types = atoms->n_types;

    double* qi = (double*) malloc( dim * sizeof( double ) );
    double* qj = (double*) malloc( dim * sizeof( double ) );
    double* ed = (double*) malloc( n_atoms * sizeof( double ) );
    double* temp_ed = (double*) malloc( n_atoms * sizeof( double ) );
    double* dF = (double*) malloc( n_types * n_atoms * sizeof( double ) );
    double* temp_dF = (double*) malloc( n_types * n_atoms * sizeof( double ) );
    double* f = (double*) malloc( 6*n_atoms*sizeof( double ) ); // we will the virial stresses
    
    double d_pair_mat[n_types][n_types][6];
    double d_rho_mat[n_types][6];
    
    double mass_i, mass_j;
    
    // Initialization
    for(i=0; i<n_atoms;         i++) temp_ed[i]=0.0;  
    for(i=0; i<n_atoms*n_types; i++) temp_dF[i]=0.0;
    for(i=0; i<n_atoms*6; i++)       f[i]=0.0;
    
    //
    // Compute the electron densities for all sites
    // 
        
    for(i=rank; i<n_atoms; i+=size){
        
        qi[0] = q[i*dim + 0];
        qi[1] = q[i*dim + 1];
        qi[2] = q[i*dim + 2];
        
        mass_i =0;
        for(int type=0; type<n_types; type++){ mass_i += x[i*n_types+type]/eam[type][type]->mass;}
        mass_i = 1.0/mass_i;
        
        for(k=0; k<numneigh[i]; k++){
            
            j = neigh[i*maxneigh+k]; // if(i>=j)continue;
            j_master = master[j];
            
            qj[0] = q[j*dim + 0];
            qj[1] = q[j*dim + 1];
            qj[2] = q[j*dim + 2];
            
            mass_j =0;
            for(int type=0; type<n_types; type++){ mass_j += x[j_master*n_types+type]/eam[type][type]->mass;}
            mass_j = 1.0/mass_j;

            r2ij = (qi[0]-qj[0])*(qi[0]-qj[0]) + (qi[1]-qj[1])*(qi[1]-qj[1]) + (qi[2]-qj[2])*(qi[2]-qj[2]);

            if( r2ij < atoms->r2_cutoff ){
                
                rij = sqrt(r2ij);
                
                for(int type=0; type<n_types; type++){
                    double rhoj = cubic_spline( &(eam[type][type]->rho), rij );
                    temp_ed[i]   += rhoj*x[j_master*n_types+type];
                    if(j<n_atoms) temp_ed[j_master]+= rhoj*x[i*n_types+type];                    
                }
            }
        }
    }
    
    MPI_Allreduce(temp_ed, ed, n_atoms, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    
    //
    // Compute the derivatives of embedding function with respect to the electron density
    //
    
    for(i=rank; i<n_atoms; i+=size){       
        for(int type=0; type<n_types; type++){ 
            temp_dF[i*n_types+type] = d_cubic_spline( &(eam[type][type]->embed), ed[i]/eam[type][type]->factor )/eam[type][type]->factor;
        }
    }
    
    MPI_Allreduce(temp_dF, dF, n_types*n_atoms, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);    
    
    //
    // Compute the pair forces
    //

    for(i=rank; i<n_atoms; i+=size){
        
        qi[0] = q[i*dim + 0];
        qi[1] = q[i*dim + 1];
        qi[2] = q[i*dim + 2];
        
        mass_i =0;
        for(int type=0; type<n_types; type++){ mass_i += x[i*n_types+type]/eam[type][type]->mass;}
        mass_i = 1.0/mass_i;
    
        for(k=0; k<numneigh[i]; k++){
            
            j = neigh[i*maxneigh+k]; // if(i>=j)continue;
            j_master = master[j];
            
            fij[0]=fij[1]=fij[2]=fij[3]=fij[4]=fij[5]=0; 
            
            qj[0] = q[j*dim + 0];
            qj[1] = q[j*dim + 1];
            qj[2] = q[j*dim + 2];
            
            mass_j =0;
            for(int type=0; type<n_types; type++){ mass_j += x[j_master*n_types+type]/eam[type][type]->mass;}
            mass_j = 1.0/mass_j;

            r2ij = (qi[0]-qj[0])*(qi[0]-qj[0])+(qi[1]-qj[1])*(qi[1]-qj[1])+(qi[2]-qj[2])*(qi[2]-qj[2]);

            if( r2ij < atoms->r2_cutoff ){
                
                for(int i_type=0; i_type<n_types; i_type++){
                    mf_d_cubic_spline(&d_rho_mat[i_type][0], &(eam[i_type][i_type]->rho), qi, qj, mass_i, mass_j, w[i], w[j_master], atoms->b);
                    for(int j_type=i_type; j_type<n_types; j_type++){
                        double val1[6];
                        mf_d_cubic_spline(val1, &(eam[i_type][j_type]->pair), qi, qj, mass_i, mass_j, w[i], w[j_master], atoms->b);
                        d_pair_mat[i_type][j_type][0]=val1[0];
                        d_pair_mat[i_type][j_type][1]=val1[1];
                        d_pair_mat[i_type][j_type][2]=val1[2];
                        d_pair_mat[i_type][j_type][3]=val1[3];
                        d_pair_mat[i_type][j_type][4]=val1[4];
                        d_pair_mat[i_type][j_type][5]=val1[5];
                        d_pair_mat[j_type][i_type][0]=val1[0];
                        d_pair_mat[j_type][i_type][1]=val1[1];
                        d_pair_mat[j_type][i_type][2]=val1[2];
                        d_pair_mat[j_type][i_type][3]=val1[3];
                        d_pair_mat[j_type][i_type][4]=val1[4];
                        d_pair_mat[j_type][i_type][5]=val1[5];
                    }                   
                }
              	
                for(int i_type=0; i_type<n_types; i_type++){                    
                    for(int j_type=0; j_type<n_types; j_type++){                    
                        for(int l=0; l<6; l++){
                            fij[l]+=(dF[i*n_types+i_type]*d_rho_mat[j_type][l]+dF[j_master*n_types+j_type]*d_rho_mat[i_type][l]+d_pair_mat[i_type][j_type][l])*x[i*n_types+i_type]*x[j_master*n_types+j_type];
                        }
                    }
                }
                                
                // sij = (1/vol) \sum_k ( -m^k*(u_i^k-\bar{u}_i)*(u_j^k-\bar{u}_j)  + 0.5 \sum_l ( q_i^l - q_i^k )*f_j^{lk})
                
                f[i*6+0] += 0.5*fij[0];//xx
                f[i*6+1] += 0.5*fij[1];//yy
                f[i*6+2] += 0.5*fij[2];//zz
                f[i*6+3] += 0.5*fij[3];//xy
                f[i*6+4] += 0.5*fij[4];//xz
                f[i*6+5] += 0.5*fij[5];//yz
                
                if(j<n_atoms){
                    f[j_master*6+0] += 0.5*fij[0];//xx
                    f[j_master*6+1] += 0.5*fij[1];//yy
                    f[j_master*6+2] += 0.5*fij[2];//zz
                    f[j_master*6+3] += 0.5*fij[3];//xy
                    f[j_master*6+4] += 0.5*fij[4];//xz
                    f[j_master*6+5] += 0.5*fij[5];//yz
                    
                }            
            }            
        }
        
        // K_B*T -> effect of the temperature.
        
        double KbT= 1.0/atoms->b;
        
        f[i*6+0] -= KbT;
        f[i*6+1] -= KbT;
        f[i*6+2] -= KbT;
        
    }
    
    MPI_Allreduce(f, all_f, 6*n_atoms, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

    free(qi);
    free(qj);
    free(ed);
    free(temp_ed);
    free(dF);
    free(temp_dF);
    free(f);

    return;
}
