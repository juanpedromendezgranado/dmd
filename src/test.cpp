#include <mpi.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream> //std::istringstream
#include "eam_potential.hpp"
#include "mf_eam_free_energy.hpp"
#include "cubic_spline.hpp"
#include "atom.hpp"
#include "init_freq.hpp"
#include "output.hpp"
#include "supercell.hpp"
#include "neighbors.hpp"
#include "solver_PETSc.hpp"

using namespace std;
using namespace MPI;

extern int size;
extern int rank;

extern double k_B;
extern double h_planck;

static double distance( double* q1, double* q2 ){
    
    int i;
    int dim = 3;
    double dist = 0.0;

    for( i=0; i<dim; i++ ){
	dist += (q1[i] - q2[i]) * (q1[i] - q2[i]);
    }

    return sqrt(dist);
}

static double mf_EAM_Free_Energy_test_q( EAMPotential *eam[2][2], atom* atoms, int i_atom, int code, double epsilon){

       
    int i, j, j_master, k;
    int dim = 3;

    double en=0.0, free_en=0.0, all_free_en=0.0, r2ij;
    
    double* q = atoms->q;
    double* x = atoms->x;
    double* w = atoms->w;
    int*    numneigh = atoms->numneigh;
    int*    neigh = atoms->neigh;
    int*    master = atoms->master;
    int     maxneigh = atoms->maxneigh;
    int n_atoms = atoms->n_atoms;
    int n_types = atoms->n_types;

    double* qi = (double*) malloc( dim * sizeof( double ) );
    double* qj = (double*) malloc( dim * sizeof( double ) );
    double* ed = (double*) malloc( n_atoms * sizeof( double ) );
    double* mf_ed = (double*) malloc( n_atoms * sizeof( double ) );
    double* temp_ed = (double*) malloc( n_atoms * sizeof( double ) );
    double* temp_mf_ed = (double*) malloc( n_atoms * sizeof( double ) );
    
    double pair_mat[n_types][n_types];
    
    double mass_i, mass_j;
    
    //
    // Compute the electron densities for all sites
    //
    
    for(i=0; i<n_atoms; i++) {temp_ed[i]=0.0; temp_mf_ed[i]=0.0;}
    
    for(i=rank; i<n_atoms; i+=size){
        
        qi[0] = q[i*dim + 0];
        qi[1] = q[i*dim + 1];
        qi[2] = q[i*dim + 2];
        
        for(k=0; k<numneigh[i]; k++){
            
            j = neigh[i*maxneigh+k]; // if(i>=j)continue;
            j_master = master[j];
            
            qj[0] = q[j*dim + 0];
            qj[1] = q[j*dim + 1];
            qj[2] = q[j*dim + 2];

            r2ij = (qi[0]-qj[0])*(qi[0]-qj[0]) + (qi[1]-qj[1])*(qi[1]-qj[1]) + (qi[2]-qj[2])*(qi[2]-qj[2]);

            if( r2ij < atoms->r2_cutoff ){
                for(int type=0; type<n_types; type++){
                    double rhoj = cubic_spline( &(eam[type][type]->rho), sqrt(r2ij) );
                    temp_ed[i]   += rhoj*x[j_master*n_types+type];
                    if(j<n_atoms){
                        temp_ed[j_master]   += rhoj*x[i*n_types+type];                    
                    }
                }
            }
        }
    }
    
    
    //update value
    atoms->q[i_atom*dim+code] += epsilon;
    
    for(i=rank; i<n_atoms; i+=size){
        
        qi[0] = q[i*dim + 0];
        qi[1] = q[i*dim + 1];
        qi[2] = q[i*dim + 2];
        
        mass_i =0;
        for(int type=0; type<n_types; type++){ mass_i += x[i*n_types+type]/eam[type][type]->mass;}
        mass_i = 1.0/mass_i;
        
        for(k=0; k<numneigh[i]; k++){
            
            j = neigh[i*maxneigh+k]; // if(i>=j)continue;
            j_master = master[j];
            
            qj[0] = q[j*dim + 0];
            qj[1] = q[j*dim + 1];
            qj[2] = q[j*dim + 2];
            
            mass_j =0;
            for(int type=0; type<n_types; type++){ mass_j += x[j_master*n_types+type]/eam[type][type]->mass;}
            mass_j = 1.0/mass_j;

            r2ij = (qi[0]-qj[0])*(qi[0]-qj[0]) + (qi[1]-qj[1])*(qi[1]-qj[1]) + (qi[2]-qj[2])*(qi[2]-qj[2]);

            if( r2ij < atoms->r2_cutoff ){
                for(int type=0; type<n_types; type++){
                    double mf_rhoj = mf_cubic_spline(&(eam[type][type]->rho), qi, qj, mass_i, mass_j, w[i], w[j_master], atoms->b);
                    temp_mf_ed[i]+= mf_rhoj*x[j_master*n_types+type];
                    if(j<n_atoms){                 
                        temp_mf_ed[j_master]+= mf_rhoj*x[i*n_types+type];
                    }
                }
            }
        }
    }
    
    
    MPI_Allreduce(temp_ed, ed, n_atoms, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    MPI_Allreduce(temp_mf_ed, mf_ed, n_atoms, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        
    //
    // Compute the energy
    //
    
    
    for(i=rank; i<n_atoms; i+=size){
        
        en=0.0;
        
        qi[0] = q[i*dim + 0];
        qi[1] = q[i*dim + 1];
        qi[2] = q[i*dim + 2];
        
        mass_i =0;
        for(int type=0; type<n_types; type++){ mass_i += x[i*n_types+type]/eam[type][type]->mass;}
        mass_i = 1.0/mass_i;
        
        // Compute the embedding energy
        
        for(int type=0; type<n_types; type++){
            en += ( cubic_spline( &(eam[type][type]->embed), ed[i]/eam[type][type]->factor ) +
                        (mf_ed[i]-ed[i])*d_cubic_spline( &(eam[type][type]->embed), ed[i]/eam[type][type]->factor )/eam[type][type]->factor 
                      )*x[i*n_types+type];
        }
        
        // Compute the pair energy
               
        for(k=0; k<numneigh[i]; k++){
            
            j = neigh[i*maxneigh+k]; // if(i>=j)continue;
            j_master = master[j];
        
            qj[0] = q[j*dim + 0];
            qj[1] = q[j*dim + 1];
            qj[2] = q[j*dim + 2];
            
            mass_j =0;
            for(int type=0; type<n_types; type++){ mass_j += x[j_master*n_types+type]/eam[type][type]->mass;}
            mass_j = 1.0/mass_j;
        
            r2ij = (qi[0]-qj[0])*(qi[0]-qj[0]) + (qi[1]-qj[1])*(qi[1]-qj[1]) + (qi[2]-qj[2])*(qi[2]-qj[2]);

            if( r2ij < atoms->r2_cutoff ){            
                
                for(int i_type=0; i_type<n_types; i_type++){
                    for(int j_type=i_type; j_type<n_types; j_type++){
                        double val = mf_cubic_spline( &(eam[i_type][j_type]->pair), qi, qj, mass_i, mass_j, w[i], w[j_master], atoms->b);
                        pair_mat[i_type][j_type] = val;
                        pair_mat[j_type][i_type] = val;
                    }
                }                
                
                for(int i_type=0; i_type<n_types; i_type++){
                    for(int j_type=0; j_type<n_types; j_type++){
                        en += pair_mat[i_type][j_type]*x[i*n_types+i_type]*x[j_master*n_types+j_type];
                    }
                }
            }
        }
        
        // entropy part and kinetic energy
        
        double s = 0.0;
        for(int type=0; type<n_types; type++){
            s += atoms->x[i*n_types+type]*log( atoms->x[i*n_types+type] + 1.0e-4 ); // we add 1.0e-4 to avoid nan number
        }
        free_en += k_B*( atoms->b*en - 3.0 + 3.0*log( h_planck*atoms->b*atoms->w[i] ) + s );

    }
    
    MPI_Allreduce(&free_en, &all_free_en, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

    free( qi );
    free( qj );
    free (ed);
    free (mf_ed);
    free (temp_ed);
    free (temp_mf_ed);
    
    //restore value
    atoms->q[i_atom*dim+code] -= epsilon;

    return all_free_en;
}

static double mf_EAM_Free_Energy_test_x( EAMPotential *eam[2][2], atom* atoms, int i_atom, int code, double epsilon){

       
    int i, j, j_master, k;
    int dim = 3;

    double en=0.0, free_en=0.0, all_free_en=0.0, r2ij;
    
    double* q = atoms->q;
    double* x = atoms->x;
    double* w = atoms->w;
    int*    numneigh = atoms->numneigh;
    int*    neigh = atoms->neigh;
    int*    master = atoms->master;
    int     maxneigh = atoms->maxneigh;
    int n_atoms = atoms->n_atoms;
    int n_types = atoms->n_types;

    double* qi = (double*) malloc( dim * sizeof( double ) );
    double* qj = (double*) malloc( dim * sizeof( double ) );
    double* ed = (double*) malloc( n_atoms * sizeof( double ) );
    double* mf_ed = (double*) malloc( n_atoms * sizeof( double ) );
    double* temp_ed = (double*) malloc( n_atoms * sizeof( double ) );
    double* temp_mf_ed = (double*) malloc( n_atoms * sizeof( double ) );
    
    double pair_mat[n_types][n_types];
    
    double mass_i, mass_j;
    
    //
    // Compute the electron densities for all sites
    //
    
    for(i=0; i<n_atoms; i++) {temp_ed[i]=0.0; temp_mf_ed[i]=0.0;}
    
    for(i=rank; i<n_atoms; i+=size){
        
        qi[0] = q[i*dim + 0];
        qi[1] = q[i*dim + 1];
        qi[2] = q[i*dim + 2];
        
        for(k=0; k<numneigh[i]; k++){
            
            j = neigh[i*maxneigh+k]; // if(i>=j)continue;
            j_master = master[j];
            
            qj[0] = q[j*dim + 0];
            qj[1] = q[j*dim + 1];
            qj[2] = q[j*dim + 2];

            r2ij = (qi[0]-qj[0])*(qi[0]-qj[0]) + (qi[1]-qj[1])*(qi[1]-qj[1]) + (qi[2]-qj[2])*(qi[2]-qj[2]);

            if( r2ij < atoms->r2_cutoff ){
                for(int type=0; type<n_types; type++){
                    double rhoj = cubic_spline( &(eam[type][type]->rho), sqrt(r2ij) );
                    temp_ed[i]   += rhoj*x[j_master*n_types+type];
                    if(j<n_atoms){
                        temp_ed[j_master]   += rhoj*x[i*n_types+type];                    
                    }
                }
            }
        }
    }
    
    
    //update value
    atoms->x[i_atom*n_types+code] += epsilon;
    
    for(i=rank; i<n_atoms; i+=size){
        
        qi[0] = q[i*dim + 0];
        qi[1] = q[i*dim + 1];
        qi[2] = q[i*dim + 2];
        
        mass_i =0;
        for(int type=0; type<n_types; type++){ mass_i += x[i*n_types+type]/eam[type][type]->mass;}
        mass_i = 1.0/mass_i;
        
        for(k=0; k<numneigh[i]; k++){
            
            j = neigh[i*maxneigh+k]; // if(i>=j)continue;
            j_master = master[j];
            
            qj[0] = q[j*dim + 0];
            qj[1] = q[j*dim + 1];
            qj[2] = q[j*dim + 2];
            
            mass_j =0;
            for(int type=0; type<n_types; type++){ mass_j += x[j_master*n_types+type]/eam[type][type]->mass;}
            mass_j = 1.0/mass_j;

            r2ij = (qi[0]-qj[0])*(qi[0]-qj[0]) + (qi[1]-qj[1])*(qi[1]-qj[1]) + (qi[2]-qj[2])*(qi[2]-qj[2]);

            if( r2ij < atoms->r2_cutoff ){
                for(int type=0; type<n_types; type++){
                    double mf_rhoj = mf_cubic_spline(&(eam[type][type]->rho), qi, qj, mass_i, mass_j, w[i], w[j_master], atoms->b);
                    temp_mf_ed[i]+= mf_rhoj*x[j_master*n_types+type];
                    if(j<n_atoms){                 
                        temp_mf_ed[j_master]+= mf_rhoj*x[i*n_types+type];
                    }
                }
            }
        }
    }
    
    
    MPI_Allreduce(temp_ed, ed, n_atoms, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    MPI_Allreduce(temp_mf_ed, mf_ed, n_atoms, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        
    //
    // Compute the energy
    //
    
    
    for(i=rank; i<n_atoms; i+=size){
        
        en=0.0;
        
        qi[0] = q[i*dim + 0];
        qi[1] = q[i*dim + 1];
        qi[2] = q[i*dim + 2];
        
        mass_i =0;
        for(int type=0; type<n_types; type++){ mass_i += x[i*n_types+type]/eam[type][type]->mass;}
        mass_i = 1.0/mass_i;
        
        // Compute the embedding energy
        
        for(int type=0; type<n_types; type++){
            en += ( cubic_spline( &(eam[type][type]->embed), ed[i]/eam[type][type]->factor ) +
                        (mf_ed[i]-ed[i])*d_cubic_spline( &(eam[type][type]->embed), ed[i]/eam[type][type]->factor )/eam[type][type]->factor 
                      )*x[i*n_types+type];
        }
        
        // Compute the pair energy
               
        for(k=0; k<numneigh[i]; k++){
            
            j = neigh[i*maxneigh+k]; // if(i>=j)continue;
            j_master = master[j];
        
            qj[0] = q[j*dim + 0];
            qj[1] = q[j*dim + 1];
            qj[2] = q[j*dim + 2];
            
            mass_j =0;
            for(int type=0; type<n_types; type++){ mass_j += x[j_master*n_types+type]/eam[type][type]->mass;}
            mass_j = 1.0/mass_j;
        
            r2ij = (qi[0]-qj[0])*(qi[0]-qj[0]) + (qi[1]-qj[1])*(qi[1]-qj[1]) + (qi[2]-qj[2])*(qi[2]-qj[2]);

            if( r2ij < atoms->r2_cutoff ){            
                
                for(int i_type=0; i_type<n_types; i_type++){
                    for(int j_type=i_type; j_type<n_types; j_type++){
                        double val = mf_cubic_spline( &(eam[i_type][j_type]->pair), qi, qj, mass_i, mass_j, w[i], w[j_master], atoms->b);
                        pair_mat[i_type][j_type] = val;
                        pair_mat[j_type][i_type] = val;
                    }
                }                
                
                for(int i_type=0; i_type<n_types; i_type++){
                    for(int j_type=0; j_type<n_types; j_type++){
                        en += pair_mat[i_type][j_type]*x[i*n_types+i_type]*x[j_master*n_types+j_type];
                    }
                }
            }
        }
        
        // entropy part and kinetic energy
        
        double s = 0.0;
        for(int type=0; type<n_types; type++){
            s += atoms->x[i*n_types+type]*log( atoms->x[i*n_types+type] + 1.0e-4 ); // we add 1.0e-4 to avoid nan number
        }
        free_en += k_B*( atoms->b*en - 3.0 + 3.0*log( h_planck*atoms->b*atoms->w[i] ) + s );

    }
    
    MPI_Allreduce(&free_en, &all_free_en, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

    free( qi );
    free( qj );
    free (ed);
    free (mf_ed);
    free (temp_ed);
    free (temp_mf_ed);
    
    //restore value
    atoms->x[i_atom*n_types+code] -= epsilon;

    return all_free_en;
}

double EAM_Potential_i( EAMPotential *eam[2][2], atom* atoms, int atom_i){

       
    int i, j, j_master, k, numneigh_i;
    int dim = 3;

    double en=0.0, all_en=0.0, r2ij;
    
    double* q = atoms->q;
    double* x = atoms->x;
    int*    numneigh = atoms->numneigh;
    int*    neigh = atoms->neigh;
    int*    master = atoms->master;
    int     maxneigh = atoms->maxneigh;
    int     n_atoms = atoms->n_atoms;
    int     n_atoms_sc = atoms->n_atoms_sc;
    int     n_types = atoms->n_types;

    double* qi = (double*) malloc( dim * sizeof( double ) );
    double* qj = (double*) malloc( dim * sizeof( double ) );
    double* temp_ed = (double*) malloc( n_atoms * sizeof( double ) );
    double* ed = (double*) malloc( n_atoms * sizeof( double ) );
    
    double  pair_mat[n_types][n_types];
    
    //
    // Compute the electron densities for all sites
    //
    
    for(i=0; i<n_atoms; i++) temp_ed[i]=0.0;
    
    for(i=rank; i<n_atoms; i+=size){
        
        qi[0] = q[i*dim + 0];
        qi[1] = q[i*dim + 1];
        qi[2] = q[i*dim + 2];
        
        numneigh_i = numneigh[i];
        
        for(k=0; k<numneigh_i; k++){
            
            j = neigh[i*maxneigh+k]; // if(i>=j)continue;
            j_master = master[j];
            
            qj[0] = q[j*dim + 0];
            qj[1] = q[j*dim + 1];
            qj[2] = q[j*dim + 2];

            r2ij = (qi[0]-qj[0])*(qi[0]-qj[0]) + (qi[1]-qj[1])*(qi[1]-qj[1]) + (qi[2]-qj[2])*(qi[2]-qj[2]);

            if( r2ij < atoms->r2_cutoff ){
                for(int type=0; type<n_types; type++){
                    double rhoj = cubic_spline( &(eam[type][type]->rho), sqrt(r2ij) );
                    temp_ed[i] += rhoj*x[j_master*n_types+type];
                    if(j< n_atoms) temp_ed[j_master] += rhoj*x[i*n_types+type];
                }
            }
        }
    }
    
    MPI_Allreduce(temp_ed, ed, n_atoms, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    
    //
    // Compute the energy = embedding energy + pair energy 
    //
    
    //for(i=rank; i<n_atoms; i+=size){
    i=atom_i;
    
        qi[0] = q[i*dim + 0];
        qi[1] = q[i*dim + 1];
        qi[2] = q[i*dim + 2];
        
        // Compute the embedding energy
        
        for(int i_type=0; i_type<n_types; i_type++) en += cubic_spline( &(eam[i_type][i_type]->embed), ed[i]/eam[i_type][i_type]->factor )*x[i*n_types+i_type];
        
        // Compute the pair energy i-j

        //numneigh_i = numneigh[i];
        
        //for(k=0; k<numneigh_i; k++){
        for(j=0; j<n_atoms_sc; j++){
            if(i==j)continue;
            
            //j = neigh[i*maxneigh+k]; // if(i>=j)continue;
            j_master = master[j];
        
            qj[0] = q[j*dim + 0];
            qj[1] = q[j*dim + 1];
            qj[2] = q[j*dim + 2];
        
            r2ij = (qi[0]-qj[0])*(qi[0]-qj[0]) + (qi[1]-qj[1])*(qi[1]-qj[1]) + (qi[2]-qj[2])*(qi[2]-qj[2]);

            if( r2ij < atoms->r2_cutoff ){
                
                for(int i_type=0; i_type<n_types; i_type++){
                    for(int j_type=i_type; j_type<n_types; j_type++){
                        double val = cubic_spline( &(eam[i_type][j_type]->pair), sqrt(r2ij) );
                        pair_mat[i_type][j_type] = val;
                        pair_mat[j_type][i_type] = val;
                    }
                }                
                
                for(int i_type=0; i_type<n_types; i_type++){
                    for(int j_type=0; j_type<n_types; j_type++){
                        en += 0.5*pair_mat[i_type][j_type]*x[i*n_types+i_type]*x[j_master*n_types+j_type];
                    }
                }
            }
        }
    //}
    
    MPI_Allreduce(&en, &all_en, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

    free( qi );
    free( qj );
    free (ed);
    free (temp_ed);

    return all_en;
}

void test(EAMPotential* eam[2][2], atom* atoms){
    
    /*
    * test input file
    */
    
//    cout << endl << "atoms " << atoms->n_atoms;
//    cout << endl << "types " << atoms->n_types;
//    cout << endl << "temperature " << atoms->b;
//    cout << endl << "r2_cutoff " << atoms->r2_cutoff;
//    cout << endl << "box_x_min " << atoms->box_x_min;
//    cout << endl << "box_x_max " << atoms->box_x_max;
//    cout << endl << "box_y_min " << atoms->box_y_min;
//    cout << endl << "box_y_max " << atoms->box_y_max;
//    cout << endl << "box_z_min " << atoms->box_z_min;
//    cout << endl << "box_z_max " << atoms->box_z_max;
//    cout << endl << "diffusion " << atoms->diffusion;
//    
//    cout<<endl<<"fixed_x ";
//    for(int i=0; i<atoms->fixed_x.size(); i++) cout << atoms->fixed_x[i]<<" ";
//    
//    cout<<endl<<"fixed_q ";
//    for(int i=0; i<atoms->fixed_q.size(); i++) cout << atoms->fixed_q[i]<<" ";
//    
//    for(int i=0; i<atoms->n_atoms; i++){
//        
//        cout << endl << atoms->x[i*atoms->n_types+0] << " "
//                     << atoms->x[i*atoms->n_types+1] << " "
//                     << atoms->x[i*atoms->n_types+2] << " "
//                     << atoms->q[i*3+0] << " "
//                     << atoms->q[i*3+1] << " "
//                     << atoms->q[i*3+2] << " ";
//        
//    }
//    
//    exit(1);

    /*
     * validation of the potential
     */
    
//    for(double r=0; r<5; r=r+0.0113974268537074){
//        
//        cout << endl << r << " " << cubic_spline(&eam[0][1]->pair,r);
//        
//    }
    
//    for(int i=0; i<atoms->n_atoms; i++){
//        
//        cout << endl << atoms->numneigh[i];
//                
//    }
    
//    for(double r=0; r<7; r+=0.02){
//        atoms->q[3]+=0.01;
//        cout << endl << atoms->q[3]<< "  "<<EAM_Potential(eam,atoms);
//    }
    
    //exit(1);
    
    /*
     * 
     * Validation- Energy
     * 
     */

    //int atom1=756, atom2=757;
    
//    for(double r=10; r<100; r=r+1){
//        
//        for(int at=0; at<atoms->n_atoms; at++){
////            atoms->q[at*3+0] *= r;
////            atoms->q[at*3+1] *= r;
////            atoms->q[at*3+2] *= r; 
//              atoms->w[at] = r;
//        }
        
        //double dis = distance( &(atoms->q[atom2*3]), &(atoms->q[atom1*3]) );
        

        //cout<<endl<<dis<<" "<< 0.5*(EAM_Potential_i( eam, atoms, atom1)+EAM_Potential_i( eam, atoms, atom2) ) << flush;
        //Minimize_petsc (0,eam, atoms, "Fqw");
//        cout<< setprecision(10) <<endl<<r<<" "<<mf_EAM_Free_Energy( eam, atoms)<<flush;
        
        //for(int at=0; at<atoms->n_atoms; at++){
        //    atoms->q[at*3+0] /= r;
        //    atoms->q[at*3+1] /= r;
        //    atoms->q[at*3+2] /= r;            
        //}
        
//    }
    
     /*
     * 
     * Validation - Force at an atoms
     * 
     */

//    int atom1=0, atom2=1;
//    double force[(atoms->n_atoms)*3];
//    
//    //for(int at=0; at<atoms->n_atoms; at++){ atoms->w[at] = 10;}
//    
//    for(double r=1.0; r<100; r=r+0.05){
//        
//        for(int at=0; at<atoms->n_atoms; at++){
//            atoms->q[at*3+0] *= r;
//            atoms->q[at*3+1] *= r;
//            atoms->q[at*3+2] *= r;            
//        }
//        
//        double dis = distance( &(atoms->q[atom2*3]), &(atoms->q[atom1*3]) );
//        
//        d_EAM_Potential( eam, atoms, force);
//        //mf_d_EAM_Free_Energy( eam, atoms, force);
//        
//        cout<<dis<<" "<<force[atom2*3+0]<<" "<<force[atom2*3+1]<<" "<<force[atom2*3+2]<<endl<<flush;
//        
//        for(int at=0; at<atoms->n_atoms; at++){
//            atoms->q[at*3+0] /= r;
//            atoms->q[at*3+1] /= r;
//            atoms->q[at*3+2] /= r;            
//        }
//        
//    }
//    
//    Write_q_x_w (0, atoms);
//    
    /*
     * 
     * Validation - frequency Force at an atoms 
     * 
     */

//    int atom1=0, atom2=1;
//    double force[(atoms->n_atoms)*3];
//    
//    for(int at=0; at<atoms->n_atoms; at++){ atoms->w[at] = 1;}
//    
//    
//    for(double r=0.01; r<10; r=r+0.01){
//        
//        for(int at=0; at<atoms->n_atoms; at++){ atoms->w[at] = r;}
//        
//        double dis = distance( &(atoms->q[atom2*3]), &(atoms->q[atom1*3]) );
//        
//        //d_EAM_Potential( eam, atoms, force);
//        mf_d_EAM_Free_Energy( eam, atoms, force);
//        
//        cout<<r<<" "<<mf_EAM_Free_Energy(eam, atoms)<<" "<<force[atoms->n_atoms*3+1]<<endl<<flush;
//        
//        for(int at=0; at<atoms->n_atoms; at++){
//            atoms->q[at*3+0] /= r;
//            atoms->q[at*3+1] /= r;
//            atoms->q[at*3+2] /= r;            
//        }
//        
//    }
    
    
    /*
     *
     * Validation - Force
     *
     */
//    
////    for(int at=0; at<atoms->n_atoms; at++){
////            atoms->q[at*3+0] *= 3.0;
////            atoms->q[at*3+1] *= 3.0;
////            atoms->q[at*3+2] *= 3.0;            
////   }
//        
//    int i_atom=0;
//    double force[(atoms->n_atoms)*3], mf_force[(atoms->n_atoms)*4], pos_energy, ne_energy, epsilon=1.0e-8;
//    
////    for(int at=0; at<atoms->n_atoms; at++){ atoms->w[at] = 10;}
//   
//    
////    d_EAM_Potential( eam, atoms, force);
////    if(rank==0)cout << "The analytical value of the force at the atom "<< i_atom << ", is " << force[i_atom*3+0] <<" "<<force[i_atom*3+1]<<" "<<force[i_atom*3+2]<<endl;
//    mf_d_EAM_Free_Energy( eam, atoms, mf_force);
//    if(rank==0)cout << "The analytical value of the mean field of the force at the atom "<< i_atom << ", is " << mf_force[i_atom*3+0] <<" "<<mf_force[i_atom*3+1]<<" "<<mf_force[i_atom*3+2]<<endl;
//   
//       
//////     X component
////    atoms->q[i_atom*3+0] += epsilon;
////    for(int i=atoms->n_atoms; i<atoms->n_atoms_sc; i++)if(atoms->master[i]==i_atom) atoms->q[i*3+0] += epsilon;
////    pos_energy = EAM_Potential( eam, atoms);
////    atoms->q[i_atom*3+0] -= 2.0*epsilon;
////    for(int i=atoms->n_atoms; i<atoms->n_atoms_sc; i++)if(atoms->master[i]==i_atom) atoms->q[i*3+0] -= 2.0*epsilon;
////    ne_energy = EAM_Potential( eam, atoms);
////    atoms->q[i_atom*3+0] += epsilon;
////    for(int i=atoms->n_atoms; i<atoms->n_atoms_sc; i++)if(atoms->master[i]==i_atom) atoms->q[i*3+0] += epsilon;
////    
////    force[0] = (pos_energy-ne_energy)/2.0/epsilon;
////    
//////     Y component
////    atoms->q[i_atom*3+1] += epsilon;
////    for(int i=atoms->n_atoms; i<atoms->n_atoms_sc; i++)if(atoms->master[i]==i_atom) atoms->q[i*3+1] += epsilon;
////    pos_energy = EAM_Potential( eam, atoms);
////    atoms->q[i_atom*3+1] -= 2.0*epsilon;
////    for(int i=atoms->n_atoms; i<atoms->n_atoms_sc; i++)if(atoms->master[i]==i_atom) atoms->q[i*3+1] -= 2.0*epsilon;
////    ne_energy = EAM_Potential( eam, atoms);
////    atoms->q[i_atom*3+1] += epsilon;
////    for(int i=atoms->n_atoms; i<atoms->n_atoms_sc; i++)if(atoms->master[i]==i_atom) atoms->q[i*3+1] += epsilon;
////    
////    force[1] = (pos_energy-ne_energy)/2.0/epsilon;  
////    
////    
//////     Z component
////    atoms->q[i_atom*3+2] += epsilon;
////    for(int i=atoms->n_atoms; i<atoms->n_atoms_sc; i++)if(atoms->master[i]==i_atom) atoms->q[i*3+2] += epsilon;
////    pos_energy = EAM_Potential( eam, atoms);
////    atoms->q[i_atom*3+2] -= 2.0*epsilon;
////    for(int i=atoms->n_atoms; i<atoms->n_atoms_sc; i++)if(atoms->master[i]==i_atom) atoms->q[i*3+2] -= 2.0*epsilon;
////    ne_energy = EAM_Potential( eam, atoms);
////    atoms->q[i_atom*3+2] += epsilon;
////    for(int i=atoms->n_atoms; i<atoms->n_atoms_sc; i++)if(atoms->master[i]==i_atom) atoms->q[i*3+2] += epsilon;
////    
////    force[2] = (pos_energy-ne_energy)/2.0/epsilon;  
////    
////    if(rank==0)cout << "The numerical value of the force at the atom "<< i_atom << ", is " << force[0] <<" "<<force[1]<<" "<<force[2]<<endl;
//    
//        // X component
//    //atoms->q[i_atom*3+0] += epsilon;
//    pos_energy = mf_EAM_Free_Energy_test_q( eam, atoms, i_atom, 0, epsilon);
//    //atoms->q[i_atom*3+0] -= 2.0*epsilon;
//    ne_energy = mf_EAM_Free_Energy_test_q( eam, atoms, i_atom, 0, -1.0*epsilon);
//    //atoms->q[i_atom*3+0] += epsilon;
//    
//    force[0] = (pos_energy-ne_energy)/2.0/epsilon;
//    
//    // Y component
//    //atoms->q[i_atom*3+1] += epsilon;
//    pos_energy = mf_EAM_Free_Energy_test_q( eam, atoms, i_atom, 1, epsilon);
//    //atoms->q[i_atom*3+1] -= 2.0*epsilon;
//    ne_energy = mf_EAM_Free_Energy_test_q( eam, atoms, i_atom, 1, -1.0*epsilon);
//    //atoms->q[i_atom*3+1] += epsilon;
//    
//    force[1] = (pos_energy-ne_energy)/2.0/epsilon;  
//    
//    
//    // Z component
//    //atoms->q[i_atom*3+2] += epsilon;
//    pos_energy = mf_EAM_Free_Energy_test_q( eam, atoms, i_atom, 2, epsilon);
//    //atoms->q[i_atom*3+2] -= 2.0*epsilon;
//    ne_energy = mf_EAM_Free_Energy_test_q( eam, atoms, i_atom, 2, -1.0*epsilon);
//    //atoms->q[i_atom*3+2] += epsilon;
//    
//    force[2] = (pos_energy-ne_energy)/2.0/epsilon;  
//    
//    if(rank==0)cout << "The numerical value of the mean field force at the atom "<< i_atom << ", is " << force[0] <<" "<<force[1]<<" "<<force[2]<<endl; 
//    
    
    /*
     *
     * Validation - Force - w
     *
     */
    
//    for(int at=0; at<atoms->n_atoms; at++){
//            atoms->q[at*3+0] *= 4.0;
//            atoms->q[at*3+1] *= 4.0;
//            atoms->q[at*3+2] *= 4.0;            
//    }
        
    
//    for(int at=0; at < atoms->n_atoms; at++){ atoms->w[at] = 10.0;}
//    
//    
//    int i_atom=3;
//    double force, mf_force[(atoms->n_atoms)*4], pos_energy, ne_energy, epsilon=1.0e-4;
//   
//    mf_d_EAM_Free_Energy( eam, atoms, mf_force);
//    if(rank==0)cout << "The analytical value of the force wrt frequency at the atom "<< i_atom << ", is " << mf_force[(atoms->n_atoms)*3+i_atom] <<endl;
//   
//    atoms->w[i_atom] += epsilon;
//    pos_energy = mf_EAM_Free_Energy( eam, atoms);
//    atoms->w[i_atom] -= 2.0*epsilon;
//    ne_energy = mf_EAM_Free_Energy( eam, atoms);
//    atoms->w[i_atom] += epsilon; 
//    
//    force = (pos_energy-ne_energy)/2.0/epsilon;
//  
//    
//    if(rank==0)cout << "The numerical value of the mean field force at the atom "<< i_atom << ", is " << force<<endl;
//    
    
    /*
     *
     * Validation - Force - x 
     *
     */
    
////    for(int at=0; at<atoms->n_atoms; at++){
////            atoms->q[at*3+0] *= 3.0;
////            atoms->q[at*3+1] *= 3.0;
////            atoms->q[at*3+2] *= 3.0;            
////   }
    
////    for(int at=0; at<atoms->n_atoms; at++){ atoms->w[at] = 10;}
        
//    //int i_atom=10;
//    double force[(atoms->n_atoms)*2], pos_energy, ne_energy, epsilon=1.0e-8;
//    
//    for(int at=0; at<atoms->n_atoms; at++){ atoms->w[at] = 50;}
//    
//    //dx_EAM_Potential( eam, atoms, force);
//    //if(rank==0)cout << "The analytical value of the force at the atom "<< i_atom << ", is " << force[i_atom*2+0] <<" "<<force[i_atom*2+1]<<endl;
//    
//    mf_dx_EAM_Free_Energy( eam, atoms, force);
//    //if(rank==0)cout << "The analytical value of the mean field of force at the atom "<< i_atom << ", is " << force[i_atom*2+0] <<" "<<force[i_atom*2+1]*300<<endl;
//
//   // mf_d_EAM_Free_Energy( eam, atoms, force);
//    
//    ofstream fichero;
//    fichero.open ("results/dFdxi", ios::app);
//    
//    for(int at=0; at<atoms->n_atoms; at++){ fichero<<force[at*2+0]<<" "<<force[at*2+1]<<endl;}
//    //for(int at=0; at<atoms->n_atoms; at++){ fichero<<force[at*3+0]<<" "<<force[at*3+1]<<" "<<force[at*3+2]<<" "<<force[atoms->n_atoms*3+at]<<endl;}
    
       
//    // type 1
//    atoms->x[i_atom*2+0] += epsilon;
//    pos_energy = EAM_Potential( eam, atoms);
//    atoms->x[i_atom*2+0] -= 2.0*epsilon;
//    ne_energy = EAM_Potential( eam, atoms);
//    atoms->x[i_atom*2+0] += epsilon;
//    
//    force[0] = (pos_energy-ne_energy)/2.0/epsilon;
//    
//    // type 2
//    atoms->x[i_atom*2+1] += epsilon;
//    pos_energy = EAM_Potential( eam, atoms);
//    atoms->x[i_atom*2+1] -= 2.0*epsilon;
//    ne_energy = EAM_Potential( eam, atoms);
//    atoms->x[i_atom*2+1] += epsilon;
//    
//    force[1] = (pos_energy-ne_energy)/2.0/epsilon;  
//    
//    if(rank==0)cout << "The numerical value of the force at the atom "<< i_atom << ", is " << force[0] <<" "<<force[1]<<endl;
//    
//    // type 1
//    pos_energy = mf_EAM_Free_Energy_test_x( eam, atoms, i_atom, 0, epsilon);
//    ne_energy  = mf_EAM_Free_Energy_test_x( eam, atoms, i_atom, 0, -epsilon);
//    
//    force[0] = (pos_energy-ne_energy)/2.0/epsilon;
//    
//    // type 2
//    pos_energy = mf_EAM_Free_Energy_test_x( eam, atoms, i_atom, 1, epsilon);
//    ne_energy  = mf_EAM_Free_Energy_test_x( eam, atoms, i_atom, 1, -epsilon);
//    
//    force[1] = (pos_energy-ne_energy)/2.0/epsilon;  
//    
//    if(rank==0)cout << "The numerical value of the mean field of the force at the atom "<< i_atom << ", is " << force[0] <<" "<<force[1]<<endl;
    
    /*
     *
     * Validation - Force - x 
     *
     */
    
        
//    int i_atom=1;
//    double force[(atoms->n_atoms)*2], pos_energy, ne_energy, epsilon=1.0e-7;
//
//    
//    mf_dx_EAM_Free_Energy( eam, atoms, force);
//    if(rank==0)cout << "The analytical value of the mean field of force at the atom "<< i_atom << ", is " << force[i_atom*2+0] <<" "<<force[i_atom*2+1]<<endl;
//   
//    
//    // type 1
//    pos_energy = mf_EAM_Free_Energy_test_x( eam, atoms, i_atom, 0, epsilon);
//    ne_energy = mf_EAM_Free_Energy_test_x( eam, atoms, i_atom, 0, -epsilon);
//    
//    force[0] = (pos_energy-ne_energy)/2.0/epsilon;
//    
//    // type 2
//    pos_energy = mf_EAM_Free_Energy_test_x( eam, atoms, i_atom, 1, epsilon);
//    ne_energy = mf_EAM_Free_Energy_test_x( eam, atoms, i_atom, 1, -epsilon);
//    
//    force[1] = (pos_energy-ne_energy)/2.0/epsilon;  
//    
//    if(rank==0)cout << "The numerical value of the mean field of the force at the atom "<< i_atom << ", is " << force[0] <<" "<<force[1]<<endl;
    
     /*
     *
     * Testing - mean field approximation
     *
     */
    
//    int atom1=0, atom2=1;
//    int type =0;
//    int n_atoms = atoms->n_atoms;
//    int n_types = atoms->n_types;
//    double* x = atoms->x;
//    double* w = atoms->w;
//    double mass_i, mass_j;
//    double result;
//    
//    for(int at=0; at<atoms->n_atoms; at++){ atoms->w[at] = 10;}
//    
//    result =mf_EAM_Free_Energy(eam, atoms);    
//    if(rank==0)cout<<endl<<" "<< result << flush;
//    
//    result =EAM_Potential(eam, atoms); 
//    if(rank==0)cout<<endl<<" "<< result << flush;
//    
//    for(double r=1.0; r<20.0; r=r+0.1){
//        
//        for(int at=0; at<atoms->n_atoms; at++){
//            atoms->q[at*3+0] *= r;
//            atoms->q[at*3+1] *= r;
//            atoms->q[at*3+2] *= r;            
//        }        
//        
//        double dis = distance( &(atoms->q[atom2*3]), &(atoms->q[atom1*3]) );
//        
////        mass_i =0;
////        for(int type_i=0; type_i<n_types; type_i++){ mass_i += x[atom1*n_types+type_i]/eam[type_i][type_i]->mass;}
////        mass_i = 1.0/mass_i;
////
////        mass_j =0;
////        for(int type_i=0; type_i<n_types; type_i++){ mass_j += x[atom2*n_types+type_i]/eam[type_i][type_i]->mass;}
////        mass_j = 1.0/mass_j;        
//
//        //cout<<endl<<dis<<" "<< mf_cubic_spline(&(eam[type][type]->rho), atom1, atom2, &(atoms->q[atom1*3]), &(atoms->q[atom2*3]), mass_i, mass_j, w[atom1], w[atom2], atoms->b) << flush;
//        //cout<<endl<<dis<<" "<< cubic_spline(&(eam[type][type]->rho), dis) << flush;
//        
//        //cout<<endl<<dis<<" "<< mf_d_cubic_spline(&(eam[type][type]->rho), atom1, atom2, &(atoms->q[atom1*3]), &(atoms->q[atom2*3]), mass_i, mass_j, w[atom1], w[atom2], atoms->b) << flush;
//        //cout<<endl<<dis<<" "<< d_cubic_spline(&(eam[type][type]->rho), dis) << flush;
//        
//        if(rank==0)cout<<endl<<dis<<" "<< mf_EAM_Potential(eam, atoms) << flush;
//        if(rank==0)cout<<endl<<dis<<" "<< EAM_Potential(eam, atoms) << flush;
//        
//        for(int at=0; at<atoms->n_atoms; at++){
//            atoms->q[at*3+0] /= r;
//            atoms->q[at*3+1] /= r;
//            atoms->q[at*3+2] /= r;            
//        }
//        
//    }    
//    
    
    
       /*
     *
     * Validation - Second derivatives
     *
     */
//    
//    for(int at=0; at<atoms->n_atoms; at++){
//            atoms->q[at*3+0] *= 2.0;
//            atoms->q[at*3+1] *= 2.0;
//            atoms->q[at*3+2] *= 2.0;            
//   }
//        
//    int i_atom=0;
//    double force[(atoms->n_atoms)], pos_energy, ne_energy, energy, epsilon=1.0e-3;
////    
//    tr_d2_EAM_Potential( eam, atoms, force);
//    cout << "The analytical value of the force at the atom "<< i_atom << ", is " << force[i_atom] <<endl;   
//       
//    energy = EAM_Potential( eam, atoms);
//    
////     X component
//    atoms->q[i_atom*3+0] += epsilon;
//    pos_energy = EAM_Potential( eam, atoms);
//    atoms->q[i_atom*3+0] -= 2.0*epsilon;
//    ne_energy = EAM_Potential( eam, atoms);
//    atoms->q[i_atom*3+0] += epsilon;
//    
//    force[0] = (pos_energy+ne_energy-2.0*energy)/epsilon/epsilon;
//    
////     Y component
//    atoms->q[i_atom*3+1] += epsilon;
//    pos_energy = EAM_Potential( eam, atoms);
//    atoms->q[i_atom*3+1] -= 2.0*epsilon;
//    ne_energy = EAM_Potential( eam, atoms);
//    atoms->q[i_atom*3+1] += epsilon;
//    
//    force[1] = (pos_energy+ne_energy-2.0*energy)/epsilon/epsilon;
//    
//    
////     Z component
//    atoms->q[i_atom*3+2] += epsilon;
//    pos_energy = EAM_Potential( eam, atoms);
//    atoms->q[i_atom*3+2] -= 2.0*epsilon;
//    ne_energy = EAM_Potential( eam, atoms);
//    atoms->q[i_atom*3+2] += epsilon;
//    
//    force[2] = (pos_energy+ne_energy-2.0*energy)/epsilon/epsilon;
//    
//    cout << "The numerical value of the force at the atom "<< i_atom << ", is " << force[0]+ force[1] +force[2];
//    
//    init_frequencies (eam, atoms);
//    
//    for(int i=0; i< atoms->n_atoms; i++)cout<<endl<<atoms->w[i];
//    
    /*
     * 
     * Validation - Virial stresses
     * 
     */
//    
//        
//    int n_atoms = atoms->n_atoms;    
//    double stresses[n_atoms*6];
//    
//    double val=4.25/4.1;
//    
//    for(int at=0; at<atoms->n_atoms; at++){
//            atoms->q[at*3+0] *= val;
//            atoms->q[at*3+1] *= val;
//            atoms->q[at*3+2] *= val;           
//    }
//    
//    atoms->box_x_max = atoms->box_x_max*val;
//    atoms->box_x_min = atoms->box_x_min*val;
//    atoms->box_y_max = atoms->box_y_max*val;
//    atoms->box_y_min = atoms->box_y_min*val;
//    atoms->box_z_max = atoms->box_z_max*val;
//    atoms->box_z_min = atoms->box_z_min*val;
//    
//    supercell  (atoms);
//    neighbors  (atoms);
//        
//    Virial_Stresses_EAM_Potential(eam,atoms, stresses );
//    
//    double vol = fabs( atoms->box_x_max - atoms->box_x_min )*
//                 fabs( atoms->box_y_max - atoms->box_y_min )*
//                 fabs( atoms->box_z_max - atoms->box_z_min );
//    
//    for(int i=0; i<n_atoms; i++)cout<<"atom "<< i <<" "<<stresses[i*6+0]/vol<<" "<<stresses[i*6+1]/vol<<" "<<stresses[i*6+2]/vol
//                                                  <<" "<<stresses[i*6+3]/vol<<" "<<stresses[i*6+4]/vol<<" "<<stresses[i*6+5]/vol<<endl;
//    
    
     /*
     *
     * Testing - Input and Output
     *
     */
    
//    double f[atoms->n_atoms*4];
//    
//    for(int i=0; i<atoms->n_atoms; i++) {f[i*3+0]=i*1000+1; f[i*3+1]=i*1000+2; f[i*3+2]=i*1000+3; f[3*atoms->n_atoms+i]=i*1000+4;}
//
//    if(rank==0)Write_q_x_w (0, atoms);
//    if(rank==0)Write_FreeEntropy(0, 124452);
//    if(rank==0)Write_FreeEntropy(1, 24524);
//    
//    Write_q_x_w_mpi (0, atoms);
//    Write_q_x_w_mpi (1, atoms);
//    Write_q_x_w_mpi (2, atoms);
//    
//    Write_q_x_w_PETSc_mpi (0, atoms, f);
//    Write_q_x_w_PETSc_mpi (1, atoms, f);
//    Write_q_x_w_PETSc_mpi (2, atoms, f);
    
//    Write_restart_mpi (atoms);
//    
//    if(rank==0)ConvertBinaryFiletoTextFile();
//    if(rank==0)ConvertBinaryFiletoTextFile_Petsc();
//    
    
    exit(1);
    
}

void Write_q_x_w_nsc (int it, atom *atoms) {
    
    /**
     * \brief function to write in a file the atomic position, the atomic molar fraction and the atomic frequency
     * @param it iteration
     * @param atoms 
     */
    
    int dim=3;
    int n_atoms = atoms->n_atoms_sc;    
    int n_types = atoms->n_types;
    
    ofstream fichero;
    fichero.open ("results/q_w_x_sc", ios::app);
    
    fichero << n_atoms<< endl;
    fichero << it << endl;
    
    for (int i=0; i<n_atoms; i++){
        
        int i_master = atoms->master[i];
        
        fichero<<i+1<<"  "<<atoms->q[i*dim+0]<<"  "<<atoms->q[i*dim+1]<<"  "<<atoms->q[i*dim+2];
        for(int j=0; j<n_types; j++) fichero <<"  "<<atoms->x[i_master*n_types+j];
        fichero<<"  "<< atoms->w[i_master]<<endl;
    }
    
    fichero.close();
    
    return;
}
