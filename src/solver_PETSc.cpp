/** 
 * \file 
 * \brief Solver PETSc for the minimization of the functional
 * \author j.p. mendez
 */

#include <mpi.h> // before <stdio.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h> /*exit(1)*/
#include <math.h> /*sqrt*/ /* exp */ /* pow */ /*fabs*/
#include <time.h>
#include <string>
#include <petscsnes.h>
#include "petscdm.h"          
#include "petscdmlabel.h" 
#include "petscdmda.h" 
#include "atom.hpp"
#include "eam_potential.hpp"
#include "mf_eam_free_energy.hpp"
#include "neighbors.hpp"
#include "output.hpp"

using namespace MPI;
using namespace std;

extern int rank, size;

extern double max_disp;
extern double max_dw;
extern double min_omega;

extern double petsc_abstol;
extern double petsc_rtol;
extern double petsc_stol;
extern double petsc_maxit;
extern double petsc_maxf;

extern char petsc_ngmres_m[];

extern char petsc_linesearch_minlambda[];
extern char petsc_linesearch_damping[];
extern char petsc_linesearch_max_it[];

extern double matrix_translation[27][3];

typedef struct {
    atom *atoms; /* Info of atoms */ 
    string functional; /* To identify the functional that we pretend to minimize */
    Vec r;
    int last_its; /* last iteration in which we updated the list of neighbors */
    EAMPotential *eam[2][2]; /* Definition of the potential */
} Ctx;

static PetscErrorCode FormFunction (SNES snes, Vec x, Vec f, void *ctx)
{
    
/**
 * \brief Function to compute the function for the petsc solver.
 * 
 * @param snes the SNES context
 * @param x input vector
 * @param f function vector (output)
 * @param ctx optional user-defined context. Info of atoms.
 * @return f
 */
    
    int dim = 3;
    atom *atoms = ((Ctx*)ctx)->atoms;
    int n_atoms =    atoms->n_atoms;
    int n_atoms_sc=  atoms->n_atoms_sc;
    string functional = ((Ctx*)ctx )->functional;
    int iw0 = dim*n_atoms, iq0, imq0;
    PetscErrorCode    ierr;
    const PetscScalar *xx;
    PetscScalar *ff;
    int its;
    
    double df[n_atoms*(dim+1)];        
    int i, i_tr;

    
    ierr = VecGetArrayRead(x,&xx);CHKERRQ(ierr);
    ierr = VecGetArray(f,&ff);CHKERRQ(ierr);
    
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Update positions and frequency
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    for (i=0; i<n_atoms; i++){             
        iq0 = dim*i;
        /* Position */      
        atoms->q[iq0]   = xx[iq0];
        atoms->q[iq0+1] = xx[iq0+1];
        atoms->q[iq0+2] = xx[iq0+2];
        /* Frequency */
        atoms->w[i] = xx[iw0+i];
    }//end of loop over i
    for (i=n_atoms; i<n_atoms_sc; i++){
        iq0  = dim*i;
        imq0 = dim*atoms->master[i];
        i_tr = atoms->trans_index[i-n_atoms];
        /* Position */      
        atoms->q[iq0]   = xx[imq0]   + matrix_translation[i_tr][0]*fabs(atoms->box_x_max - atoms->box_x_min);
        atoms->q[iq0+1] = xx[imq0+1] + matrix_translation[i_tr][1]*fabs(atoms->box_y_max - atoms->box_y_min);
        atoms->q[iq0+2] = xx[imq0+2] + matrix_translation[i_tr][2]*fabs(atoms->box_z_max - atoms->box_z_min);
        
    }
        
    /* Compute neighbors */
    ierr = SNESGetIterationNumber(snes,&its);CHKERRQ(ierr);
    if( its%20 == 0 && ((Ctx*)ctx)->last_its != its ){
        neighbors(atoms);
        ((Ctx*)ctx)->last_its = its;
    }
    

    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    Compute function
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    
    if(functional.compare("Fqw")==0){
        mf_d_EAM_Free_Energy( ((Ctx*)ctx)->eam, atoms, df);
    }else if(functional.compare("Fq")==0){
        mf_d_EAM_Free_Energy( ((Ctx*)ctx)->eam, atoms, df);
        for(i=n_atoms*dim; i<n_atoms*(dim+1); i++) df[i]=0.0;
    }else if(functional.compare("Fw")==0){
        mf_d_EAM_Free_Energy( ((Ctx*)ctx)->eam, atoms, df);
        for(i=0; i<n_atoms*dim; i++) df[i]=0.0;
    }else if(functional.compare("V")==0){ 
        d_EAM_Potential( ((Ctx*)ctx)->eam, atoms, df);
        for(i=n_atoms*dim; i<n_atoms*(dim+1); i++) df[i]=0.0;
    }else{
        cout << " The functional has not been specified or the keyword is incorrect" << endl;
        exit(1);
    }                
    
    /* Store the values in the output vector*/    
    for(i=0; i< n_atoms; i++){
        iq0=i*dim;
        ff[iq0]   = -df[iq0];
        ff[iq0+1] = -df[iq0+1];
        ff[iq0+2] = -df[iq0+2];        
        ff[iw0+i] =  df[iw0+i];
    }
    
    /* Fixed atoms */
    for(i=0; i<(atoms->fixed_q).size(); i++){
        iq0 = atoms->fixed_q[i]*3;
        ff[iq0]   = 0.0;
        ff[iq0+1] = 0.0;
        ff[iq0+2] = 0.0;  
    }

    
   /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Restore vectors
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
   ierr = VecRestoreArrayRead(x,&xx);CHKERRQ(ierr);
   ierr = VecRestoreArray(f,&ff);CHKERRQ(ierr);
   
   return 0;
    
}

static PetscErrorCode SNESLinePreCheck(SNESLineSearch linesearch, Vec x, Vec y, PetscBool *changed, void *ctx)
{
    /**
     * \brief This function constrains the maximum step in each iteration.
     * 
     * @param linesearch
     * @param x solution vector
     * @param y search direction vector
     * @param changed flag to indicate the precheck changed x or y
     * @param ctx user context
     * @return 
     */

    PetscScalar *yy;
    PetscErrorCode ierr;
    atom *atoms = ((Ctx*)ctx)->atoms;
    int n_atoms = atoms->n_atoms;
    
    ierr = VecGetArray(y,&yy);CHKERRQ(ierr);
    
    // ** do something **//
    
    ierr = VecRestoreArray(y,&yy);CHKERRQ(ierr);
    
    *changed = PETSC_FALSE;
    
    return 0;
}

static PetscErrorCode SNESLinePostCheck(SNESLineSearch linesearch, Vec x, Vec y, Vec w, PetscBool *changed_y, PetscBool *changed_w, void *ctx)
{
    /**
     * \brief This function constrains the maximum step in each iteration.
     * 
     * @param linesearch
     * @param x old solution vector
     * @param y search direction vector
     * @param w new solution vector
     * @param changed_y indicates that the line search changed y
     * @param changed_w indicates that the line search changed w
     * @param ctx user context
     * @return 
     */
    
    int dim=3;
    const PetscScalar *xx;
    PetscScalar *ww;
    PetscErrorCode ierr;    
    atom *atoms = ((Ctx*)ctx)->atoms;
    int n_atoms = atoms->n_atoms;
    string functional = ((Ctx*)ctx )->functional;
    int iw0 = dim*n_atoms, iq0;    
    double dq2, invdq, dw, max_disp2 = max_disp*max_disp;                   
    int i;
    
    ierr = VecGetArrayRead(x,&xx);CHKERRQ(ierr);
    ierr = VecGetArray(w,&ww)    ;CHKERRQ(ierr);
            
    /* to avoid numerical error if we are minimaxing only q or w */
    if( functional.compare("Fq")==0 || functional.compare("V")==0 ){        
        for(i=0; i<n_atoms; i++) ww[iw0+i] = xx[iw0+i];
    }
    if(functional.compare("Fw")==0){
        for(i=0; i<n_atoms; i++){ 
            iq0 = dim*i;
            ww[iq0]   = xx[iq0];
            ww[iq0+1] = xx[iq0+1];
            ww[iq0+2] = xx[iq0+2];
        }
    } 
    
    for(i=0; i<n_atoms; i++){
        iq0 = dim*i;
        
        /*Applying some constraints on the maximum step*/
        // position
        dq2 = (ww[iq0]-xx[iq0])*(ww[iq0]-xx[iq0]) + (ww[iq0+1]-xx[iq0+1])*(ww[iq0+1]-xx[iq0+1]) + (ww[iq0+2]-xx[iq0+2])*(ww[iq0+2]-xx[iq0+2]);
        if(dq2>max_disp2){ 
            invdq  = max_disp/sqrt(dq2);
            ww[iq0]  = xx[iq0]   + (ww[iq0]  -xx[iq0])  *invdq;
            ww[iq0+1]= xx[iq0+1] + (ww[iq0+1]-xx[iq0+1])*invdq;
            ww[iq0+2]= xx[iq0+2] + (ww[iq0+2]-xx[iq0+2])*invdq;
        }    

        //frequency
        dw=fabs(ww[iw0+i]-xx[iw0+i]); 
        if(dw>max_dw) ww[iw0+i] = xx[iw0+i] + max_dw*(ww[iw0+i]- xx[iw0+i])/dw;
        
        //checks
        if(ww[iw0+i]<=min_omega)ww[iw0+i]=min_omega;
    
    }//end of loop over i        
        
    ierr = VecRestoreArrayRead(x,&xx);CHKERRQ(ierr);
    ierr = VecRestoreArray(w,&ww);CHKERRQ(ierr);
    
    *changed_y = PETSC_FALSE;
    *changed_w = PETSC_TRUE;
    
    return 0;
}

PetscErrorCode Minimize_petsc (int time_step, EAMPotential *eam[2][2], atom *atoms, string functional)
{
   
    /**
     * \brief 
     * 
     * @param time_step time step
     * @param sites info of the atoms
     * @param Natoms_sc number of atoms of the supercell
     * @param Natoms number of atoms of the cell
     * @param functional function to minimize: V, Fq, Fw, Fqw
     * @return PetscErrorCode: 1, 0.
     */
    
    SNES           snes;       /* nonlinear solver context */
    Vec            x;          /* solution vectors */
    const PetscScalar *rr, *xx;
    PetscErrorCode ierr;
    PetscInt       its, ix[3], iw;
    SNESLineSearch linesearch;
    double dqmax=0.0, dwmax=0.0, dq=0.0, dw=0.0, pos[3], w;
    int i, iw0, iq0, imq0, dim=3, i_tr;
    int n_atoms= atoms->n_atoms;
    int n_atoms_sc= atoms->n_atoms_sc;
////    DM dm;
    
    PetscReal abstol = petsc_abstol; //absolute convergence tolerance
    PetscReal rtol = petsc_rtol; //relative convergence tolerance
    PetscReal stol = petsc_stol; //convergence tolerance in terms of the norm of the change in the solution between steps, || delta x || < stol*|| x ||
    PetscInt  maxit = petsc_maxit;//maximum number of iterations
    PetscInt  maxf =  petsc_maxf;//maximum number of function evaluations
    SNESConvergedReason	reason;		/* Set a reason for convergence/divergence of solver (SNES) */
    
  
    /* Set some options */

    
    /* SNESNGMRES */
    
    PetscOptionsSetValue(NULL,"-snes_type","ngmres");
    PetscOptionsSetValue(NULL,"-snes_ngmres_m",(const char*)&petsc_ngmres_m);
    PetscOptionsSetValue(NULL,"-snes_ngmres_select_type","difference");//options: none; difference; linesearch // CUIDAD LO HE CAMBIADO. ANTES ESTABA DIFFERENCE
    //PetscOptionsSetValue(NULL,"-snes_ngmres_restart_type","difference");
    //PetscOptionsSetValue(NULL,"-snes_ngmres_monitor",NULL);
    
    /*Line seach*/    
    PetscOptionsSetValue(NULL,"-snes_linesearch_type","cp");// cp, l2
    PetscOptionsSetValue(NULL,"-snes_linesearch_minlambda",(const char*)&petsc_linesearch_minlambda);
    //PetscOptionsSetValue(NULL,"-snes_linesearch_maxstep","10");
    PetscOptionsSetValue(NULL,"-snes_linesearch_damping",(const char*)&petsc_linesearch_damping); // default is 1.0
    PetscOptionsSetValue(NULL,"-snes_linesearch_max_it",(const char*)&petsc_linesearch_max_it);
    
    
    
    Ctx ctx;
    ctx.atoms = atoms;
    ctx.functional = functional;
    ctx.last_its=-1;
    
    ctx.eam[0][0] = eam[0][0];
    ctx.eam[0][1] = eam[0][1];
    ctx.eam[1][1] = eam[1][1];
    ctx.eam[1][0] = eam[1][0];
    
    iw0 = dim*n_atoms;
  
   /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Create nonlinear solver context
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    ierr = SNESCreate(PETSC_COMM_SELF ,&snes);CHKERRQ(ierr);
    
////    ierr = DMDACreate1d(PETSC_COMM_SELF, DM_BOUNDARY_NONE, (PetscInt)n_atoms*(dim+1), 1, 1, NULL, &dm);
////    ierr = SNESSetDM(snes, dm);CHKERRQ(ierr);
    
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Create matrix and vector data structures; set corresponding routines
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  /* Create vectors for solution and nonlinear function */
    ierr = VecCreateSeq(PETSC_COMM_SELF,(PetscInt)n_atoms*(dim+1),&x);CHKERRQ(ierr);
    ierr = VecSetFromOptions(x);CHKERRQ(ierr);
    ierr = VecDuplicate(x,&(ctx.r));CHKERRQ(ierr);

  /* Set function evaluation routine and vector. */    
    ierr = SNESSetFunction(snes,ctx.r,FormFunction,&ctx);CHKERRQ(ierr);
      
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Customize nonlinear solver; set runtime options
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
        
    ierr = SNESGetLineSearch(snes, &linesearch);CHKERRQ(ierr);CHKERRQ(ierr);
    ierr = SNESLineSearchSetPreCheck(linesearch, SNESLinePreCheck, &ctx);CHKERRQ(ierr);
    ierr = SNESLineSearchSetPostCheck(linesearch,SNESLinePostCheck, &ctx);CHKERRQ(ierr);   
    
    ierr = SNESSetFromOptions(snes);CHKERRQ(ierr);
    ierr = SNESSetTolerances(snes,abstol,PETSC_DEFAULT,PETSC_DEFAULT,maxit,maxf);CHKERRQ(ierr);  

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Evaluate initial guess; then solve nonlinear system; save final solution
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    /* Set the initial values */
    for(i=0; i<n_atoms; i++){
            iq0 = dim*i;
            ix[0]=iq0; ix[1]=iq0+1; ix[2]=iq0+2;
            pos[0] = atoms->q[iq0];
            pos[1] = atoms->q[iq0+1];
            pos[2] = atoms->q[iq0+2];
            VecSetValues(x,3,ix,pos,INSERT_VALUES);
    
            iw= iw0 + i;
            w = atoms->w[i];
            VecSetValues(x,1,&iw,&w,INSERT_VALUES);
    }

    /* Solve */
    ierr = SNESSolve(snes,NULL,x);CHKERRQ(ierr);
    ierr = SNESGetIterationNumber(snes,&its);CHKERRQ(ierr); 
       
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Store the final solution
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    ierr = VecGetArrayRead(x,&xx);CHKERRQ(ierr);
    for (i=0; i<n_atoms; i++){             
        iq0 = dim*i;
        /* Position */      
        atoms->q[iq0]   = xx[iq0];
        atoms->q[iq0+1] = xx[iq0+1]; 
        atoms->q[iq0+2] = xx[iq0+2];         
        /* Frequency */
        atoms->w[i] = xx[iw0+i];
          
    }//end of loop over i
    for (i=n_atoms; i<n_atoms_sc; i++){        
        iq0  = dim*i;
        imq0 = dim*atoms->master[i];
        i_tr = atoms->trans_index[i-n_atoms];      
        /* Position */      
        atoms->q[iq0]   = xx[imq0]   + matrix_translation[i_tr][0]*fabs(atoms->box_x_max - atoms->box_x_min);
        atoms->q[iq0+1] = xx[imq0+1] + matrix_translation[i_tr][1]*fabs(atoms->box_y_max - atoms->box_y_min);
        atoms->q[iq0+2] = xx[imq0+2] + matrix_translation[i_tr][2]*fabs(atoms->box_z_max - atoms->box_z_min);
        
    }
    ierr = VecRestoreArrayRead(x,&xx);CHKERRQ(ierr);
    
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     converged or diverged reason
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    ierr = VecGetArrayRead(ctx.r,&rr);CHKERRQ(ierr);
    
    if(rank==0){
    
        ierr = SNESGetConvergedReason(snes,&reason); CHKERRQ(ierr);
    
        ofstream fichero;
        fichero.open ("results/ConvergenceCondition_q", ios::app);
    
        fichero << "Iteration: "<< time_step << endl;
        fichero << "Number of SNES iterations = "<< its << endl;
        switch (reason) {
            /*---------- DIVERGED-----------*/
            case  SNES_DIVERGED_FUNCTION_DOMAIN:{fichero << "SNES_DIVERGED_FUNCTION_DOMAIN\n";}break; /* the new x location passed the function is not in the domain of F */
            case  SNES_DIVERGED_FUNCTION_COUNT:{fichero <<"SNES_DIVERGED_FUNCTION_COUNT\n";} break;
            case  SNES_DIVERGED_LINEAR_SOLVE:{fichero <<"SNES_DIVERGED_LINEAR_SOLVE\n";} break; /* the linear solve failed */
            case  SNES_DIVERGED_FNORM_NAN:{fichero <<"SNES_DIVERGED_FNORM_NAN\n";} break;
            case  SNES_DIVERGED_MAX_IT:{fichero <<"SNES_DIVERGED_MAX_IT\n";} break;
            case  SNES_DIVERGED_LINE_SEARCH:{fichero <<"SNES_DIVERGED_LINE_SEARCH\n";} break; /* the line search failed */
            case  SNES_DIVERGED_INNER:{fichero <<"SNES_DIVERGED_INNER\n";} break; /* inner solve failed */
            case  SNES_DIVERGED_LOCAL_MIN:{fichero <<"SNES_DIVERGED_LOCAL_MIN\n";} break; /* || J^T b || is small, implies converged to local minimum of F() */
            case  SNES_CONVERGED_ITERATING:{fichero <<"SNES_CONVERGED_ITERATING\n";} break;
            /*---------- CONVERGED-----------*/
            case  SNES_CONVERGED_FNORM_ABS:{fichero <<"SNES_CONVERGED_FNORM_ABS\n";} break; /* ||F|| < atol */
            case  SNES_CONVERGED_FNORM_RELATIVE:{fichero <<"SNES_CONVERGED_FNORM_RELATIVE\n";} break; /* ||F|| < rtol*||F_initial|| */
            case  SNES_CONVERGED_SNORM_RELATIVE:{fichero <<"SNES_CONVERGED_SNORM_RELATIVE\n";} break; /* Newton computed step size small; || delta x || < stol || x ||*/
            case  SNES_CONVERGED_ITS:{fichero <<"SNES_CONVERGED_ITS\n";} break; /* maximum iterations reached */
            case  SNES_CONVERGED_TR_DELTA:{fichero <<"SNES_CONVERGED_TR_DELTA\n";} break;
        }        
    
        for(i=0; i<n_atoms; i++){
            iq0=3*i;
            dq = rr[iq0]*rr[iq0] + rr[iq0+1]*rr[iq0+1] + rr[iq0+2]*rr[iq0+2];
            dw = fabs(rr[iw0+i]);       
            if(dw>dwmax) dwmax = dw; 
            if(dq>dqmax) dqmax = dq;
        }
       
    
        fichero << "max_dq " << sqrt(dqmax) << endl;
        fichero << "max_dw " << dwmax << endl;    
    
        fichero.close();
    
    }
    
    Write_q_x_w_PETSc_mpi(time_step, atoms, (double*)rr);
    ierr = VecRestoreArrayRead(ctx.r,&rr);CHKERRQ(ierr); 
    
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Free work space.
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
////    ierr = DMDestroy(&dm);CHKERRQ(ierr);
    ierr = VecDestroy(&x);CHKERRQ(ierr);
    ierr = SNESDestroy(&snes);CHKERRQ(ierr);
    ierr = VecDestroy(&(ctx.r));CHKERRQ(ierr);  
        
    return 0;
}
