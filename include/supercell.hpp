
/* 
 * File:   supercell.hpp
 * Author: juan
 *
 * Created on February 5, 2018, 2:03 PM
 */

#ifndef SUPERCELL_HPP
#define SUPERCELL_HPP

void supercell ( atom *atoms);

#endif /* SUPERCELL_HPP */

