/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   solver_PETSc.hpp
 * Author: juan
 *
 * Created on February 23, 2018, 12:43 PM
 */

#ifndef SOLVER_PETSC_HPP
#define SOLVER_PETSC_HPP

#include <petscsnes.h>

PetscErrorCode Minimize_petsc (int time_step, EAMPotential *eam[2][2], atom *atoms, string functional);

#endif /* SOLVER_PETSC_HPP */

