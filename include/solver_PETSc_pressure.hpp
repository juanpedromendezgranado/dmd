
#ifndef SOLVER_PETSC_PRESSURE_HPP
#define SOLVER_PETSC_PRESSURE_HPP

#include <petscsnes.h>

PetscErrorCode Minimize_npt_petsc (int time_step, EAMPotential *eam[2][2], atom *atoms, double *press);

#endif /* SOLVER_PETSC_PRESSURE_HPP */

