#ifndef EAM_POTENTIAL_HPP
#define EAM_POTENTIAL_HPP

#include "cubic_spline.hpp"
#include "atom.hpp"

enum EAMMaterials{Mg,Al,MgAl};

/**
 * Datastructure for EAM potential when using spline data
 */

typedef struct EAMPotential{
        
        CubicSpline embed;
	CubicSpline rho;
	CubicSpline pair;

	double mass;
        double radius;
	double factor;
        double r_cutoff;
        
}EAMPotential;

void init_EAM( EAMPotential* eam, EAMMaterials eam_material );
void destroy_EAM( EAMPotential* eam );

/* total potential */
double EAM_Potential( EAMPotential *eam[2][2], atom *atoms);

/* gradient of potential in a specified direction */
void d_EAM_Potential( EAMPotential *eam[2][2], atom *atoms, double*);

void tr_d2_EAM_Potential( EAMPotential *eam[2][2], atom *atoms, double*);

void dx_EAM_Potential( EAMPotential *eam[2][2], atom* atoms, double *f );

void Virial_Stresses_EAM_Potential( EAMPotential *eam[2][2], atom* atoms, double *all_f );

#endif
