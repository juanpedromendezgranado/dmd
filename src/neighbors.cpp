#include <mpi.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <iostream>
#include "atom.hpp"

using namespace MPI;
using namespace std;

extern int rank;
extern int size;

void neighbors(atom *atoms){
    
    // we store for each i atom their neighbors that i<j.

    double r2cutoff = ( sqrt(atoms->r2_cutoff) + atoms->skin )*( sqrt(atoms->r2_cutoff) + atoms->skin );
    int n_atoms = atoms->n_atoms;
    int n_atoms_sc = atoms->n_atoms_sc;
    int maxneigh = atoms->maxneigh;
    int i, j;
    double dx, dy, dz, rij2=0.0;

    int* neigh =    (int*) malloc( maxneigh * n_atoms * sizeof(int) );
    int* numneigh = (int*) malloc( n_atoms * sizeof(int) );
    
    // Initialization    
    for(i=0; i<n_atoms; i++)numneigh[i]=0;
    for(i=0; i<n_atoms; i++)for(j=0; j<maxneigh; j++)neigh[i*maxneigh+j]=0;
    
    // neighbors
    
    for(i=rank; i<n_atoms; i+=size){
        for(j=i+1; j<n_atoms_sc; j++){
        
            dx = atoms->q[j*3+0]-atoms->q[i*3+0];
            rij2=dx*dx;
            if(rij2<=r2cutoff){
                dy = atoms->q[j*3+1]-atoms->q[i*3+1];
                rij2 += dy*dy;
                if(rij2<=r2cutoff){
                    dz = atoms->q[j*3+2]-atoms->q[i*3+2];
                    rij2 += dz*dz;
                    if(rij2<=r2cutoff){
                        if(numneigh[i]>=maxneigh){ cout << "The number of neighbors for atom "<<i<<" has exceeded the limit "<< maxneigh<<endl; exit(1);}
                        neigh[i*maxneigh + numneigh[i] ] = j;
                        numneigh[i] += 1;                        
                    }                
                }           
            }
        }//end of the for
    }//end of the for
    
    // communication between processors
    
    MPI_Allreduce(numneigh, atoms->numneigh, n_atoms,          MPI_INT, MPI_SUM, MPI_COMM_WORLD);
    MPI_Allreduce(neigh,    atoms->neigh,    maxneigh*n_atoms, MPI_INT, MPI_SUM, MPI_COMM_WORLD);    
        
    // free memory
    free(neigh);
    free(numneigh);

    return;

}
