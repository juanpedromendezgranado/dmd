
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "atom.hpp"

extern double maxneigh;
extern double skin;
extern int diffusion;

extern double box_x_min;
extern double box_y_min;
extern double box_z_min;
extern double box_x_max;
extern double box_y_max;
extern double box_z_max;

extern double r2_cutoff;

void init_atom( atom* cs){
    
    cs->maxneigh = maxneigh; 
    cs->skin = skin;
    cs->diffusion = diffusion;
    
    cs->box_x_min = box_x_min;
    cs->box_y_min = box_y_min;
    cs->box_z_min = box_z_min;
    cs->box_x_max = box_x_max;
    cs->box_y_max = box_x_max;
    cs->box_z_max = box_x_max;
    
    cs->r2_cutoff = r2_cutoff;
    
    return;   
}

void destroy_atom( atom* cs ){
    
    free( cs->q );
    free( cs->x );
    free( cs->w );
    free( cs->numneigh );
    free( cs->neigh );
    free( cs->master );
    free( cs->trans_index );
    
    return;
}
