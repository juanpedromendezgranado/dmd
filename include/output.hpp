/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   output.hpp
 * Author: juan
 *
 * Created on February 1, 2018, 3:44 PM
 */

#ifndef OUTPUT_HPP
#define OUTPUT_HPP

void Write_q_x_w (int it, atom *atoms);
void Write_FreeEntropy(int it, double en);

void Write_q_x_w_mpi (int it, atom *atoms);
void Write_q_x_w_PETSc_mpi  (int it, atom *atoms, double f[]);
void Write_restart_mpi (atom *atoms);

void ConvertBinaryFiletoTextFile();
void ConvertBinaryFiletoTextFile_Petsc();

void Write_Diffusion_Coeff(int iter, double coeff);
void Write_Diffusive_Step(int iter, double coeff);

#endif /* OUTPUT_HPP */

