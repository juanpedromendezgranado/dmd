/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   variables.hpp
 * Author: juan
 *
 * Created on February 2, 2018, 10:57 AM
 */

#ifndef VARIABLES_HPP
#define VARIABLES_HPP

/*****************************/
/********* VARIABLES  ********/
/*****************************/

double maxneigh=200;//50
double skin=2.0;
int sc_coeff=2.0;
double r2_cutoff = 36; // default cutoff radio

/*****************************/
/********* BOX DIMENSION  ********/
/*****************************/

double box_x_min = -100000;
double box_y_min = -100000;
double box_z_min = -100000;
double box_x_max = 100000;
double box_y_max = 100000;
double box_z_max = 100000;

/*****************************/
/********* FREQUENCY  ********/
/*****************************/

double min_omega=1.0; //!< the minimun value of frequency. A frequency smaller than min_omega is not allow.
double min_init_freq=50;  //!< the minimun value of frequency for the initial computation of the frequency.

/*****************************/
/********* CONSTANTS  ********/
/*****************************/

double k_B = 8.617332478e-5 ;///< Boltzmann constant [eV/K]
double h_planck = 6.5821192815e-4;///<  Reduced Planck constant \bar{h}=h/2/pi = 6.5821192815×10−16 eV s = 6.5821192815×10−4 eV ps

/***********************************/
/********* PARALLELIZATION  ********/
/***********************************/

int rank=0;
int size=1;

/*********************************/
/*********** MAXIMUN STEP  ********/
/*********************************/

double max_disp =0.3; //!< maxium displacement possible in each iteraction
double max_dw = 10; //!< maxium step for frequency in each iteration

/*********************************/
/*********** PARAMETERS FOR PETSC SOLVER ********/
/*********************************/

double petsc_abstol=1.e-4;//!< absolute convergence tolerance
double petsc_rtol=1.e-4;//!< relative convergence tolerance
double petsc_stol=1.e-4;//!< convergence tolerance in terms of the norm of the change in the solution between steps, || delta x || < stol*|| x ||
double petsc_maxit=2;//!< maximum number of iterations
double petsc_maxf=10;//!< maximum number of function evaluations

char petsc_ngmres_m[]="2";//!< Number of stored previous solutions and residuals

char petsc_linesearch_minlambda[]="0.01";//!< The minimum step length
char petsc_linesearch_damping[]="0.10";//!< The linesearch damping parameter
char petsc_linesearch_max_it[]="3";//!< The number of iterations for iterative line searches

/*********************************/
/*********** PARAMETERS FOR PETSC SOLVER - PRESSURE ********/
/*********************************/

char linesearch_damping[] ="0.0000001"; //="0.0000001"; //!< lambda value for the Richardson nonlinear solver
double frequency_force_factor = 1.0; //!< factor applied to the frequency forces
int Fw=0; //<! 1 == minimization of F w.r.t. w; 0 == no minimization of F w.r.t. w

double petsc_pressure_abstol=1.e-4;//!< absolute convergence tolerance
double petsc_pressure_rtol=1.e-4;//!< relative convergence tolerance
double petsc_pressure_stol=1.e-4;//!< convergence tolerance in terms of the norm of the change in the solution between steps, || delta x || < stol*|| x ||
double petsc_pressure_maxit=200;//!< maximum number of iterations
double petsc_pressure_maxf=200;//!< maximum number of function evaluations


/*********************************/
/*** PARAMETERS FOR DIFFUSION ***/
/*********************************/

double Df=1.0;           //!< Diffusivity. Parameter to be adjusted. see article below
double dt_diffusion=1.0; //!< time step. Parameter to be adjusted.
double dr_diffusion=3.0; //!< diffusivity distance. Parameter to be adjusted.
int    diffusion=1;      //!< label to consider diffusion or not. 0=No diffusion and 1= yes diffusion; by default yes diffusion
double min_dxij=0.0;     //!< minimun diffusion. If the diffusion between site i and j is less than this amoun, we dont take into account in our calculations.
int    max_it_diff=2;    //<! maximum number of iterations of the diffusion for each step
double max_total_mass = 0.01; //<! in percentage

/*********************************/
/*** MATRIX TRANSLATION ***/
/*********************************/

    /* COMBINATIONS:
    * 
    * +x  ->   1 0 0 
    * +x+y  -> 1 1  0
    * +x-y  -> 1 -1 0 
    * +x+z  -> 1 0  1 
    * +x-z  -> 1 0  -1  
    * 
    * -x  ->   -1 0  0
    * -x+y  -> -1 1  0 
    * -x-y  -> -1 -1 0   
    * -x+z  -> -1 0  1  
    * -x-z  -> -1 0  -1   
    *
    * y+z  -> 0 1 1   
    * y-z  -> 0 1 -1   
    *  
    * -y+z  -> 0 -1 1   
    * -y-z  -> 0 -1 -1
    * 
    * +x+y-z  -> 1 1  -1
    * +x+y+z  -> 1 1  1
    * +x-y-z  -> 1 -1 -1
    * +x-y+z  -> 1 -1 1
    * 
    * -x+y-z  -> -1 1 -1
    * -x+y+z  -> -1 1  1
    * -x-y-z  -> -1 -1 -1
    * -x-y+z  -> -1 -1 1
    *       
    */

    // combinations     
    double matrix_translation[27][3]= {{0., 0., 0.},
                                       { 0., 0., 1.}, { 0., 0., -1.},
                                       {1., 0., 0.}, { 1., 1.,  0.}, { 1., -1., 0.}, { 1., 0.,  1.}, { 1., 0.,  -1.},
                                       {-1., 0.,  0.}, {-1., 1.,  0.}, {-1., -1., 0.}, {-1., 0.,  1.}, {-1., 0.,  -1.},
                                       { 0., 1., 0.}, { 0., 1., 1.}, { 0., 1., -1.},
                                       { 0., -1., 0.}, { 0., -1., 1.}, { 0., -1., -1.}, 
                                       { 1., 1.,  -1.}, { 1., 1.,  1.}, { 1., -1., -1.}, { 1., -1., 1.},
                                       {-1., 1., -1.}, {-1., 1.,  1.}, {-1., -1., -1.}, {-1., -1., 1.}};

#endif /* VARIABLES_HPP */
