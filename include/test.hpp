
/* 
 * File:   test.hpp
 * Author: juan
 *
 * Created on November 15, 2017, 11:09 AM
 */

#ifndef TEST_HPP
#define TEST_HPP

void test(EAMPotential* eam[2][2], atom* atoms);
void Write_q_x_w_nsc (int it, atom *atoms);

#endif /* TEST_HPP */

