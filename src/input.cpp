
#include <mpi.h> // before <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream> //std::istringstream
#include <fstream>
#include <math.h>
#include <cstdio>
#include <string> //std::string
#include <cstring> 

#include "atom.hpp"

using namespace std;
using namespace MPI;

extern int rank, size;
extern double k_B;

//void read_atoms(atom *atoms){
//        
//    FILE* f_atoms;
//    double b, r_cutoff;
//    double box_x_min, box_x_max;
//    double box_y_min, box_y_max;
//    double box_z_min, box_z_max;
//    int n_atoms, n_types, error;    
//    
//    f_atoms = fopen( "input/hcp_shell.dat", "r" );    
//    error = fscanf(f_atoms, "%d %d %lf %lf", &n_atoms, &n_types, &b, &r_cutoff);
//    error = fscanf(f_atoms, "%lf %lf", &box_x_min, &box_x_max);
//    error = fscanf(f_atoms, "%lf %lf", &box_y_min, &box_y_max);
//    error = fscanf(f_atoms, "%lf %lf", &box_z_min, &box_z_max);
//    
//    atoms->n_atoms = n_atoms;
//    atoms->n_atoms_sc = n_atoms;
//    atoms->n_types = n_types;
//    atoms->temperature = b;
//    atoms->b = 1.0/k_B/b;
//    atoms->r2_cutoff = r_cutoff*r_cutoff;
//    atoms->q = (double*) malloc(3*n_atoms*sizeof(double));
//    atoms->x = (double*) malloc(n_types*n_atoms*sizeof(double));
//    atoms->w = (double*) malloc(n_atoms*sizeof(double));
//    atoms->numneigh = (int*) malloc( n_atoms * sizeof(int) );
//    atoms->neigh =    (int*) malloc( atoms->maxneigh * n_atoms * sizeof(int) );
//    atoms->master = (int*) malloc( n_atoms * sizeof(int) );
//    atoms->trans_index = (int*) malloc( 0 * sizeof(int) );
//    atoms->box_x_min=box_x_min;
//    atoms->box_x_max=box_x_max;
//    atoms->box_y_min=box_y_min;
//    atoms->box_y_max=box_y_max;
//    atoms->box_z_min=box_z_min;
//    atoms->box_z_max=box_z_max;
////    atoms->lx = fabs(box_x_max-box_x_min);
////    atoms->ly = fabs(box_y_max-box_y_min);
////    atoms->lz = fabs(box_z_max-box_z_min);
//    
//    for(int i=0; i<n_atoms; i++) atoms->master[i]=i;
//    
//    if(n_types>=4){cout<< "Error: The read_atoms functions is only implemented for up to 3 atoms type."<<endl; exit(1);}
//    
//    if(n_types==1)for(int i=0;i<n_atoms;i++)error = fscanf(f_atoms, "%lf %lf %lf %lf",         &(atoms->x[n_types*i]),                                                     &(atoms->q[3*i+0]), &(atoms->q[3*i+1]), &(atoms->q[3*i+2]) );
//    if(n_types==2)for(int i=0;i<n_atoms;i++)error = fscanf(f_atoms, "%lf %lf %lf %lf %lf",     &(atoms->x[n_types*i]), &(atoms->x[n_types*i+1]),                           &(atoms->q[3*i+0]), &(atoms->q[3*i+1]), &(atoms->q[3*i+2]) );
//    if(n_types==3)for(int i=0;i<n_atoms;i++)error = fscanf(f_atoms, "%lf %lf %lf %lf %lf %lf", &(atoms->x[n_types*i]), &(atoms->x[n_types*i+1]), &(atoms->x[n_types*i+2]), &(atoms->q[3*i+0]), &(atoms->q[3*i+1]), &(atoms->q[3*i+2]) );
//       
//    fclose( f_atoms );    
//    
//    return;
//}

void read_atoms(atom *atoms) {

    double b, r_cutoff;
    double box_x_min, box_x_max;
    double box_y_min, box_y_max;
    double box_z_min, box_z_max;
    int n_atoms, n_types;
    double pos[3], xi;
    
    int flag_n_atoms=0, flag_n_types=0, flag_temperature=0, i_atom=0;
    
    string line;
    fstream fichero;
    
    fichero.open ("input/hcp_shell.dat", ios::in);    
    
    if (fichero == NULL){
        printf("Fatal error: opening file %s", "input/hcp_shell.dat");
        exit(1);
    }
       
    while( getline(fichero, line) != NULL )
    {       
        
        istringstream iss;
        
        if( line[0] == '#' || line[0] == '\n'|| line[0] == '\r' || line=="") {continue;}
                                    
        // number of atoms
        if(line.find("atoms ") != string::npos){                
            iss.str (line);
            char campo[] = "atoms";
            iss >> campo;
            iss >> n_atoms;                                 
            
            atoms->n_atoms = n_atoms;
            atoms->n_atoms_sc = n_atoms; // initially, n_atoms_sc = n_atoms           
         
            flag_n_atoms = 1;
            continue;
        }
        
        if(flag_n_atoms==0){cout << endl <<"Error: The number of atoms has to be defined in the first line of the input file (e.g., atoms 123)";  exit(1);}
        
        if(line.find("types ") != string::npos) 
        {                
            iss.str (line);
            char campo[] = "types";
            iss >> campo;
            iss >> n_types;
            atoms->n_types = n_types;
                 
            // we set up all variables
            n_atoms = atoms->n_atoms;
            atoms->q = (double*) malloc(3*n_atoms*sizeof(double));
            atoms->x = (double*) malloc(n_types*n_atoms*sizeof(double));
            atoms->w = (double*) malloc(n_atoms*sizeof(double));
            atoms->numneigh = (int*) malloc( n_atoms * sizeof(int) );
            atoms->neigh =    (int*) malloc( atoms->maxneigh * n_atoms * sizeof(int) );
            atoms->master = (int*) malloc( n_atoms * sizeof(int) );
            atoms->trans_index = (int*) malloc( 0 * sizeof(int) );
            
            for(int i=0; i<n_atoms; i++) atoms->master[i]=i;
                  
            flag_n_types=1;
            continue;
        }
        
        if(flag_n_types==0){cout << endl <<"Error: The number of atom types has to be defined in the second line of the input file (e.g., types 2)";  exit(1);}
        
        if(line.find("temperature ") != string::npos) 
        {                
            iss.str (line);
            char campo[] = "temperature";
            iss >> campo;
            iss >> b;
            atoms->b = 1.0/k_B/b;
            flag_temperature = 1;
            continue;
        }

        if(line.find("r_cutoff ") != string::npos) 
        {                
            iss.str (line);
            char campo[] = "r_cutoff";
            iss >> campo;
            iss >> r_cutoff;
            atoms->r2_cutoff = r_cutoff*r_cutoff;
            continue;
        }     
               
        // Fixed atoms
        if(line.find("fixed_atom ") != string::npos) 
        {                           
            iss.str (line);
            char campo[] = "fixed_atom";
            int fixed_atom;
            iss >> campo;         
            while(iss >> fixed_atom) atoms->fixed_q.push_back(fixed_atom-1);     
            continue;
        }
        
        // Fixed molar fractions
        if(line.find("fixed_molar_fraction ") != string::npos) 
        {                
            iss.str (line);
            char campo[] = "fixed_molar_fraction";
            int fixed_molar_fraction;
            iss >> campo;         
            while(iss >> fixed_molar_fraction) atoms->fixed_x.push_back(fixed_molar_fraction-1);         
            continue;
        }
        
        // box_x_min
        if(line.find("box_x_min ") != string::npos) 
        {                
            iss.str (line);
            char campo[] = "box_x_min";
            iss >> campo;         
            iss >> box_x_min;
            atoms->box_x_min=box_x_min;       
            continue;
        }
        
        // box_x_max
        if(line.find("box_x_max ") != string::npos) 
        {                
            iss.str (line);
            char campo[] = "box_x_max";
            iss >> campo;         
            iss >> box_x_max;
            atoms->box_x_max=box_x_max;        
            continue;
        }
        
        // box_y_min
        if(line.find("box_y_min ") != string::npos) 
        {                
            iss.str (line);
            char campo[] = "box_y_min";
            iss >> campo;         
            iss >> box_y_min;
            atoms->box_y_min=box_y_min;        
            continue;
        }
        
        // box_y_max
        if(line.find("box_y_max ") != string::npos) 
        {                
            iss.str (line);
            char campo[] = "box_y_max";
            iss >> campo;         
            iss >> box_y_max;
            atoms->box_y_max=box_y_max;      
            continue;
        }
        
        // box_z_min
        if(line.find("box_z_min ") != string::npos) 
        {                
            iss.str (line);
            char campo[] = "box_z_min";
            iss >> campo;         
            iss >> box_z_min;
            atoms->box_z_min=box_z_min;        
            continue;
        }
        
        // box_z_max
        if(line.find("box_z_max ") != string::npos) 
        {                
            iss.str (line);
            char campo[] = "box_z_max";
            iss >> campo;         
            iss >> box_z_max;
            atoms->box_z_max=box_z_max;
            continue;
        }
        
        // box_x
        if(line.find("box_x ") != string::npos) 
        {                
            iss.str (line);
            char campo[] = "box_x";
            iss >> campo;         
            iss >> box_x_min;
            iss >> box_x_max;
            if(box_x_min>box_x_max){
                cout << endl << "Error: box_x_min > box_x_max"; 
                exit(1);
            }
            atoms->box_x_min=box_x_min;
            atoms->box_x_max=box_x_max;
            continue;
        }
        
        // box_y
        if(line.find("box_y ") != string::npos) 
        {                
            iss.str (line);
            char campo[] = "box_y";
            iss >> campo;         
            iss >> box_y_min;
            iss >> box_y_max;
            if(box_y_min>box_y_max){
                cout << endl << "Error: box_y_min > box_y_max"; 
                exit(1);
            }
            atoms->box_y_min=box_y_min;
            atoms->box_y_max=box_y_max;
            continue;
        }
        
        // box_z
        if(line.find("box_z ") != string::npos) 
        {                
            iss.str (line);
            char campo[] = "box_z";
            iss >> campo;         
            iss >> box_z_min;
            iss >> box_z_max;
            if(box_y_min>box_y_max){
                cout << endl << "Error: box_z_min > box_z_max"; 
                exit(1);
            }
            atoms->box_z_min=box_z_min;
            atoms->box_z_max=box_z_max;
            continue;
        }
        
        // diffusion
        if(line.find("diffusion ") != string::npos) 
        {                
            iss.str (line);
            char campo[] = "diffusion";
            char value[5];
            iss >> campo;         
            iss >> value;
            if(strcmp(value,"YES")==0 || strcmp(value,"yes")==0 || strcmp(value,"Yes")==0 ){atoms->diffusion=1;}
            else{
                if(strcmp(value,"NO")==0 || strcmp(value,"no")==0 || strcmp(value,"No")==0){atoms->diffusion=0;}
                else{cout << endl << "Error: the input of diffusion has to be \"yes\" or \"no\", instead of \""<<value<<"\""; exit(1);}
            }
            continue;
        }
        
        if(i_atom<atoms->n_atoms){
        
            iss.str (line);
                      
            // atomic molar fraction
            for (int i=0; i<n_types;i++) {iss >>xi; atoms->x[i_atom*n_types+i]=xi;};
        
            // position
            for (int i=0; i<3;i++) {iss >> pos[i];}
            atoms->q[i_atom*3+0]=pos[0];
            atoms->q[i_atom*3+1]=pos[1];
            atoms->q[i_atom*3+2]=pos[2];
        
            i_atom++;
        }

    }
    
    fichero.close();
    
    if(flag_temperature==0){cout << endl <<"Error: the temperature has not been defined in the input file (e.g., temperature 300)."; exit(1);}   
    
    return;
}

void read_atoms_mpi (atom *atoms){
    
    /**
     * \brief Read the input file of the restart file in parallel, using MPI. If there is not restart file, this function calls the function ReadPositionAtoms() that read the data from an input file.
     * 
     * @param sites info of atoms
     */
    
    // definition of variable
    int n_atoms, n_types;
    int errcode;
    MPI_File file;
    MPI_Offset ofst=(int)0;
    int num_elements;
    MPI_Datatype datatype, filetype;
    int heading1[4];
    double heading2[9];
    int num_fixed_q, num_fixed_x;
    MPI_Aint data_disps[3], file_disps[3]; // q_i[3](double), x_ik[n_types](double), omega_i(double)
    int array_blocklens[3];
    MPI_Datatype array_types[3]={MPI_BYTE,MPI_BYTE,MPI_BYTE};
    
    // Opening the restart file 
    errcode = MPI_File_open(MPI_COMM_SELF,"results/restart", MPI_MODE_RDONLY, MPI_INFO_NULL, &file);
    
    // if restart file does not exist, we proceed to read the txt input file and exit the function
    if(errcode!=MPI_SUCCESS){
        if(rank==0)cout << endl << "Reading initial inputs in serial. This function has not been implemented to read the input file in parallel.";
        return read_atoms(atoms);
        
    }
 
    if(rank==0){cout<<"Warning: Reading file restart from a previous simulations..."<<endl;}
    
    // rank==0 reads the heading of the file (dimension of the cell, number of atoms, etc.) 
    if(rank==0){      
               
        MPI_File_read_at (file, (int)0, heading1, sizeof(int)*4, MPI_BYTE, MPI_STATUS_IGNORE); // n_atoms, n_types, diffusion, maxneigh
        MPI_File_read_at (file, sizeof(int)*4, heading2, sizeof(double)*9, MPI_BYTE, MPI_STATUS_IGNORE); // r_cutoff, skin, b, box_x_min, box_x_max, box_y_min, box_y_max, box_z_min, box_z_max
        
        MPI_File_read_at (file, sizeof(int)*4+sizeof(double)*9, &num_fixed_q, sizeof(int), MPI_BYTE, MPI_STATUS_IGNORE); // Number of atoms that are fixed
        (atoms->fixed_q).resize(num_fixed_q);
        MPI_File_read_at (file, sizeof(int)*5+sizeof(double)*9, &(atoms->fixed_q[0]), sizeof(int)*num_fixed_q, MPI_BYTE, MPI_STATUS_IGNORE); // Number of atoms that are fixed
        
        MPI_File_read_at (file, sizeof(int)*(5+num_fixed_q)+sizeof(double)*9, &num_fixed_x, sizeof(int), MPI_BYTE, MPI_STATUS_IGNORE); // Number of atoms that have fixed atomic molar concentration
        (atoms->fixed_x).resize(num_fixed_x);
        MPI_File_read_at (file, sizeof(int)*(6+num_fixed_q)+sizeof(double)*9, &(atoms->fixed_x[0]), sizeof(int)*num_fixed_x, MPI_BYTE, MPI_STATUS_IGNORE); // Number of atoms that have fixed atomic molar fraction
        
        atoms->n_atoms   = heading1[0];
        atoms->n_types   = heading1[1];
        atoms->diffusion = heading1[2];
        atoms->maxneigh  = heading1[3];
        
        atoms->r2_cutoff  = heading2[0];
        atoms->skin      = heading2[1];
        atoms->b         = heading2[2];
        atoms->box_x_min = heading2[3];
        atoms->box_x_max = heading2[4];        
        atoms->box_y_min = heading2[5];
        atoms->box_y_max = heading2[6];
        atoms->box_z_min = heading2[7];
        atoms->box_z_max = heading2[8]; 
        cout << endl << "Number total of atoms:" << atoms->n_atoms;
        
        MPI_Bcast ( heading1, 4, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Bcast ( heading2, 9, MPI_DOUBLE, 0, MPI_COMM_WORLD);
        
        MPI_Bcast ( &num_fixed_q, 1, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Bcast ( &(atoms->fixed_q[0]), num_fixed_q, MPI_INT, 0, MPI_COMM_WORLD);
        
        MPI_Bcast ( &num_fixed_x, 1, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Bcast ( &(atoms->fixed_x[0]), num_fixed_x, MPI_INT, 0, MPI_COMM_WORLD);
    
    }else{
        MPI_Bcast ( heading1, 4, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Bcast ( heading2, 9, MPI_DOUBLE, 0, MPI_COMM_WORLD);
        
        MPI_Bcast ( &num_fixed_q, 1, MPI_INT, 0, MPI_COMM_WORLD);
        (atoms->fixed_q).resize(num_fixed_q);
        MPI_Bcast ( &(atoms->fixed_q[0]), num_fixed_q, MPI_INT, 0, MPI_COMM_WORLD);
        
        MPI_Bcast ( &num_fixed_x, 1, MPI_INT, 0, MPI_COMM_WORLD);
        (atoms->fixed_x).resize(num_fixed_x);
        MPI_Bcast ( &(atoms->fixed_x[0]), num_fixed_x, MPI_INT, 0, MPI_COMM_WORLD);
        
        atoms->n_atoms   = heading1[0];
        atoms->n_types   = heading1[1];
        atoms->diffusion = heading1[2];
        atoms->maxneigh  = heading1[3];
        
        atoms->r2_cutoff  = heading2[0];
        atoms->skin      = heading2[1];
        atoms->b         = heading2[2];
        atoms->box_x_min = heading2[3];
        atoms->box_x_max = heading2[4];        
        atoms->box_y_min = heading2[5];
        atoms->box_y_max = heading2[6];
        atoms->box_z_min = heading2[7];
        atoms->box_z_max = heading2[8];        
    }
    
    // we proceed to create the struct for atoms info
    
    n_atoms = atoms->n_atoms;
    n_types = atoms->n_types;
    
    atoms->q = (double*) malloc(3*n_atoms*sizeof(double));
    atoms->x = (double*) malloc(n_types*n_atoms*sizeof(double));
    atoms->w = (double*) malloc(n_atoms*sizeof(double));
    atoms->numneigh = (int*) malloc(n_atoms*sizeof(int) );
    atoms->neigh =    (int*) malloc( atoms->maxneigh * n_atoms * sizeof(int) );
    atoms->master = (int*) malloc( n_atoms * sizeof(int) );    
 
    
    // we read the info of the atoms: pos, atomic molar fraction, frequency, etc. 
    
    num_elements = (int)(n_atoms/size); 
    if(num_elements<1){// only when there is more processors than data to read
        if(rank<n_atoms){num_elements=1; }else{num_elements=0;}
    }
    
    
    ofst = sizeof(double)*9+sizeof(int)*(6+num_fixed_q+num_fixed_x);
    
    //for the data
    MPI_Get_address ( &atoms->q[rank*num_elements*3],       &data_disps[0]); // q
    MPI_Get_address ( &atoms->x[rank*num_elements*n_types], &data_disps[1]); // x[n_types]
    MPI_Get_address ( &atoms->w[rank*num_elements],         &data_disps[2]); // w
    //for file
    file_disps[0]=ofst+rank*num_elements*sizeof(double)*3; // q_i
    file_disps[1]=ofst+(n_atoms*3+rank*num_elements*n_types)*sizeof(double); // x[n_types]
    file_disps[2]=ofst+(n_atoms*(3+n_types)+rank*num_elements)*sizeof(double); // w
    
    if(rank<size-1 || size==1){           
        if(num_elements>0){
            array_blocklens[0]=sizeof(double)*3*num_elements; // q[3]
            array_blocklens[1]=sizeof(double)*n_types*num_elements; // x[n_types]
            array_blocklens[2]=sizeof(double)*num_elements; // w      
        }else{for(int i=0; i<3; i++)array_blocklens[i]=1;}
    }else{// for the rank==size-1 when size>1, write the rest
        if(num_elements>0){
            array_blocklens[0]=sizeof(double)*3*(n_atoms-num_elements*rank); // q[3]
            array_blocklens[1]=sizeof(double)*n_types*(n_atoms-num_elements*rank); // x[n_types]
            array_blocklens[2]=sizeof(double)*(n_atoms-num_elements*rank); // w
        }else{for(int i=0; i<3; i++)array_blocklens[i]=1;}
    }
    
    // define MPI_type for the data and file
    MPI_Type_create_struct(3,array_blocklens,data_disps,array_types, &datatype); 
    MPI_Type_create_struct(3,array_blocklens,file_disps,array_types, &filetype);     
    // commit MPI_type
    MPI_Type_commit(&datatype);
    MPI_Type_commit(&filetype);    
    // write
    MPI_File_set_view(file, 0,MPI_BYTE,filetype,"native",MPI_INFO_NULL);
    MPI_File_read_all(file, 0, 1, datatype, MPI_STATUS_IGNORE);    
    // free MPI_type
    MPI_Type_free(&datatype);
    MPI_Type_free(&filetype);  
    
    /*** Send all info ***/
    
    num_elements = (int)(n_atoms/size); 
    if(num_elements<1) num_elements=1;// only when there are more processors than data to read
    
    // define MPI_type for the data
    array_blocklens[0]=sizeof(double)*3*num_elements; // q[3]
    array_blocklens[1]=sizeof(double)*n_types*num_elements; // x[n_types]
    array_blocklens[2]=sizeof(double)*num_elements; // w
    
    // sending the information
    for(int i=0; i< size-1 && i<n_atoms; i++){
        MPI_Get_address ( &atoms->q[i*num_elements*3],       &data_disps[0]);   // q[3]
        MPI_Get_address ( &atoms->x[i*num_elements*n_types], &data_disps[1]);   // x[n_types]
        MPI_Get_address ( &atoms->w[i*num_elements],         &data_disps[2]);   // w
        MPI_Type_create_struct(3,array_blocklens,data_disps,array_types, &datatype);
        MPI_Type_commit(&datatype);
        MPI_Bcast (0, 1, datatype, i, MPI_COMM_WORLD);
        MPI_Type_free(&datatype);       
    }
   
    // sending the information that was read by rank==size-1 if size>1
    if(n_atoms > (int)num_elements*(size-1)  && size>1 ){
        MPI_Get_address ( &atoms->q[(size-1)*num_elements*3],       &data_disps[0]); // q[3]
        MPI_Get_address ( &atoms->x[(size-1)*num_elements*n_types], &data_disps[1]); // x[n_types]
        MPI_Get_address ( &atoms->w[(size-1)*num_elements],         &data_disps[2]); // w
        array_blocklens[0]=sizeof(double)*3*(n_atoms-num_elements*(size-1)); // q_i
        array_blocklens[1]=sizeof(double)*n_types*(n_atoms-num_elements*(size-1)); // x_i
        array_blocklens[2]=sizeof(double)*(n_atoms-num_elements*(size-1)); // omega_i
        MPI_Type_create_struct(3,array_blocklens,data_disps,array_types, &datatype);
        MPI_Type_commit(&datatype);
        MPI_Bcast(0, 1, datatype, size-1, MPI_COMM_WORLD);
        MPI_Type_free(&datatype);
    } 
    
    // deleting the datatype and colsing the file    
    MPI_File_close(&file); 
    
    return;
}
