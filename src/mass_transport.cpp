/**
 * \file
 * \author J.P. Mendez
 * \brief Atomistic Diffusion (parallel code)
 * 
 * Ref. article: Phys. Rev. B 84, 054103 (2011). Ju Li et al.
 * 
 */

#include <omp.h>
#include <mpi.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <math.h> /*sqrt*/ /* exp */ /* pow */
#include <stdlib.h> /*exit(1)*/
#include <time.h>
#include <algorithm> 

#include "atom.hpp"
#include "eam_potential.hpp"
#include "mf_eam_free_energy.hpp"
#include "output.hpp"

using namespace MPI;

extern int rank, size;
extern double k_B;
extern double Df;
extern double dt_diffusion;
extern double dr_diffusion;
extern double min_dxij;
extern int max_it_diff;
extern double max_total_mass;

static double exponential( double fij){

    double value = exp( fij/(2.0*k_B) );
    
    return value;    
    //if(value<1.0){return value;}else{return 1.0;}
    
}

static double Normsq( double *qi, double *qj){
    
    return  (qj[0]-qi[0])*(qj[0]-qi[0]) + (qj[1]-qi[1])*(qj[1]-qi[1]) + (qj[2]-qi[2])*(qj[2]-qi[2]);
}

void mass_transport (int iter, atom *atoms, EAMPotential *eam[2][2]){ 
/** 
 *  \brief serial version
 *  The mass diffusion only occurs if the interstitial sites are within "dr_diffusion" distance   
 */ 
    
    int n_atoms = atoms->n_atoms;
    int n_types = atoms->n_types;
    int dim=3;
    
    double xi0, xf0, dx[n_atoms], dxij, dxi, xfinal0=0.0, total_mass=0.0; // to store the molar fraction 
    double fij0, dfdxi[n_atoms*n_types]; // when we call the function to compute these values, we reset the array
    double total_factor=0.0, factor=1.0;
    double rsq = dr_diffusion*dr_diffusion;    
    int atom_i, atom_j, master_j, num_atom_j, it, it_diff=0;
    
    
   
    while( total_mass < max_total_mass*n_atoms && it_diff<max_it_diff ){
        
        // compute df/dx for all atoms and types of atoms
        mf_dx_EAM_Free_Energy( eam, atoms, dfdxi);
        
        factor=1.0; it_diff++; 
    
        // initializing x
        for(atom_i=0; atom_i<n_atoms; atom_i++) dx[atom_i] = 0.0;
 
        for(atom_i=0; atom_i<n_atoms; atom_i++){      
        
            num_atom_j = atoms->numneigh[atom_i];
            for(it=0; it< num_atom_j; it++){
                atom_j   = atoms->neigh[atom_i*atoms->maxneigh + it]; 
                master_j = atoms->master[atom_j];
           
                if(Normsq(&atoms->q[atom_i*dim],&atoms->q[atom_j*dim])>rsq) continue; // The diffusion only occurs within a certain distance,  dr_diffusion.   
            
                fij0 = ( dfdxi[atom_i*n_types]-dfdxi[atom_i*n_types+1]-dfdxi[master_j*n_types]+dfdxi[master_j*n_types+1] ); // nu_i^0 - nu_j^0 = ( dF/dx_i^0 - dF/dx_i^1 ) - ( dF/dx_j^0 - dF/dx_j^1  ) in order to force that \sum_l x_{il}=1.0
                dxij = dt_diffusion*Df*( atoms->x[master_j*n_types]*atoms->x[atom_i*n_types+1]  *exponential(-fij0) - 
                                         atoms->x[atom_i*n_types]*  atoms->x[master_j*n_types+1]*exponential(fij0) );
                
                // some verifications
                
                if(dxij>=0){

                    //if( atoms->x[master_j*n_types]< dxij || 1.0-atoms->x[atom_i*n_types] < dxij ) dxij = min(atoms->x[master_j*n_types],1.0-atoms->x[atom_i*n_types]); // if site j has less mass than dxij or site i has less space to store the all flux
                    if( atoms->x[master_j*n_types]<=0.0 || atoms->x[atom_i*n_types]>=1.0 || fabs(dxij)< min_dxij) continue;
                
                }else{
                    
                    //if( 1.0-atoms->x[master_j*n_types]< fabs(dxij) || atoms->x[atom_i*n_types] < fabs(dxij) ) dxij = -min(1.0-atoms->x[master_j*n_types],atoms->x[atom_i*n_types]);                    
                    if( atoms->x[master_j*n_types]>=1.0 || atoms->x[atom_i*n_types]<=0.0 || fabs(dxij)< min_dxij) continue;
                
                }
            
                dx[atom_i] += dxij;
                if(atom_j< n_atoms) dx[atom_j] -= dxij;
            }
        }
   
        //rescaling
        for(atom_i=0; atom_i<n_atoms; atom_i++){
        
            xi0 = atoms->x[atom_i*n_types];
            xf0 = xi0 + dx[atom_i];
        
            if(xf0>1.0 && xf0!=xi0){ // x[atom_i]!=xi to avoid nan number
                double val = (1.0-xi0)/(xf0-xi0);
                if(factor > val ){factor = val;}            
            }
        
            if(xf0<0.0 && xf0!=xi0){ // x[atom_i]!=xi to avoid nan number
                double val = (0.0-xi0)/(xf0-xi0);
                if(factor > val){factor = val;}     
            }        
        }   
    
        if(factor<1.0){
            if(rank==0)cout<<endl<<"Recalculating diffusion with a factor of "<< factor; 
        }    
    
        // updating the atomic molar fractions.
        for(atom_i=0; atom_i<n_atoms; atom_i++){
            
            dxi = dx[atom_i]*factor;
            total_mass += dxi;
            xfinal0 = atoms->x[atom_i*n_types] + dxi;
            //if(xfinal0>1.0) xfinal0 = 1.0; // to avoid numerical error
            //if(xfinal0<0.0) xfinal0 = 0.0; // to avoid numerical error
            
            atoms->x[atom_i*n_types+0]= xfinal0;
            atoms->x[atom_i*n_types+1]= 1.0 - xfinal0; // to avoid numerical error
        
        }

        if(rank==0) Write_Diffusion_Coeff(iter, factor);
        total_factor += factor;
    
    }// end of the while
    
    if(rank==0) Write_Diffusive_Step(iter, total_factor);   
    
    return;
}
